# Linux Device Drivers

[TOC]

## Introduction
### Kernel
```
╭───────────────────────────────────────────────────────────────────────────╮
│                         The System Call Interface                         │
╰──────┬──────────────┬─────────────┬──────────────┬───────────────┬────────╯
       │              │             │              │               │
╭──────┼──────────────┼─────────────┼──────────────┼───────────────┼────────╮
│      │              │             │              │               │        │
│╭─────┴──────╮ ╭─────┴──────╮ ╭────┴────╮    ╭────┴────╮    ╭─────┴──────╮ │
││   Process  │ │   Memroy   │ │   File  │    │ Device  │    │ Networking │ │ Kernel
││ Management │ │ Management │ │ Systems │    │ Control │    │            │ │ Subsystems
│╰────────────╯ ╰────────────╯ ╰─────────╯    ╰─────────╯    ╰────────────╯ │
│  Concurrency,   Virtual      Files & dirs     ttys &        Connectivity  │ Features
│  Multicasting   memory         The VFS      device access                 │ Implemented
│╭───────────╮  ╭─────────╮    ╭─────────────╮╭───────────╮  ╭───────────╮  │
││   Arch-   │  │ Memory  │    │ File System ││ Character │  │  Network  │  │
││ dependent │  │ Manager │    │   types     ││  Devices  │  │ Subsystem │  │
││   code    │  │         │    │    ■   ■    ││           │  │           │  │
││           │  │         │    ╰─────────────╯│           │  ╰───────────╯  │ Software
││           │  │         │    ╭─────────────╮│           │  ╭───────────╮  │ Support
││           │  │         │    │    Block    ││           │  │     IF    │  │
││           │  │         │    │   Devices   ││           │  │  Drivers  │  │
││           │  │         │    │    ■   ■    ││   ■   ■   │  │   ■   ■   │  │
│╰─────┬─────╯  ╰────┬────╯    ╰──────┬──────╯╰─────┬─────╯  ╰─────┬─────╯  │
╰──────┼─────────────┼────────────────┼─────────────┼──────────────┼────────╯
       │             │                │             │              │
    ╭──┴──╮     ╭────┴───╮       ╭────┴──╮     ╭────┴────╮  ╭──────┴────╮
    │ CPU │     │ Memory │       │ Disks │     │ Console │  │  Network  │     Hardware 
    ╰─────╯     ╰────────╯       ╰───────╯     ╰─────────╯  │ Interface │
                                                            ╰───────────╯
```
#### Process Management
- Creating and destroying processes and connection to outside word (input/output).
- Communicating between different processes (signals, pipes, interprocess communiction primitaves).
#### Memory Management
- Kernel builds up virtual address space for all processes on top of limited available resources.
#### Filesystems
- Structured filesystem on top of unstructured hardware.
#### Device Control
- The code is called a *device driver* and the kernel must have one for every peripheral present on the system.
#### Networking
- Networking must be managed by the OS because most network operations are not specific to a process.
- All the routing and address resolution issues are implemented within the kernel.
### Loadable Modules
- Each piece of code that can be added to the kernel at runtime is called a *module*.
- The kernel supports different types (classes) of modules, including device drivers.
- Each module is made up of object code (not linked into a complete executable) that can be dynamically linked to the running kernel by the `insmod` program and unlinked by the `rmmod` program.
### Classes of Devices and Modules
- Three fundamental device types:
#### Character Devices
- A character (char) device is one that can be accessed as a stream of byte.
- The driver usually impliments at least the `open`, `close`, `read`, and `write` system calls.
- Examples: `/dev/console`, `/dev/ttyS0`
#### Block Devices
- A device (i.e. disk) that can host a filesystem.
- Generally, can only handle I/O operations that transfer one or more whole blocks, which are usually 512 bytes (or larger power of 2) in length.
#### Network Interfaces
- Communication between kernel and detwork device driver is completely different from that of char and block drivers.
- Instead of `read` and `write` the kernel calls functions related to packet transmission.
## Building and Running Modules
### The Hello World Module
- This module defines two functions:
  - One to be invoked when the module is loaded into the kernel: `hello_init`
  - One for when the module is removed: `hello_exit`
``` c
#include <linux/init.h>
#include <linux/module.h>

static int hello_init(void) {
       printk(KERN_ALERT "Hello, world\n");
       return 0;
}

static void hello_exit(void) {
       printk(KERN_ALERT "Goodbye, cruel world\n");
}

module_init(hello_init);
module_exit(hello_exit);
```
- The `printk` functions is defined in the linux kernel and made available to modules.
- Behaves similar to the `printf` function.
- The string `KERN_ALERT` is the priority of the mesage.
### Kernel Modules Versus applications
```
                                          ╭──────────────────╮
                                          │ blk_init_queue() ├╌╌╌╌╌╌╌╌─╮
              ╭───────────────╮╌╌╌╌╌╌╌╌╌> ╰──────────────────╯         ╎
    insmod───>│ init function │                                        ╎
              ╰───────────────╯╌╌╌╌╌╌╌╌╌> ╭────────────╮               ╎
                                          │ add_disk() │               ╎
                                          ╰──────┬─────╯               ╎
                                          ╭─╌╌╌╌╌╯                     ╎
                                          V         struct             ╎
                                      ╭───────╮     gendisk            ╎
                            ╭─────────┤       │<╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╮ ╎
    block_device ops        │         ╰───┬───╯                      ╎ ╎
    ╭──────────────╮        │             │                          ╎ ╎
╭───┤              │        │             ╰──────────────╮           ╎ ╎
│   ├──────────────┤<───────╯                            │           ╎ ╎
│╭──┤              │                                     │           ╎ ╎
││  ├──────────────┤                                     V           ╎ ╎
││╭─┤              │                             ╭───────────────╮<╌╌┼╌╯
│││ ╰──────────────╯        ╭────────────────────│               │   ╎
│││                         │                    ╰───────────────╯<╌╌┼╌╮
│││                         │                      request_queue_    ╎ ╎
│││                         │                                        ╎ ╎
│││                         │                                        ╎ ╎
│││    ╭───────────╮        │                                        ╎ ╎
│││    │ request() │<───────╯                                        ╎ ╎
│││    ╰───────────╯                                                 ╎ ╎
││╰───>╭───────────╮╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌>╭───────────╮         ╎ ╎
│╰────>│           │╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌>│           ├╌╌╌╌╌╌╌╌╌╯ ╎
╰─────>╰───────────╯╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌>╰───────────╯           ╎
                                           ╭───────────────╮           ╎
           ╭──────────╮╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌>│ del_gendisk() │           ╎
 rmmod────>│ cleanup  │                    ╰───────────────╯           ╎
           │ function │                    ╭─────────────────────╮     ╎
           ╰──────────╯╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌>│ blk_cleanup_queue() ├╌╌╌╌╌╯
                                           ╰─────────────────────╯
```
### User Space and Kernel Space
- A module runs in *kernel space*, whereas applications run in *user space*.
- Under Unix the kernel executes in the highest CPU level (modality) (also called *supervisor mode*), where everything is allowed.
- Under Unix applications execute int the lowest CPU level (modality) (*user mode*), where the processor regulates direct access to hardware and unauthorized access to memroy.
### Concurrency in the kernel
- **SMP**: Symmetric Multiprocessor
- Linux kernel code, (including driver code), must be *reentrant*--it must be capable of running in more than one context at the same time.
### The Current Process
- Kernel code can refer to the current process by accessing the global item `current`, defined in `<asm/current.h>`, which yeilds a pointer to `struct task_struct`, defined by `<linux/sched.h>`.
- During the execution of a system call, such as `open` or `read`, the current process is the one that invoked the call.
- Example: `printk(KERN_INFO "The process is \"%s\" (pid %i)\n", current->comm, current->pid);`
### A Few Other Details
- The kernel has a very small stack; it can be as small as a single 4096 byte page.
### Compiling Modules
