# Notes

[TOC]

## Math
### [Trigonometry](math/trig.md)
#### [Trigonometic Identites](math/trig.md#trigonometric-identities)
#### [Conics](math/trig.md#conics)
#### [Logic Symbols](math/trig.md#logic-symbols)
#### [Set Theory](math/trig.md#set-theory)
#### [Exponent and Logarithm Properties](math/trig.md#exponent-and-logarithm-properties)
### [Matrices](math/linearalgebra.md)
#### [Matrix Row Operations](math/linearalgebra.md#matrix-row-operations)
#### [Miscellaneous](math/linearalgebra.md#miscellaneous)
#### [Vector Operations](math/linearalgebra.md#vector-operations)
#### [Tensors](math/linearalgebra.md#tensors)
### [Calculus](math/calc.md)
#### [Derivatives](math/calc.md#derivatives)
#### [Integrals](math/calc.md#integrals)
#### [Differential Equations](math/calc.md#differential-equations)
##### [Separation of Variables](math/calc.md#separation-of-variables)
##### [Integrating Factor](math/calc.md#integrating-factor)
## Physics
### [Statics](math/physics1.md)
#### [Laws of Motion](math/physics1.md#laws-of-motion)
### [Dynamics](math/physics2.md)
#### [Determining Velocity and Acceleration from Position](math/physics2.md#determining-velocity-and-acceleration-from-position-1)
#### [Determining Position from Velocity and Velocity from Acceleration](math/physics2.md#determining-position-from-velocity-and-velocity-from-acceleration)
#### [Equations of Motion for Constant Acceleration along a Straight Line](math/physics2.md#equations-of-motion-for-constant-acceleration-along-a-straight-line)
#### [Alternate Relationship between Velocity and Acceleration](math/physics2.md#alternate-relationship-between-velocity-and-acceleration)
#### [Newton's Second Law of Motion](math/physics2.md#newtons-second-law-of-motion)
#### [Sliding Friction](math/physics2.md#sliding-friction)
#### [Determining Velocity from Position](math/physics2.md#determining-velocity-from-position)
#### [Determining Position from Velocity](math/physics2.md#determining-position-from-velocity)
#### [Determining Acceleration from Velocity or Position](math/physics2.md#determining-acceleration-from-velocity-or-position)
#### [Determining Velocity from Acceleration](math/physics2.md#determining-velocity-from-acceleration)
#### [Motion in a Plane in Space](math/physics2.md#motion-in-a-plane-in-space)
#### [Periodic Motion](math/physics2.md#periodic-motion)
#### [Kepler's Law](math/physics2.md#keplers-law)
#### [Simple Harmonic Motion](math/physics2.md#simple-harmonic-motion)
#### [Work, Energy, & Power](math/physics2.md#work-energy--power)
#### [Linear Momentum and Collisions](math/physics2.md#linear-momentum-and-collisions)
#### [Torque and Angular Momentum](math/physics2.md#torque-and-angular-momentum)
### [Electromagnetism](math/physics3.md)
#### [Gravitational/Electric Forces  & Fields](math/physics3.md#gravitationalelectric-forces--fields)
#### [Gravitation & Electric Potential](math/physics3.md#gravitation--electric-potential)
### [Special Relativity](math/physics4.md)
#### [Time Dilation](math/physics4.md#time-dilation)
#### [Length Contraction](math/physics4.md#length-contraction)
#### [Lorentz Transformation](math/physics4.md#lorentz-transformation)
#### [Velocity Transformation](math/physics4.md#velocity-transformation)
#### [Relativistic Momentum](math/physics4.md#relativistic-momentum)
#### [Relativistic Force](math/physics4.md#relativistic-force)
#### [Relativistic Energy](math/physics4.md#relativistic-energy)
#### [Mass Energy](math/physics4.md#mass-energy)
#### [Relativistic Translational  Kinetic Energy](math/physics4.md#relativistic-translational-kinetic-energy)
### [Units and Prefixes](math/physics5.md)
#### [SI Base Units](math/physics5.md#si-systéme-international-base-units)
#### [SI Derived Units](math/physics5.md#si-systéme-international-derived-units)
#### [SI Prefixes](math/physics5.md#si-systéme-international-prefixes)
## Programming
### General
#### Bitwise Operators
- AND (`&`)
  - |  A  |  B  | A&B |
    |:---:|:---:|:---:|
    |  1  |  1  |  1  |
    |  1  |  0  |  0  |
    |  0  |  1  |  0  |
    |  0  |  0  |  0  |

- OR (`|`)
  - |  A  |  B  | A\|B |
    |:---:|:---:|:---:|
    |  1  |  1  |  1  |
    |  1  |  0  |  1  |
    |  0  |  1  |  1  |
    |  0  |  0  |  0  |

- XOR (`^`)
  - |  A  |  B  | A^B |
    |:---:|:---:|:---:|
    |  1  |  1  |  0  |
    |  1  |  0  |  1  |
    |  0  |  1  |  1  |
    |  0  |  0  |  0  |

- NOT (`~`)
  - |  A  | ~A  |
    |:---:|:---:|
    |  1  |  0  |
    |  0  |  1  |

- Left Shift (`<<`)
  - Shift defined number of bits to the left.
  - Left bit shift of 1 is the same as multiplication by 2.
- Right Shift (`>>`)
  - Shift defined number of bits to the right.
  - Right bit shift of 1 is the same as division by 2 (and rounding down for odd integers).
#### Regular Expressions (REGEX)
##### Quantifiers
- | Quantifier | Description |
  | :--------: | :---------: |
  | `x\|y`     | Match either `x` or `y` |
  | `?`        | Match `0` or `1` times |
  | `+`        | Match `1` or more times |
  | `*`        | Match `0` or more times |
  | `{n}`      | Match `n` times |
  | `{n,}`     | Match `n` or more times |
  | `{n,m}`    | Match at least `n` times and match no more than `m` times |
  | `*?`       | Match `0` or more times, but stop after first match (non-greedy) |

##### Patterns
- | Pattern | Description |
  | :-----: | :---------: |
  | `[A-Z]`  | Match any uppercase character (range from `A` to `Z`) |
  | `[a-z]`  | Match any lowercase character (range from `a` to `z`) |
  | `[0-9]`  | Match any number (range from `0` to `9`) |
  | `[asdf]` | Match any character that is: `a`, `s`, `d`, or `f` |
  | `[^asdf]`| Match any character that **not**: `a`, `s`, `d`, or `f` |
  | `.`      | Match any character |
  | `\n`     | Match newline character |
  | `\t`     | Match tab character |
  | `\s`     | Match any whitespace character |
  | `\S`     | Match any **non**-whitespace character |
  | `\w`     | Match any word character (uppercase, lowercase,  numbers) |
  | `\W`     | Match any **non**-word character |
  | `^`      | Match beginning of a line |
  | `$`      | Match end of a line |

##### Capture Groups
- Any pattern  of characters enclosed in non-escaped paraenthese will be captured.
- There can be multiple captures in a single regular expression.
- The first capture can later be referenced by `$1` or `\1` depending upon the regex implimentation.
- The second capture will be `$2` or `\2` and so on.
#### 2's Complement
- For a given integer `x`:
  - `~x + 1 = -x`
  - Example, given `x = 13`:
    - ```
       13     = 0b00001101
      ~13     = 0b11110010
      ~13 + 1 = 0b11110011 = -13
  - Proof:
    - ```
      13 - 13 = 13 + (-13) = 0
        0b00001101 (13)
      + 0b11110011 (-13)
      ------------------
        0b00000000 (0)
- **Note:** 
  - For signed integers, a leading  bit of `1` indicates a negative number.
  - Therefore, a leading `0` means a positive (or zero) number.
### Algorithms
### Data Structures
#### [Data Structures In C](data_structures/data\ structures\ in\ C.md)
### Make
#### [The GNU Make Reference Manual](make/make.md)
### C
#### [C Notes](C/c.md)
### Go
#### [Go Notes](Go/go.md)
### COBOL
#### [COBOL Notes](COBOL/cobol.md)
#### [JCL Notes](COBOL/jcl.md)
### System Calls
- ```
  ╭────────────────────────────────────────────────╮
  │                  User Programs                 │
  ╰────────────────────────────────────────────────╯
  ╭────────────────────────────────────────────────╮
  │                 User Interface                 │
  ╰────────────────────────────────────────────────╯
  ╭────────────────────────────────────────────────╮
  │                  System Calls                  │
  ╰────────────────────────────────────────────────╯
  ╭────────────╮╭──────────╮╭──────────╮╭──────────╮
  │  Program   ││    I/O   ││   File   ││   Comms  │
  │  Contgrol  ││          ││  System  ││          │
  ╰────────────╯╰──────────╯╰──────────╯╰──────────╯
  ╭────────────╮╭──────────╮╭──────────╮╭──────────╮
  │    Error   ││ Resource ││ Auditing ││ Security │
  │ Management ││          ││          ││          │
  ╰────────────╯╰──────────╯╰──────────╯╰──────────╯
  ╭────────────────────────────────────────────────╮
  │                    Hardware                    │
  ╰────────────────────────────────────────────────╯
#### File Syscalls
  | Name | Description |
  |------|-------------|
  | read | Read from file descriptor into a buffer |
  | write | Write to a file descriptor |
  | open | Open and/or create a file |
#### Process Syscalls
  | Name | Description |
  |------|-------------|
  | close | Close a file |
  | fork | Creates a new process by duplicating the calling process |
  | exec | Loads and executes a new program in the current program |
  | wait | Suspends the calling process until one of its child processes terminates |
  | exit | Terminates the current process and returns the exit status to the parent process |
#### Memory Syscalls
  | Name | Description |
  |------|-------------|
  | brk | Sets the end of the program's data segment |
  | mmap | Maps files or devices into memory |
  | munmap | Unmaps files or devices from memory |
  | mprotect | Changes the protection attributes of a memory region |
#### IO Syscalls
  | Name | Description |
  |------|-------------|
  | read | See [File Syscalls](#file-syscalls) (Includes STDIN) |
  | write | See [File Syscalls](#file-syscalls) (Includes STDOUT and STDERR) |
#### Network Syscalls
  | Name | Description |
  |------|-------------|
  | socket | Creates a new communication endpoint (socket) |
  | connect | Initiates a connection to a remote socket |
  | bind | Associates a socket with a local address |
  | send | Sends data over a socket |
  | recv | Receives data over a socket |

#### System Information Syscalls
  | Name | Description |
  |------|-------------|
  | getpid | Retrieves the PID of the calling process |
  | getuid | Retrieves the UID of the calling process |
  | uname | Retrieves the system name and information |
## Files
### Permissions
#### Octals
- An octal is just a number but instead of base 10, as normal, it is base 8.
- The conversion is made just like in hex, but the base of the exponent expression is 8 instead of 16.
- For hexadecimal, the number `539` would be converted to decimal as follows:
  - ```
       16**2      16**1      16**0
         5          3          9
    --------------------------------
    = 5(16**2) + 3(16**1) + 9(16**0)
    =   1280   +   48     +    9
    = 1337 (base 10)
- For octal, the number `2471` would be converted to decimal as follows:
  - ```
       8**3      8**2      8**1      8**0
        2         4         7         1
    ---------------------------------------
    = 2(8**3) + 4(8**2) + 7(8**1) + 1(8**0)
    = 1024    +   256   +    56   +    1
    = 1337 (base 10)
- Octals are primarialy used within the scope of Linux/Unix file permissions:
  - ![Linux File Permissions](fileperms.png)
  - Where the highest possible value (`rwx`) is a full octal (`4 + 2 + 1 = 7`)
## Networking
### Network Protocols
- [TCP/IP Guide](http://www.tcpipguide.com/free/index.htm)
- ![Network Protocols](networkprotocols.jpg)
### IP Addresses and Subnetting Explained (CIDR)
- We will be using the following addresses:
    - Laptop:      `172.16.1.200`
    - PC:          `172.16.1.201`
    - Subnet Mask: `255.255.255.0`
    - Gateway:     `172.16.1.1`
- This network spans the IP addresses `172.16.1.0` - `172.16.1.255`
  - NOTE: `172.16.1.0` This is part of the network but is not usable, this is the **lowest** possible address (**Network ID**)
  - NOTE: `172.16.1.1` This is part of the network but is not usable, this is the **highest** possible address (**Broadcast**)
    - Any traffic sent to the Boradcast address will be sent to all hosts on this network
    - A valid Broadcast address is from left to right a continuous sequence of 1's followed by a continuous sequence of 0's
- This network range can also be represented in **CIDR** notation: `172.16.1.1/24`
  - In this case `172.16.1.1` is the Gateway, but really the Gateway just has to be an IP address in teh useable part of the network.
  - `/24` This is the **Network Bit** (**Network Prefix**)
    - This implies that the **Host Bit** (**Host Identifier**) is 8
      - This is because all IPv4 addresses are 32 bits, therefore: 24 + 8 = 32
- IPv4 addresses are in the form `xx.xx.xx.xx`, this is a 32 bit address, eacho of the four 'chunks' are separated by dots.  These are then essentially bytes, but are more often refered to as 'octets'.  Just two different names for a series of 8 bits, and a 32 bit IP address can also be thought of just a 4 Byte number.
- Using this **CIDR** notation we can determine the scop of the entire network using the process below:
  - Given: `172.16.1.1/24`
    - First thing to do is to **calculate the Subnet Mask**, this is done by starting with an empty IP address (Written out in binary):
    `00000000.00000000.00000000.00000000`
    And then start flipping on the **Network Bits** (/24) going from left to right:
    `11111111.11111111.11111111.00000000`
    This is the subnet mask in Binary, now **convert it back to decimal**:
    `255.255.255.0`
    This is the **Subnet Mask**
    - Next, **calculate the Network ID**:
    First take the IP address from the CIDR notation and **convert that to Binary** (172.16.1.1):
    `10101100.00010000.00000001.00000001`
    Now do a [bitwise](#bitwise-operators) **AND** on this binary address and the binary address of the Subnet Mask:
      - `10101100.00010000.00000001.00000001` IP Address in the range
      - `11111111.11111111.11111111.00000001` Subnet Mask for the range
      - `10101100.00010000.00000001.00000000` **Network ID** (172.16.1.0)
    - Next, **calculate the Broadcast Address**:
    Take the Network ID you just calculated and flip all of the bits in the **Host Bit** part of the address to 1. This is equivilant to the [bitwise](#bitwise-operators) **OR** of an IP address in range and the [bitwise](#bitwise-operators) **NOT** of the Subnet Mask:
      - `10101100.00010000.00000001.00000001` IP Address in the range
      - `00000000.00000000.00000000.11111111` **NOT** of Subnet Mask for the range
      - `10101100.00010000.00000001.11111111` **Broadcast** (172.16.1.255)
    - Now you have all needed information to define the scope of the network:
      - **Network ID**:  `172.16.1.0`
      - **Broadcast**:   `172.16.1.255`
      - **Subnet Mask**: `172.16.1.255`
    - `/24`, `/16`, and `/8` networks are nice because they are segmented evenly along one of the octet (byte) divisions.  But Network Bits that are not divisible by 8 are also valid, as long as they are in the range of 0 - 32.  See below for some examples:
    ```
      ipcalc 172.16.1.1/30
      Address:   172.16.1.1           10101100.00010000.00000001.000000 01
      Netmask:   255.255.255.252 = 30 11111111.11111111.11111111.111111 00
      Wildcard:  0.0.0.3              00000000.00000000.00000000.000000 11
      =>
      Network:   172.16.1.0/30        10101100.00010000.00000001.000000 00
      HostMin:   172.16.1.1           10101100.00010000.00000001.000000 01
      HostMax:   172.16.1.2           10101100.00010000.00000001.000000 10
      Broadcast: 172.16.1.3           10101100.00010000.00000001.000000 11
      Hosts/Net: 2                     Class B, Private Internet
    ```

    ```
    ipcalc 172.16.1.1/11
    Address:   172.16.1.1           10101100.000 10000.00000001.00000001
    Netmask:   255.224.0.0 = 11     11111111.111 00000.00000000.00000000
    Wildcard:  0.31.255.255         00000000.000 11111.11111111.11111111
    =>
    Network:   172.0.0.0/11         10101100.000 00000.00000000.00000000
    HostMin:   172.0.0.1            10101100.000 00000.00000000.00000001
    HostMax:   172.31.255.254       10101100.000 11111.11111111.11111110
    Broadcast: 172.31.255.255       10101100.000 11111.11111111.11111111
    Hosts/Net: 2097150               Class B, In Part Private Internet
    ```
### BGP
- ![An Introduction to BGP](https://quantum5.ca/2023/07/14/introduction-to-bgp-from-operator-of-small-as/)
## Operating Systems
### Linux
#### Device Drivers
- [Device Driver Notes](linux/devicedrivers.md)
#### Distributions
- ![Linux Distributions](distros.svg)
#### Commands
- `tac` Cat a file backwards (bottom to top)
- `grep -P` Gives you [PCRE (Perl Compatable Regular Expression)](#regex) matching
```
lsof | grep -P "^.*bash.*\d+.*$"
bash       38             jgiberson  cwd       DIR   8,32     4096   261 /home/jgiberson
bash       38             jgiberson  rtd       DIR   8,32     4096     2 /
bash       38             jgiberson  txt       REG   8,32  1265648  7609 /usr/bin/bash
bash       38             jgiberson  mem       REG   8,32  3048928  6108 /usr/lib/locale/locale-archive
bash       38             jgiberson  mem       REG   8,32  1922136 78972 /usr/lib/x86_64-linux-gnu/libc.so.6
bash       38             jgiberson  mem       REG   8,32   204088  8530 /usr/lib/x86_64-linux-gnu/libtinfo.so.6.4
bash       38             jgiberson  mem       REG   8,32    27028 79237 /usr/lib/x86_64-linux-gnu/gconv/gconv-modules.cache
bash       38             jgiberson  mem       REG   8,32   210968 78969 /usr/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
bash       38             jgiberson    0u      CHR  136,0      0t0     3 /dev/pts/0
bash       38             jgiberson    1u      CHR  136,0      0t0     3 /dev/pts/0
bash       38             jgiberson    2u      CHR  136,0      0t0     3 /dev/pts/0
bash       38             jgiberson  255u      CHR  136,0      0t0     3 /dev/pts/0
```
- `wc` Count lines, words, characters in a file or PIPE
- `cut` Parse a file or PIPE based on a given delimiter. i.e.:
```
echo "1.2.3.4" | cut -d "." -f1
1

echo "1.2.3.4" | cut -d "." -f2
2

echo "1.2.3.4" | cut -d "." -f3
3

echo "1.2.3.4" | cut -d "." -f4
```
- `cut` Parse a file or PIPE based on a given character position. i.e.:
```
echo "1.2.3.4" | cut -c 2-5
.2.3

echo "1.2.3.4" | cut -c -5
1.2.3

echo "1.2.3.4" | cut -c 2-
.2.3.4
```
- `sed` Useful for a bunch of stuff but this example is capturing data between two patterns over multiple lines:
```
cat test
dsf
dsaf
ew
43
pattern starts here
a
23
23
4r

43
3f4
pattern ends here
afd
a
s

sed -n '/pattern/,/pattern/p' test
pattern starts here
a
23
23
4r

43
3f4
pattern ends here
```
- `which` Show the location of a file from the PATH variable:
```
which python3
/usr/bin/python3
```
- `readlink -f` Follow all links back to the original file location. Really useful when combined with the which command for things like java which tend to be a nested mess of symlinks:
```
readlink -f `which python3`
/usr/bin/python3.11
```
- `lsof` List open files or show what processes have a certain file open:
```
lsof /usr/bin/rsync
COMMAND PID      USER  FD   TYPE DEVICE SIZE/OFF  NODE NAME
rsync   797 jgiberson txt    REG   8,32   510344 37016 /usr/bin/rsync
rsync   798 jgiberson txt    REG   8,32   510344 37016 /usr/bin/rsync
rsync   799 jgiberson txt    REG   8,32   510344 37016 /usr/bin/rsync
```
- `fuser` Another way to show what process is using a file:
```
fuser /usr/bin/rsync
/usr/bin/rsync:        797e   798e   799e
```
- `head` & `tail` Show 'x' amount of lines at beginning or end of a file (or follow the output of a growing file in the case of `tail -f`):
```
cat test
dsf
dsaf
ew
43
pattern starts here
a
23
23
4r

43
3f4
pattern ends here
afd
a
s

cat test | head -n 2 | tail -n 1
dsaf
```
- `find` **1)** Find everything under `pwd` that has been modified less than 2 days ago. **2)** Find files modified less than 2 days ago. **3)** Find directories modified less than 2 days ago. **4)** Find file modified more than 7 days ago:
```
find . -mtime -2
.
./tmpFile
./tmpDir

find . -mtime -2 -type f
./tmpFile

find . -mtime -2 -type d
.
./tmpDir

find . -mtime +7
./long_file
./a
./eol
./file_list
./tabs
```
- `nl` Number lines:
```
cat test
dsf
dsaf
ew
43
pattern starts here
a
23
23
4r

43
3f4
pattern ends here
afd
a
s

cat test | nl
     1  dsf
     2  dsaf
     3  ew
     4  43
     5  pattern starts here
     6  a
     7  23
     8  23
     9  4r

    10  43
    11  3f4
    12  pattern ends here
    13  afd
    14  a
    15  s
 ```
- `less -i` Just like normal less but it allows you to do case insensitive search.
  - `less`/`vi` commands:
    - `/SEARCH` Searches from top to bottom
    - `?SEARCH` Searches from bottom to top
    - `n` Goes to the next search match
    - `N` Goes to the next search match, but in the opposite direction
- `vim` Run `vimtutor` if its installed, that will cover all the basics
- `diff` Show the difference between files:
```
diff test test2
1d0
< dsf
2a2
> dsf
6c6
< a
---
> b
9d8
< 4r
```
- `vimdiff` A more visual, highlighted, version of diff.  Also, these files are opened in `vim` so they can be edited on the fly:
- `sdiff` Side by side diff.  Good alternative if you don't have access to `vimdiff`:
```
sdiff test test2
dsf                                                           <
dsaf                                                            dsaf
                                                              > dsf
ew                                                              ew
43                                                              43
pattern starts here                                             pattern starts here
a                                                             | b
23                                                              23
23                                                              23
4r                                                            <

43                                                              43
3f4                                                             3f4
pattern ends here                                               pattern ends here
afd                                                             afd
a                                                               a
s                                                               s
```
- `find` Show what is linked to a given file:
```
ls -l
total 28
lrwxrwxrwx 1 jgiberson jgiberson     1 Jul  5 08:47 a -> b
lrwxrwxrwx 1 jgiberson jgiberson     1 Jul  5 08:47 b -> c
lrwxrwxrwx 1 jgiberson jgiberson     1 Jul  5 08:47 c -> d
-rw-r--r-- 1 jgiberson jgiberson     0 Jul  5 08:47 d
-rw-r--r-- 1 jgiberson jgiberson 17403 Jul  5 08:44 README.md
-rw-r--r-- 1 jgiberson jgiberson    80 Jul  5 08:32 test
-rw-r--r-- 1 jgiberson jgiberson    77 Jul  5 08:45 test2

find . -lname d
./c

find . -lname c
./b

find . -lname b
./a
```
- `pstree` Show a hierarchy view of running processes:
```
pstree -pug
init(Debian)(1,)─┬─Relay(11)(10,8)───cron(35,35)
                 ├─SessionLeader(36,36)───Relay(38)(37,36)───bash(38,38,jgiberson)───pstree(1274,1274)
                 ├─init(5,)───{init}(6,)
                 └─{init(Debian)}(7,)
```
- `strace` Show syscalls from a particular process (can also be attached to a currently running process if needed, see `man strace`) (Also can look a `ltrace`, basically the same syntax but it traces library calls instead of system calls):
  - Use `man syscalls` for a list of all possible [system calls](#systemcalls)
  - Use `man <syscall>` for a description of the function for a specific [system call](#systemcalls)
```
strace -x -tt -s 9999 -f cat test
08:52:21.567721 execve("/usr/bin/cat", ["cat", "test"], 0x7ffe676ab050 /* 29 vars */) = 0
08:52:21.568893 brk(NULL)               = 0x55c7ad0ac000
08:52:21.569400 mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f510fbd2000
08:52:21.569912 access("/etc/ld.so.preload", R_OK) = -1 ENOENT (No such file or directory)
08:52:21.570507 openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
08:52:21.570937 newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=52942, ...}, AT_EMPTY_PATH) = 0
08:52:21.571353 mmap(NULL, 52942, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f510fbc5000
08:52:21.571662 close(3)                = 0
08:52:21.571944 openat(AT_FDCWD, "/lib/x86_64-linux-gnu/libc.so.6", O_RDONLY|O_CLOEXEC) = 3
08:52:21.572338 read(3, "\x7f\x45\x4c\x46\x02\x01\x01\x03\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x3e\x00\x01\x00\x00\x00\x90\x73\x02\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x58\x44\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x40\x00\x38\x00\x0e\x00\x40\x00\x40\x00\x3f\x00\x06\x00\x00\x00\x04\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x10\x03\x00\x00\x00\x00\x00\x00\x10\x03\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x38\x53\x02\x00\x00\x00\x00\x00\x38\x53\x02\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x05\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x3c\x4c\x15\x00\x00\x00\x00\x00\x3c\x4c\x15\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\xee\x2b\x05\x00\x00\x00\x00\x00\xee\x2b\x05\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x06\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x98\x4f\x00\x00\x00\x00\x00\x00\x80\x26\x01\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x06\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x44\x00\x00\x00\x00\x00\x00\x00\x44\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x04\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x90\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x53\xe5\x74\x64\x04\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x50\xe5\x74\x64\x04\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x14\x74\x00\x00\x00\x00\x00\x00\x14\x74\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x51\xe5\x74\x64\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x52\xe5\x74\x64\x04\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x30\x37\x00\x00\x00\x00\x00\x00", 832) = 832
08:52:21.573085 pread64(3, "\x06\x00\x00\x00\x04\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x10\x03\x00\x00\x00\x00\x00\x00\x10\x03\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x38\x53\x02\x00\x00\x00\x00\x00\x38\x53\x02\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x05\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x3c\x4c\x15\x00\x00\x00\x00\x00\x3c\x4c\x15\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\xee\x2b\x05\x00\x00\x00\x00\x00\xee\x2b\x05\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x06\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x98\x4f\x00\x00\x00\x00\x00\x00\x80\x26\x01\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x06\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x44\x00\x00\x00\x00\x00\x00\x00\x44\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x04\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x90\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x53\xe5\x74\x64\x04\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x50\xe5\x74\x64\x04\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x14\x74\x00\x00\x00\x00\x00\x00\x14\x74\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x51\xe5\x74\x64\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x52\xe5\x74\x64\x04\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x30\x37\x00\x00\x00\x00\x00\x00\x30\x37\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00", 784, 64) = 784
08:52:21.573655 newfstatat(3, "", {st_mode=S_IFREG|0755, st_size=1922136, ...}, AT_EMPTY_PATH) = 0
08:52:21.574032 pread64(3, "\x06\x00\x00\x00\x04\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x10\x03\x00\x00\x00\x00\x00\x00\x10\x03\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x00\x04\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\xf0\x0a\x1a\x00\x00\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x1c\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x38\x53\x02\x00\x00\x00\x00\x00\x38\x53\x02\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x05\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x00\x60\x02\x00\x00\x00\x00\x00\x3c\x4c\x15\x00\x00\x00\x00\x00\x3c\x4c\x15\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x04\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\x00\xb0\x17\x00\x00\x00\x00\x00\xee\x2b\x05\x00\x00\x00\x00\x00\xee\x2b\x05\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x06\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x98\x4f\x00\x00\x00\x00\x00\x00\x80\x26\x01\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x06\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x60\x1b\x1d\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x04\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x70\x03\x00\x00\x00\x00\x00\x00\x44\x00\x00\x00\x00\x00\x00\x00\x44\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x04\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x90\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x53\xe5\x74\x64\x04\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x50\x03\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x20\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x50\xe5\x74\x64\x04\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x0c\x0b\x1a\x00\x00\x00\x00\x00\x14\x74\x00\x00\x00\x00\x00\x00\x14\x74\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x51\xe5\x74\x64\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x52\xe5\x74\x64\x04\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\xd0\xe8\x1c\x00\x00\x00\x00\x00\x30\x37\x00\x00\x00\x00\x00\x00\x30\x37\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00", 784, 64) = 784
08:52:21.574517 mmap(NULL, 1970000, PROT_READ, MAP_PRIVATE|MAP_DENYWRITE, 3, 0) = 0x7f510f9e4000
08:52:21.574870 mmap(0x7f510fa0a000, 1396736, PROT_READ|PROT_EXEC, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x26000) = 0x7f510fa0a000
08:52:21.575258 mmap(0x7f510fb5f000, 339968, PROT_READ, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x17b000) = 0x7f510fb5f000
08:52:21.575566 mmap(0x7f510fbb2000, 24576, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x1ce000) = 0x7f510fbb2000
08:52:21.575951 mmap(0x7f510fbb8000, 53072, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x7f510fbb8000
08:52:21.576305 close(3)                = 0
08:52:21.576701 mmap(NULL, 12288, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f510f9e1000
08:52:21.577141 arch_prctl(ARCH_SET_FS, 0x7f510f9e1740) = 0
08:52:21.577451 set_tid_address(0x7f510f9e1a10) = 1501
08:52:21.577745 set_robust_list(0x7f510f9e1a20, 24) = 0
08:52:21.578013 rseq(0x7f510f9e2060, 0x20, 0, 0x53053053) = 0
08:52:21.578501 mprotect(0x7f510fbb2000, 16384, PROT_READ) = 0
08:52:21.579101 mprotect(0x55c7ab8dc000, 4096, PROT_READ) = 0
08:52:21.579540 mprotect(0x7f510fc04000, 8192, PROT_READ) = 0
08:52:21.579941 prlimit64(0, RLIMIT_STACK, NULL, {rlim_cur=8192*1024, rlim_max=RLIM64_INFINITY}) = 0
08:52:21.580342 munmap(0x7f510fbc5000, 52942) = 0
08:52:21.580700 getrandom("\x3c\x47\x9d\xa1\x56\xf1\x81\xf2", 8, GRND_NONBLOCK) = 8
08:52:21.581091 brk(NULL)               = 0x55c7ad0ac000
08:52:21.581417 brk(0x55c7ad0cd000)     = 0x55c7ad0cd000
08:52:21.581850 openat(AT_FDCWD, "/usr/lib/locale/locale-archive", O_RDONLY|O_CLOEXEC) = 3
08:52:21.582177 newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=3048928, ...}, AT_EMPTY_PATH) = 0
08:52:21.582492 mmap(NULL, 3048928, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7f510f6f8000
08:52:21.582862 close(3)                = 0
08:52:21.583224 newfstatat(1, "", {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0), ...}, AT_EMPTY_PATH) = 0
08:52:21.583578 openat(AT_FDCWD, "test", O_RDONLY) = 3
08:52:21.584067 newfstatat(3, "", {st_mode=S_IFREG|0644, st_size=80, ...}, AT_EMPTY_PATH) = 0
08:52:21.584493 fadvise64(3, 0, 0, POSIX_FADV_SEQUENTIAL) = 0
08:52:21.584953 mmap(NULL, 139264, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f510f6d6000
08:52:21.585359 read(3, "dsf\ndsaf\new\n43\npattern starts here\na\n23\n23\n4r\n\n43\n3f4\npattern ends here\nafd\na\ns\n", 131072) = 80
08:52:21.585798 write(1, "dsf\ndsaf\new\n43\npattern starts here\na\n23\n23\n4r\n\n43\n3f4\npattern ends here\nafd\na\ns\n", 80dsf
dsaf
ew
43
pattern starts here
a
23
23
4r

43
3f4
pattern ends here
afd
a
s
) = 80
08:52:21.586234 read(3, "", 131072)     = 0
08:52:21.586547 munmap(0x7f510f6d6000, 139264) = 0
08:52:21.586867 close(3)                = 0
08:52:21.587258 close(1)                = 0
08:52:21.587636 close(2)                = 0
08:52:21.588089 exit_group(0)           = ?
08:52:21.588542 +++ exited with 0 +++
```
![Userspace and Kernelspace](userspace.png "Userspace & Kernelspace")
- `uniq` Remove duplicate adjacent entries, good command to pair with the sort command:
```
cat test3
x
x
a
b
c
d
x
x
x
y
z
1
2

cat test3 | uniq
x
a
b
c
d
x
y
z
1
2

cat test3 | sort | uniq
1
2
a
b
c
d
x
y
z
```
- `lsof` Another way to use `lsof` to specifically show only network connections on the system (probably will need **root** access for this):
- `!!` Repeat last command executed, really useful when you forget to `sudo` something:
- `!n` Repeat the *n'th* command ran, i.e. then index of the command that is shown via the `history` command:
- `/etc/fstab` File that shows filesystems/partitions that should be mounted upon system boot
- `/etc/mtab` File that shows the currently mounted filesystems/partitions
- `/proc/cpuinfo` File that shows info regarding the systems CPU's
- `/etc/hosts` File for IP to hostname mapping
- `/etc/*release` File that states what version the OS/System is (filename can vary)
- `/etc/sudoers` File that controls what users have `sudo` access and what that user has `sudo` access to
- `grep --color=auto` Can also be useful with color highlighting for pattern matches:
- `free` Show memory usage on the system:
- `top` Show resource usage on the system (memory, processor, etc)
- `apropos` Command to search thru `man` pages:
- `man` Some commands exist in different sections of the `man` pages. Example: This command `man readlink` will return a different man page than this command `man 2 readlink`.  By default the first command uses section 1, which is for executables and shell commands but it also exists in section 2 which is for syscalls.  See below for a list or reference `man`'s manpage (`man man`):
- `rpm` When installing software I like to use the `-ivF` switch, this will give you a visual output and only upgrade the package if it is of a newer version then the one currently installed.
- `rpm2cpio` - Useful if you want to inspect the contents of an rpm before installing. Note: This syntax will dump the contents into your current directory:
- `vim` You probably already know vim some... (see `vimtutor`), `vim` can also be used to inspect compressed and archived files like: `zip`, `tar`, `gz`, `bz`, `jar`, etc.  You can use `vim` to drill down into the file structure of an archived file and open and individual file without ever having to manually extract the file.
- Manual way of calculating fingerprint for a public key:
- `chown` Change ownership of a file (user and group):
- `du` Disk usage, show size of files in a particular directory:
- `cal` Quick calendar reference
- `expect` Good scripting language to know, really good at duct taping thing together quickly.
  - [Expect Wikipedia](https://en.wikipedia.org/wiki/Expect)
- `file` Display what type of file something is
- Brace Expansion: Generate a sequence of numbers or characters.  Similar to the `seq` command, can be useful for loops:
```
echo {1..13}
1 2 3 4 5 6 7 8 9 10 11 12 13

echo {a..e}
a b c d e

echo {r..D}
r q p o n m l k j i h g f e d c b a ` _ ^ ]  [ Z Y X W V U T S R Q P O N M L K J I H G F E D

echo {R..d}
R S T U V W X Y Z [  ] ^ _ ` a b c d
```
- `xxd` Does a hex dump of a file.  Can also be used to dump in binary and octal formats as well:
- `split` Split a file into multiple files, in this case 10 byte files.  Useful if you need to break up a large file for some reason, like trying to fit it onto multiple CDs:
- `w` & `who` Shows who has a session on the system:
- Edit previous command: Useful for when you make typos:
```
ls -a code/my-configs/
.  ..  .bashrc  .git  README.md  .vimrc

^my-configs/^notes
ls -a code/notes
.  ..  a  b  c  d  .git  README.md  test  test2  test3  userspace.png
```
- Create a tcp (telnet) connection when you don't have the telnet binary installed:
  - `cat </dev/tcp/[ip]/[port]`
  - Example: Connectiong to a telnet server with the IP of 192.168.1.2:
    - `cat </dev/tcp/192.168.1.2/23
- `ipcalc [IP ADDR]/[PREFIX] -nmb` or `ipcalc [IP ADDR] [SUBNET MASK] -nmb`
  - Displays the Subnet Mask, Network ID, and Broadcast ID for the given network segment
- `objdump -d` Run a binary and display the assembly operations:
```
objdump -d /usr/bin/cat test | head -n 30

/usr/bin/cat:     file format elf64-x86-64


Disassembly of section .init:

0000000000002000 <.init>:
    2000:       48 83 ec 08             sub    $0x8,%rsp
    2004:       48 8b 05 bd 8f 00 00    mov    0x8fbd(%rip),%rax        # afc8 <__cxa_finalize@plt+0x8c38>
    200b:       48 85 c0                test   %rax,%rax
    200e:       74 02                   je     2012 <free@plt-0x1e>
    2010:       ff d0                   call   *%rax
    2012:       48 83 c4 08             add    $0x8,%rsp
    2016:       c3                      ret

Disassembly of section .plt:

0000000000002020 <free@plt-0x10>:
    2020:       ff 35 ca 8f 00 00       push   0x8fca(%rip)        # aff0 <__cxa_finalize@plt+0x8c60>
    2026:       ff 25 cc 8f 00 00       jmp    *0x8fcc(%rip)        # aff8 <__cxa_finalize@plt+0x8c68>
    202c:       0f 1f 40 00             nopl   0x0(%rax)

0000000000002030 <free@plt>:
    2030:       ff 25 ca 8f 00 00       jmp    *0x8fca(%rip)        # b000 <__cxa_finalize@plt+0x8c70>
    2036:       68 00 00 00 00          push   $0x0
    203b:       e9 e0 ff ff ff          jmp    2020 <free@plt-0x10>

0000000000002040 <abort@plt>:
    2040:       ff 25 c2 8f 00 00       jmp    *0x8fc2(%rip)        # b008 <__cxa_finalize@plt+0x8c78>
    2046:       68 01 00 00 00          push   $0x1
```
- `alias` Creates an alias for another command:
- `dirname` Return just the directory name from a file path:
- `basename` Return just the filename from a file path:
- `realpath` Show the full path, filename included:
- `rename` Rename file.  Better than `mv` for several files as it supports regex matching:
  - `rename [text to be replaced] [new text] [pattern of filename to match]
- `lsattr` Shows attributes of files:
- `chattr` Change file attributes
- `chmod` Change file permissions. See [Octals](#octals)
### BSD
#### Distributions
- ![BSD Distributions](bsd.svg)
### Windows
## Misc
### Config Settings
#### `.bashrc`
- ```bash
    function gomac() {
        GOOS=darwin    GOARCH=amd64 go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_mac
    }

    function gosol() {
        GOOS=solaris   GOARCH=amd64 go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_solaris
    }

    function gobsd() {
        GOOS=dragonfly GOARCH=amd64 go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_dragonflyBSD
        GOOS=freebsd   GOARCH=amd64 go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_freeBSD
        GOOS=netbsd    GOARCH=amd64 go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_netBSD
        GOOS=openbsd   GOARCH=amd64 go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_openBSD

    }

    function goarm() {
        GOOS=linux GOARM=5 GOARCH=arm go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_arm5
        GOOS=linux GOARM=6 GOARCH=arm go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_arm6
        GOOS=linux GOARM=7 GOARCH=arm go build -o `cat ./go.mod | head -n 1 | awk '{ print $2 }'`_arm7

    }

    function wait() {
        ctr=$1
        while [ $ctr -gt 0 ]
        do
            echo -ne "\b\b\b\b\b     \b\b\b\b\b$ctr"
            sleep 1
            ctr=$(( $ctr - 1 ))
        done
        echo -ne "\b\b\b\b\b     \b\b\b\b\b"
    }

    function mirrorShell() {
        tty=pts/$1
        echo "Begin shell mirror of $tty"
        sudo peekfd -8cnd $(ps -fat | grep "$tty *Ss" | awk '{print$1}') 0 1 2
    }

    function cCompile() {
        filename=$1
        file=`echo $filename | cut -d "." -f1`
        #gcc -o $file $filename
        gcc -Wall -pedantic -ansi -std=gnu11 -o $file $filename
    }

    function cWCompile() {
        filename=$1
        file=`echo $filename | cut -d "." -f1`
        x86_64-w64-mingw32-gcc  -Wall -pedantic -ansi -std=gnu11 -o $file $filename
    }

    alias cc='cCompile'
    alias ccw='cWCompile'

    alias gowin='GOOS=windows GOARCH=amd64 go build'
    alias goall='gowin && gomac && gosol && gobsd && goarm && go build'

    alias grep='grep --color=auto'
    alias ls='ls --color=auto'

#### `.vimrc`
- ```vim
  call plug#begin()
  Plug 'mattn/emmet-vim'
  Plug 'mhartington/oceanic-next'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'jcherven/jummidark.vim'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'jremmen/vim-ripgrep'
  Plug 'preservim/nerdtree'
  Plug 'ryanoasis/vim-devicons'
  Plug 'metakirby5/codi.vim'
  Plug 'preservim/nerdcommenter'
  Plug 'tpope/vim-surround'
  Plug 'APZelos/blamer.nvim'
  call plug#end()
  colorscheme habamax
  set encoding=UTF-8
  set guifont=Cousine\ Nerd\ Font,\ Regular
  nmap <F6> :NERDTreeToggle<CR>
  nmap <C-e> :NERDTreeToggle<CR>
  nmap <C-E> :NERDTreeToggle<CR>
  map <C-Up> :m -2<CR>
  map <C-k> :m -2<CR>
  map <C-Down> :m +1<CR>
  map <C-j> :m +1<CR>
  nnoremap d "_d
  nnoremap c d
  nnoremap C D
  vnoremap c d
  nnoremap x "_x
  nmap <C-_>   <Plug>NERDCommenterToggle
  vmap <C-_>   <Plug>NERDCommenterToggle<CR>gv
  set ignorecase
  set nu
  let g:blamer_enabled = 1
  let g:blamer_delay = 500
  runtime macros/matchit.vim

  set expandtab
  set shiftround
  set shiftwidth=4
  set smarttab
  set tabstop=4
  set hlsearch
  set ignorecase
  set linebreak
  syntax enable
  set wrap
  set ruler
  set noerrorbells
  set title
  set history=1000
  set ffs=unix
  set encoding=utf-8
  set fileencoding=utf-8
  filetype indent on
  set number
  set colorcolumn=120
  colorscheme habamax
  set showcmd
  set wildmenu
  " Allow crosshair cursor highlighting.
  highlight CursorLine   cterm=NONE ctermbg=0
  highlight CursorColumn cterm=NONE ctermbg=0
  set cursorline! cursorcolumn!
  set backup
  set backupdir=~/.vim/backup
  set directory=~/.vim/tmp
  set smartcase
  set showmatch
  set ttyfast
#### Useful Unicode
##### Box Drawing

- Double
    ```
    ╔═╦═╗
    ╠═╬═╣
    ║ ║ ║
    ╚═╩═╝
    ```

- Thick
    ```
    ┏━┳━┓
    ┣━╋━┫
    ┃ ┃ ┃
    ┗━┻━┛
    ```

- Thin
    ```
    ┌─┬─┐
    ├─┼─┤
    │ │ │
    └─┴─┘
    ```

- Round
    ```
    ╭─┬─╮
    ├─┼─┤
    │ │ │
    ╰─┴─╯
    ```

- Diaganol
    ```
     ╳
    ╱ ╲
    ╲ ╱
     ╳
    ╱ ╲
    ```
- Example
    ```
    ┌─┬┐  ╔═╦╗  ╓─╥╖  ╒═╤╕
    │ ││  ║ ║║  ║ ║║  │ ││
    ├─┼┤  ╠═╬╣  ╟─╫╢  ╞═╪╡
    └─┴┘  ╚═╩╝  ╙─╨╜  ╘═╧╛
    ┌───────────────────┐
    │  ╔═══╗ Some Text  │▒
    │  ╚═╦═╝ in the box │▒
    ╞═╤══╩══╤═══════════╡▒
    │ ├──┬──┤           │▒
    │ └──┴──┘           │▒
    └───────────────────┘▒
    ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    ```
##### Greek
- Lower Case\
`α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ σ/ς τ υ φ χ ψ ω`

- Upper Case\
`Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ  Σ  Τ Υ Φ Χ Ψ Ω`

##### Math Symbols
`∀ ∁ ∂ ∃ ∄ ∅ ∆ ∇ ∈ ∉ ∊ ∋ ∌ ∍ ∎ ∏`\
`∐ ∑ − ∓ ∘ ∙ √ ∛ ∜ ∝ ∞ ∟`\
`∠ ∡ ∢ ∣ ∤ ∥ ∦ ∧ ∨ ∩ ∪ ∫ ∬ ∭ ∮ ∯`\
`∰ ∱ ∲ ∳ ∴ ∵`\
`≅ ≆ ≇ ≈ ≠ ≡ ≤ ≥ ≦ ≧ ≨ ≩ ≪ ≫`\
`⊂ ⊃ ⊄ ⊅ ⊆ ⊇ ⊈ ⊉`\
`ℝ ℂ ℕ ℤ`
##### Superscript
`⁰ ¹ ² ³ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹`
##### Subscript
`₀ ₁ ₂ ₃ ₄ ₅ ₆ ₇ ₈ ₉`
#### dBm to mW Conversion
x = Power in dBm\
P = Power in mW
```math
\begin{align*}
x =& 10log_{10}\left(\frac{P}{1mW}\right)\\
&\therefore\\
P =& 1mW \cdot 10^{\frac{x}{10}}
\end{align*}
```
This implies that you can never truely have `0` Watts when expressed in terms of dBm:
```math
\begin{align*}
\lim_{x\to-\infty}P(x) &= 10^{\frac{-\infty}{10}}\\
\lim_{x\to-\infty}P(x) &= \frac{1}{10^{\frac{\infty}{10}}}\\
\therefore\\
\lim_{x\to-\infty}P(x) &= 0
\end{align*}
```
Because to have `0` Watts you will need negative infinity dBm.
#### Cheat Sheets
- [https://cheat.sh/](https://cheat.sh/)
