# C Notes
- Notes from: *C Programming: A Modern Approach* by *K.N. King*

[TOC]

## Fundamentals
### Compiling and Linking
- **Preprocessing:** The program is first given to a **preprocessor**, which obeys commands that begin wiht # (known as **directives**). A preporcessor is a bit like an editior, it can add things to the program and make modifications.
- **Compiling:** The modified program now goes to a **compiler**, which translates it  into machine instructions(**object code**). The program is not yet ready to run.
- **Linking:** The final step, a **linker** combines the object code produced by the compiler with any additional code needed to yeild a complete executable program.  This additional code includes libarary function (like *printf*) that are used in the program.
### General Form
#### Directives
- Commands intended for the preprocessor are called directives.
- In the below example, the directive state that the information in `<stdio.h>` is to be "included" into the program before it is compilied.
- Example: `#include <stdio.h>`
- C has a number of **headers** like `<stdio.h>`; each contains information about some part of the standard library.
#### Functions
- **Functions** are l ike "procedures" or "subroutines" in other programming languages.
- Functions fall into two categories:
  - Those written by thhe programmer
  - Those provided as part of  the C "library" of functions that are supplied with the compiler.
#### Statements
- A **statement** is a command to be executed when the program runs.
#### Variables and Assignment
- A **variable** is a way for a program to temporarily store data during program execution.
##### Types
- Every variable must have a **type**, which specifies what kind of data it will hold.
##### Declarations
- Variables must be **declared** before they can be used.
- To declare a variable, we specify the *type* of the variable, then its *name*.
##### Assignment
- A variable can be given a value by means of **assignment**.
- Example:
```C
height = 8;
length = 12;
width  = 10;
volume = height * length * width;   /* volume is now 960 */
```
- The numbers `8`,`12`, and `10` are said the be **constants**.
- The right side of an assignment can be a formula (**expression**) involving constants, variables, and operators.
##### Initialization
- Some variables are  automatically set to zero when a program begins to execute, but most are not.
- A variable that doesn't have a default value and hasn't yet been assigned a value by the program is said to be **uninitialized**.
#### Reading Input
- Both `scanf` and `printf` require the use of a **format string** to specify the appearance of the input or output data.
  - `scanf` needs to know what form the input data will take.
  - `printf` needs to know how to display output data.
- Example: `scanf("%d", &i);   /* reads an integer; stores into i */`
- Example: `printf("%d\n", height * length * width);`
#### Defining Names for Constants
- Using the feature known as **macro definition**, we can name a constant:
- Example: `#define INCHES_PER_POUND 166`
- `#define` is a preprocessing directive, just as `#include` is, so there's no semicolon at the end of the line.
#### Identifiers
- Names for variables, functions, macros, and other entities are called **identifiers**.
- Identifier may  contain letters, digits, and underscores.
- Identifier **must** begin with witha a letter or underscore.
- C is **case-sensitive**.
#### Layout of a C Program
- You can think of a C program as a series of **tokens**: groups of characters that can't be split up without changing  their meaning.
- Identifiers and keywords are are tokens.
- So are operators like `+` and `-`, punctuation marks such as commas and semicolons, and  string literals.
- The statement: `printf("Height: %d\n", height);` consists of seven tokens:
  - ```
    printf    (    "Height: %d\n"    ,    height    )    ;
      ①       ②       ③            ④      ⑤      ⑥    ⑦
  - Tokens ① and ⑤ are identifiers, token ③ is a string literal, and tokens ②, ④, ⑥, and ⑦ are punctuation.
- C allows the insertion of any  amount of space (blanks, tabs, and new-line characters) between tokens.
  - *Statements can be divided* over any number of lines.
  - *Space between tokens* makes it easier for the eye to separate them.
  - *Indentation* can make nesting easier to spot.
  - *Blank lines* can divide a  program into logical units.
## Formatted Input/Output
### The `printf` Function
- The `printf` function is designed to display the contents of a string, known as the **format string**, with values possibly inserted at specified points in the string.
- `printf(string, expr1, expr2, ...);`
- The format string may contain both ordinary characters and **conversion specifications**, which begin with the `%` character.
  - A conversion specification is a placeholder representing a value to be filled in during printing.
  - The information that follows the `%` character *specifies* how the value is *converted* from its internal form (binary) to printed form (characters).
#### Conversion Specifications
- Can have the form `%m.pX` or `%-m.pX`, where `m` and `p` are integer constants and `X` is a letter.
- Both `m` and `p` are optional.
- If `p` is omitted the period that spearates `m` and `p` is also dropped.
- Example: In the conversion specification: `%10.2f`
  - `m` is `10`
  - `p` is `2`
  - `X` is `f`
- The **minimum field width**, `m`, specifies the minimum number of characters to print.
  - If the value to be printed requires fewer than `m` characters, the value is right-justified within the field.
- The meaning of the **precision**, `p`, depends on the choice of `X`
- The **conversion specifier**, `X`, indicates which conversion should be applied to the value before it is printed.
- The most common conversion specifers for numbers are:
  - `d`: Displays an integer in decimal (base 10) form.
    - `p` indicates the minimum number of digits to display.
    - if `p` is omitted, it is assumed to have a value of `1`.
    - i.e: `%d == %.1d`
  - `e`: Displays a floating-point number in exponential format (scientific notation).
    - `p` indicates how many digits should appear after the decimal point (the default is `6`).
    - If `p` is `0` the decimal point is not displayed.
  - `f`: Displays a floating-point number in "fixed-decimal" format, without an exponent.
    - `p` has the same meaning as for the `e` specifier.
  - `g`: Displays a floating-point number in either exponential format or fixed decimal format, depending on the numbers size.
    - `p` indicates the maximum number of significant digits (**not** digits after the decimal point) to be displayed. Will not show trailing zeros.
#### Escape Sequences
- The `\n` code often used in formated strings is called an **escape sequence**.
- A few examples:
  - Alert (bell) `\a`
  - Backspace `\b`
  - New line `\n`
  - Horizontal tab `\t`
### The `scanf` Function
#### How `scanf` Works
- `scanf` is controlled by the format string.
- Begins processing the information  in the string, starting at the left.
- For each conversion specification  in the format string, `scanf` tries to locate an item of the appropriate type in the input data, skipping blank space if necessary.
- `scanf` reads the item, stopping when it encounters a character that can't possibly belong to the item.
  - If the item was read successfully, it continues processing tthe rest of the format string.
  - If any item is not read successfully, it treturns immediately without looking at the rest of the format strring (or the remaining input data).
- As it searches for the beginning of a number, `scanf` ignores **white-space characters** (the space, horizontal and vertical tab, form-feed, and new-line characters).
#### Ordinary Characters in Format Strings
- The action that `scanf` takes when it processes an ordinary character in a format string depends on whether or not it's a white-space character:
  - **White-space characters**: When it encounters one or more consecutive white-space characters in a format string, it repeatedly reads white-space characters from the input until in reaches a non-whitespace character (which is "put back").
    - Putting a whie-space character in a format string **doesn't** force the input to contain white-space characters.
    - A white-space character in a format string matches **any** number of white-space characters in the input, including none.
  - **Other characters**: When it encounters a non-white-space character in a format string, it compares it with the next input character.
    - If the two characters match, it discards the input character and continues processing the format string.
    - If the characters don't match, it puts the offending character back into the input, then aborts without further processing the format string or reading characters from the input.
##  Expressions
### Arithmetic Operators
- **Arithmetic operators**: Operators that perform addition, subtraction, multiplication, and division.

| Unary | Binary (Additive) | Binary (Multiplicative) |
| --- | --- | --- |
| `+` unary plus | `+` addition | `*` multiplication |
| `-` unary minus | `-` subraction | `/` division |
| | | `%` remainder |

- The additive and multiplicative operators are said to be **binary** because they require *two* operands.
- The **unary** operators require *one* operand.
- When both of its operands are integers, the `/` operator "truncates" the result by dropping the fractional part.
- The `%` operator requires integer operands; if either operand is not an integer, the program won't compile.
- Using `0` as the right operand of either `/` or `%` causes undefined behavior.
### Operator Precedence and Associativity
- C uses **operator precedence** rules to determine arithmetic operators precedence:

| Precedence | Name | Symbol(s) | Associativity |
| ---        | ---  | ---       | ---           |
| 1 | increment (postfix)<br>decrement (postfix) | `++`<br>`--` | left |
| 2 | increment (prefix)<br>decrement (prefix)<br>unary plus<br>unary minus | `++`<br>`--`<br>`+`<br>`-` | right |
| 3 | multiplicative | `*` `/` `%` | left |
| 4 | additive | `+` `-` | left |
| 5 | assignment | `=` `*=` `/=` `%=` `+=` `-=` | right |
## Selection Statements
- Most of C's remaining statements fall into three categories:
  - **Selection statements:** The `if` and `switch` statements allow a program to select a particular execution path from a set of alternatives.
  - **Iteration statements:** The `while`, `do`, and `for` statements support iteration (looping).
  - **Jump statements:** The `break`, `continue`, and `goto` statements cause an unconditional jump to some ither place in the program.  (The `return` statement belongs in this category, as well).
### Logical Expressions
#### Relational Operators

| Symbol | Meaning |
| ---    | ---     |
| `<` | less than |
| `>` | greater than |
| `<=` | less than or equal to |
| `>=` | greater than or equal to |
#### Equality Operators

| Symbol | Meaning |
| --- | --- |
| `==` | equal to |
| `!=` | not equal to |
#### Logical Operators

| Symbol | Meaning |
| --- | --- |
| `!` | logical negation |
| `&&` | logical *and* |
| `\|\|` | logical *or* |
### The `if` Statement
- Example: `if ( expression ) statement`
- To test whether `0 ≤ i < n`:
  - **idiom:** `if (0 <= i && i < n) ...`
- To test the *opposite* condition:
  - **idiom:** `if (i < 0 || i >= n) ...`
#### The `else` Clause
- Example: `if ( expression ) statement else statement`
- Example:
```c
if (i > j)
  max = i;
else
  max = j;
```
#### Cascaded `if` Statements
- Example:
```c
if ( expression ) {
  statement
}
else if ( expression ) {
  statement
}
...
else if ( expression ) {
  statement
}
else {
  statement
}
```
#### Conditional Expressions
- The **conditional operator** consists of two symbols (`?` and `:`), which must be used together in the following way:
- Example: `expr1 ? expr2 : expr3`
- This expression requires *three* operands instead of one or two.  For this reason it is said to be a **ternary** operator.
- The expression should be read as:
  - "if `expr1` then `expr2` else `expr3`".
### The `switch` Statement
- Example:
```c
switch ( expression ) {    // Controlling Expression
    case constant-expression : statements
    ...
    case constant-expression : statements
    default : statements
}
```
## Loops
- A **loop** is a statement whose job is to repeatedly execute some other statement (the **loop body**).
- Every loop has a **controlling expression**.
- Each time the loop body is executed (an **iteration** of the loop), the controlling expression is evaluated; if the expression is *true* (has a value that's not zero) the loop continues to execute.
### The `while` Statement
- Example: `while ( expression ) statement`
#### Infinite Loops
- A `while` statement won't terminate if the controlling expression always has a nonzero value.
  - **idiom:** `while (1) ...`
### The `do` Statement
- Example: `do statement while ( expression ) ;`
### The `for` Statement
- Example: `for ( expr1 ; expr2; expr3 ) statement`
- **Idioms**:
  - **Counting up from 0 to n-1:**
    - `for (i = 0; i < n; i++) ...`
  - **Counting up from 1 to n:**
    - `for (i = 1; i <= n; i++) ...`
  - **Counting down from n-1 to 0:**
    - `for (i = n - 1; i >= 0; i--) ...`
  - **Counting down from n to 1:**
    - `for (i = n; i > 0; i--) ...`
#### The Comma Operator
- Example: `expr1, expr2`
### Exiting from a Loop
#### The `break` Statement
- The `break` statement transfers control out of the *innermost* enclosing `while`, `do`, `for`, or `switch` statement.
#### The `continue` Statement
- The `continue` statement transfers control to a point just *before* the end of the loop body.
#### The `goto` Statement
- The `goto` statement transfers control to *any* statement in a function, provided that the statement has a **label**.
- Example:
```c
identifier : statement
...
goto identifier ;
```
### The Null Stattement
- A statement can be **null**, devoid of symbols except for the semicolon at the end.
## Basic Types
### Integer Types
- Values of an **integer type** are whole numbers, while values of a floating ttype can have a fractional part as well.  The integer types, in t urn, are divided  into two categories: signed and unsigned.
- Signed
  - The leftmost bit of a **signed** integer (known as the **sign bit**) is `0` if the number is positive or zero, `1` if it's negative.
  - Thus, the largest 16-bit integer has the binary representation: `0111111111111111` which  has the value 32,767 (2<sup>15</sup>-1).
  - The largest 32-bit integer is: `01111111111111111111111111111111` which has the value 2,147,483,647 (2<sup>31</sup>-1).
- Unsigned
  - An integer with no sign but (the left most bit is considered part of the number's magnitude) is said to be **unsigned**.
  - The largest 16-bit unsigned integer is 65,535 (2<sup>16</sup>-1).
  - The largest 32-bit unsigned integer is 4,294,967,295 (2<sup>32</sup>-1).
- Integer Types on a 16-bit Machine

| Type | Smallest Value | Largest Value |
| ---  | ---            | ---           |
| `short int` | -32,768 | 32,767 |
| `unsigned short int` | 0 | 65,535 |
| `int` | -32,768 | 32,767 |
| `unsigned int` | 0 | 65,535 |
| `long int` | -2,147,483,648 | 2,147,483,647 |
| `unsigned long int` | 0 | 4,294,967,295 |
- Integer Types on a 32-bit Machine

| Type | Smallest Value | Largest Value |
| ---  | ---            | ---           |
| `short int` | -32,768 | 32,767 |
| `unsigned short int` | 0 | 65,535 |
| `int` | -2,147,483,648 | 2,147,483,647 |
| `unsigned int` | 0 | 4,294,967,295 |
| `long int` | -2,147,483,648 | 2,147,483,647 |
| `unsigned long int` | 0 | 4,294,967,295 |

- Integer Types on a 64-bit Machine

| Type | Smallest Value | Largest Value |
| ---  | ---            | ---           |
| `short int` | -32,768 | 32,767 |
| `unsigned short int` | 0 | 65,535 |
| `int` | -2,147,483,648 | 2,147,483,647 |
| `unsigned int` | 0 | 4,294,967,295 |
| `long int` | -9,223,372,036,854,775,808 | 9,223,372,036,854,775,807 |
| `unsigned long int` | 0 | 18,446,744,073,709,551,615 |

#### Integer Constants
- C allows integer constants to be written in decimal (base 10), ocatal (base 8), or hexadecimal (base 16).
  - **Decimal** constants contain digits between `0` and `9`, but must not begin with a zero:
    - `15  255  32767`
  - **Octal** constants contain only digits between `0` and `7`, and **must** begin with a zero:
    - `017  0377  077777`
  - **Hexadecimal** constants contain digits between `0` and `9` and letters between `a` and `f`, and **always** begin with `0x`:
    - `0xf  0xff 0x7fff`
- To force the compiler to treat a constant as a long integer, just follow it  with the letter `L` (or `l`):
  - `15L  0377L  0x7fffL`
- To indicate that the constant is unsigned, ut the letter `U` (or `u`) after it:
  - `15U  0377L  0x7fffU`
#### Integer Overflow
- If a value cannot be represented as an `int` (because it requires too many bits), we say that **overflow** has occurred.
#### Floating Types
- C provides three **floating types**, corresponding to different floating-point formats:
  - `float`: Single-precision floating-point
  - `double`: Double-precision floating-point
  - `long double`: Extended-precision floating-point
- Floating Type Characteristics (IEEE Standard)

| Type | Smallest Positive Value | Largest Value | Precision |
| ---  | ---                     | ---           | ---       |
| `float` | 1.17549×10<sup>-38</sup> | 3.40282×10<sup>38</sup> | 6 digits |
| `double` | 2.22507×10<sup>-308</sup> | 1.79769×10<sup>308</sup> | 15 digits |
### Floating Constants
- All are valid ways of writing the number 57.0:
  - `57.0  57.  57.0e0  57E0  5.7e1  5.7e+1  .57e2  570.e01`
### Character Types
#### Operations on Characters
- ```c
  char ch;
  int i;

  i = 'a';     /* i is now 97   */
  ch = 65;     /* ch is now 'A' */
  ch = ch + 1; /* ch is now 'B' */
  ch++;        /* ch is now 'C' */
  
#### Arithmetic Types
- The integer types and floating types are collectively known as **arithmetic tpes**.
- Summary of the arithmetic types in C89, divided into categories:
  - **Integral Types:**
    - `char`
    - Signed integer types (`signed char`, `short int`, `int`, `long int`).
    - Unsigned integer types (`unsigned char`, `unsigned short int`, `unsigned int`, `unsigned long int`).
    - Enumerated types.
  - **Floating Types:**
    - (`float`, `double`, `long double`)
- C99 has a more complicated hierarchy for its arithmetic types:
  - **Integer Types:**
    - `char`
    - Signed integer types, both standard (`signed char`, `short int`, `int`, `long int`, `long long int`) and extended.
    - Unsigned  integer types, both standard (`unsigned char`, `unsigned short int`, `unsigned int`, `unsigned long int`, `unsigned long long int`, `_Bool`) and extended.
    - Enumerated types.
  - **Floating Types:**
    - Real floating types (`float`, `double`, `long double`).
    - Complex types (`float _Complex`, `double _Complex`, `long double _Complex`).
#### Escape Sequences
- There are two kinds of escape sequences: **character escapes** and **numeric escapes**.

| Name | Escape Sequence |
| ---  | ---             |
| Alert (bell) | `\a` |
| Backspace | `\b` |
| Form feed | `\f` |
| New line | `\n` |
| Carriage return | `\r` |
| Horizontal tab | `\t` |
| Vertical tab | `\v` |
| Backslash | `\\` |
| Question mark | `\?` |
| Single quote | `\'` |
| Double quote | `\"` |

- An **octal escape sequence** consists of the `\` character followed by an octal number with at most three digits. i.e. `\33` is the same as `\033`.
- A **hexadecimal escape sequence** consists of `\x` followed by a hexadecimal number.
- **idiom:**
  - ```c
    while (getchar() != '\n')   /* skips rest of line */
    ;

- **idiom:**
  - ```c
    while ((ch = getchar()) == ' ')   /* skips blanks */
    ;

### Type Conversion
- For a computer to perform an arithmetic operation the operands must usually be of the same size (the same number of bits) and be stored in the same way.
- However, C allows the basic types to be mixed in expressions.
- When the complier handles these conversions automatically they're known as **implicit conversions**.
- C also allows the programmer to perform **explicity conversions**, using the cast operator.
- Implicit conversions are performed in the following situations:
  - When the operands in an arithmetic or logical expression don't have the same type (C performs what are known as the **usual arithmetic converions**).
  - When the type of the expression on the right side of an assignment doesn't match the  type of the variable on the left side.
  - When the type of an argument in a function call doesn't match the type of the corresponding parameter.
  - When the type of the expression in a `return` statement doesn't match the function's return type.
#### The Usual Arithmetic Conversions
- The usual arithmetic conversions are applied to the operands of most binary operators, including the arithmetic, relational,  and equality operators.
- The strategy behind the usual arithmetic conversions is to convert operands to the "narrowest" type that will safely accommodate both values.
- Tye types of the operands can often be made to match by converting the operand of the narrower type to the type of the other operand (this acti is known as **promotion**).
- Among the most common promotions are the **integeral promotions**, which convert a character or short interger type to `int` (or to `unsigned int` in some cases).
- We can divide the rules for performing the usual arithmetic conversions into two cases:
  - **The type of either operand is a floating type**
    ```
    long double
        ↑
      double
        ↑
      float

  - **Neither operand typoe is a floating type**
    ```
    unsigned long int
            ↑
        long int
            ↑
           int

#### Conversion During Assignment
- The usual arithmetic conversions don't apply to assignment.
- C follows the simple rule that the expression on the right side of the assignment is convert to the type of the variable on the left side.
- Example:
  - ```c
    char c;
    int i;
    float f;
    double d;

    i = c;   /* c is converted to int    */
    f = i;   /* i is converted to float  */
    d = f;   /* f is converted to double */

#### Implicit Conversions in C99
- Rules are different because C99 has additional types.
- Each integer type is given an "integer conversion rank":
1) `long long int`, `unsigned long long int`
2) `long int`, `unsigned long int`
3) `int`, `unsigned int`
4) `short int`, `unsigned short int`
5) `char`, `signed char`, `unsigned char`
6) `_Bool`
- Rules for type converion in C99:
  - **The type of either operand is a floating type**
    - As long as neighter operand has a complex type, the rules are the same as before.
  - **Neither operand type is a floating type**
    - First perform integer promotion on both operands.  If the types of the two operands are now the same, the process ends.
    - Otherwise, use the following rules, stopping at the first one that applies:
      - *If both operands have signed types or both have unsigned types*, convert the operand whose type has the lesser integer conversion rank to the type of the operand with greater rank.
      - *If the unsigned operand has rank greater or equal to the rank of the type of the signed operand*, convert the signed operand to the type of the unsigned operand.
      - *If the type of the signed operand can represent all of the values of the type of the unsigned operand*, convert the unsigned operand to the type of the signed operand.
      - *Otherwise*, convert both operands to the unsigned type corresponding to the type of the signed operand.
#### Casting
- C provides **casts**. A cast expression has the form:
  - `( type-name ) expression`
  - `type-name` specifies the type to which  the expression should be converted.
  - Example: How to use a cast expression to compute the fractional part of a `float` value:
    - ```c
      float f, frac_part;

      frac_part = f - (int) f;

- C regards `( type-name )` as a unary operator, therefore  higher precedence than binary operators.
### Type Definitions
- Previously we used the `#define` directive to creat a macro  that could be used as a Boolean type: `#define BOOL int`.
- There's a better way to setup a Boolean type, using a feature know as a **type definition**: `typedef int Bool;`
- Using `typedef` to define `Bool` causes the compiler to add `Bool` to the list of type names that it recognizes.  `Bool` can know be used in the same way as the built-in types: `Bool flag;   /* same as int flag; */`
#### Advantages of Type Definitions
- Type definitions can make a program more understandable.
#### Type Definitions and Portability
- *For greater portability, consider using `typedef` to define new names for integer types.*
### The `sizeof` Operator
- The `sizeof` operator allows a program to determine how much memory is required to store values of a particular type.
- `sizeof ( type-name )`
- The value of the expression is an unsigned integer representing the number of bytes required to store a value belonging to `type-name`.
- `sizeof(char)` is always `1`.
- Other sizes may vary, on a 32-bit machine, `sizeof(int)` is normally `4`.
- Example:
  - ```c
    #include <stdio.h>
    #include <limits.h>

    int main(void) {
        short int a;
        unsigned short int b;
        int c;
        unsigned int d;
        long int e;
        unsigned long int f;
        long long int g;
        unsigned long long int h;

        printf("╭────────────────────────┬──────┬──────────────────────┬──────────────────────╮\n");
        printf("│          Type          │ Size |         Start        │         Stop         │\n");
        printf("├────────────────────────┼──────┼──────────────────────┼──────────────────────┤\n");
        printf("│ short int              │ %ldB   │               %d │ %d                │\n", sizeof(a), SHRT_MIN, SHRT_MAX);
        printf("│ unsigned short int     │ %ldB   │                    %d │ %d                │\n", sizeof(b), 0, USHRT_MAX);
        printf("│ int                    │ %ldB   │          %d │ %d           │\n", sizeof(c), INT_MIN, INT_MAX);
        printf("│ unsigned int           │ %ldB   │                    %d │ %u           │\n", sizeof(d), 0, UINT_MAX);
        printf("│ long int               │ %ldB   │ %ld │ %ld  │\n", sizeof(e), LONG_MIN, LONG_MAX);
        printf("│ unsigned long int      │ %ldB   │                    %d │ %lu │\n", sizeof(f), 0, ULONG_MAX);
        printf( "│ long long int          │ %ldB   │ %lld │ %lld  │\n", sizeof(g), LLONG_MIN, LLONG_MAX);
        printf("│ unsigned long long int │ %ldB   │                    %d │ %llu │\n", sizeof(h), 0, ULLONG_MAX);
        printf("╰────────────────────────┴──────┴──────────────────────┴──────────────────────╯\n");

        return 0;
    }
  - Output (64-Bit Machine):
    - ```
        ╭────────────────────────┬──────┬──────────────────────┬──────────────────────╮
        │          Type          │ Size |         Start        │         Stop         │
        ├────────────────────────┼──────┼──────────────────────┼──────────────────────┤
        │ short int              │ 2B   │               -32768 │ 32767                │
        │ unsigned short int     │ 2B   │                    0 │ 65535                │
        │ int                    │ 4B   │          -2147483648 │ 2147483647           │
        │ unsigned int           │ 4B   │                    0 │ 4294967295           │
        │ long int               │ 8B   │ -9223372036854775808 │ 9223372036854775807  │
        │ unsigned long int      │ 8B   │                    0 │ 18446744073709551615 │
        │ long long int          │ 8B   │ -9223372036854775808 │ 9223372036854775807  │
        │ unsigned long long int │ 8B   │                    0 │ 18446744073709551615 │
        ╰────────────────────────┴──────┴──────────────────────┴──────────────────────╯
## Arrays
- So far we have only seen:
  - **scalar:** capable of holding a single data item.
- C also supports **aggregate** variables, which can store collections of values.
### One-Dimensional Arrays
- An **array** is a data structure containing a number of data values, all of which have the same type.
- These values, known as **elements**, can be individually selected by their position within the array.
- How to visualize a one-dimensional array named `a`:
  - ```
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
- To declare:
  - `int a[10];`
#### Array Subscripting
- To access a particular element of an array, we write the array name followed by an integer value in square brackets (this is referred to as **subscripting** or **indexing** the array).
- Here are a few examples of typical operations on an array `a` of length `N`:
  - **idiom:**
    - ```c
      for (i = 0; i < N; i++) {
        a[i] = 0;
      }
  - **idiom:**
    - ```c
      for (i = 0; i < N; i++) {
        scanf("%d", &a[i]);
      }
  - **idiom:**
    - ```c
      for (i = 0; i < N; i++) {
        sum += a[i];
      }
#### Array Initialization
- The most common form of **array initializer** is a list of constant expressions enclosed in braces and separated by commas: `int a[10]  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};`.
- If the initializer is *shorter* than the array, the remaining elements are given the value `0`:
  - ```c
    int a[10] = {1, 2, 3, 4, 5, 6};
      /* inital value of a is  {1, 2, 3, 4, 5, 6, 0, 0, 0, 0} */
- Using this feature, we can easily initialize an array to all zeros:
  - ```c
    int a[10] = {0};
      /* initial value of a is {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; */
- It's illegal f or an initializer to be completely empty.
- It's illegal for an initializer to be *longer* than the array it initializes.
#### Designated Initializers
- Consider the following: `int a[15] = {0, 0, 29, 0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 48};`.
- C99 has **designated initializers** that can be used.
- Example: `int a[15] = {[14] = 48, [9] = 7, [2] = 29};`.
#### Using the `sizeof` Operator with Arrays
- The `sizeof` operator can determine the size of an array (in bytes).
- If `a` is an array  of 10 integers, then `sizeof(a)` is typically `40` (assuming that each integer requires four bytes).
- We can also use `sizeof` to measure the size of an array element, such as `a[0]`.  Dividing the array size by the element size gives the length of the array: `sizeof(a) / sizeof(a[0])`
- Example:
  - ```c
    for (i = 0; i < sizeof(a) / sizeof(a[10]); i++) {
        a[i] = 0;
    }
###  Multidimensional Arrays
- The following declaration creates a two-dimensional array: `int m[5][9];`.
- The array `m` has `5` rows and `9` columns.
  - ```
        0   1   2   3   4   5   6   7   8
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───╮
    0 │   │   │   │   │   │   │   │   │   │
      ├───┼───┼───┼───┼───┼───┼───┼───┼───┤
    1 │   │   │   │   │   │   │   │   │   │
      ├───┼───┼───┼───┼───┼───┼───┼───┼───┤
    2 │   │   │   │   │   │   │   │   │   │
      ├───┼───┼───┼───┼───┼───┼───┼───┼───┤
    3 │   │   │   │   │   │   │   │   │   │
      ├───┼───┼───┼───┼───┼───┼───┼───┼───┤
    4 │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───╯
- To access teh element of `m` in row `i`, column `j`, we must write `m[i][j]`.
- The expression `m[i]` designates row `i` of `m`.
- The expression `m[i][j]` selects element `j` in row `i`.
- Although we visualize two-dimensional arrays as tables, that's not the way they're actually stored in computer memory.  C stores arrays in **row-major order**, with row `0` first, then row `1`, and so forth.
- Example:
  - ```
               row 0                row 1                       row 4
       ╭──────────────────╮ ╭──────────────────╮        ╭──────────────────╮
      ╭──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────╮
      │m[0,0]│ .... │m[0,8]│m[1,0]│ .... │m[1,8]│ .... │m[4,0]│ .... │m[4,8]│
      ╰──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────╯
#### Initializing a Multidimensional Array
- Creating an initializer for a two-dimensional array can be done by nesting one-dimensional initializers:
  - ```c
    int m[5][9] = {{1, 1, 1, 1, 1, 0, 1, 1, 1},
                   {0, 1, 0, 1, 0, 1, 0, 1, 0},
                   {0, 1, 0, 1, 1, 0, 0, 1, 0},
                   {1, 1, 0, 1, 0, 0, 0, 1, 0},
                   {1, 1, 0, 1, 0, 0, 1, 1, 1}};
- Ways to abbreviate intializers for multidimensional arrays:
  - If an initializer isn't large enough to fill a multidimensional array, the remaining elements are given the value `0`.
  - If an inner list isn't long enough to fill a row, the remaining elements in the row are initialized to `0`
  - You can also omit inner braces:
    - ```c
          int m[5][9] = {1, 1, 1, 1, 1, 0, 1, 1, 1,
                         0, 1, 0, 1, 0, 1, 0, 1, 0,
                         0, 1, 0, 1, 1, 0, 0, 1, 0,
                         1, 1, 0, 1, 0, 0, 0, 1, 0,
                         1, 1, 0, 1, 0, 0, 1, 1, 1};
### Variable-Length Arrays (C99)
- In C99 it's sometimes possible to use an expression that's **not** constant to specify the length of an array variable:
  - ```c
    ...
    int n;

    scanf("%d", &n);

    int a[n];
    ...
## Functions
### Defining and Calling Functions
- Example:
  - ```c
    double average(double a, double b) {
        return (a + b) / 2;
    }
- The word `double` at the beginning is `average`'s **return type**: the type of data that the function returns each  time it's called.
- Every function has an executable part, called the **body**, which is enclosed in braces.
- To call a function, we write the function name, followed by a list of **arguments**.
#### Function Definitions
- The general form of a **function definition**:
  - ```c
    return-type function-name ( parameters ) {
        declarations
        statements
    }
- The return type of a function is the type of value that the function returns.  The following rules govern the return type:
  - Functions may not return array, but there are no other restrictions on the return type.
  - Specifying that the return type is `void` indicates that the function doesn't return a value.
  - If the return type is omitted in C89, the function is presumed to return a value of type `int`.  In C99, it's illegal to omit the return type of a function.
- In C89, variable declarations must come first, before all statements in the body of the function.
- In C99, variable declarations and statements can be mixed, as long as each variable is declared prior to the first statement that uses the variable.
#### Function Calls
- A function call consists of a function name followed  by a list of arguments, enclosed in parentheses.
### Function Declarations
- C **doesn't** require that the definition of a function precede its calls.
- Function declarations of the kind we've been discussing are known as **function prototypes** to distinguish them from an older style of function declaration in which the parentheses are left empty.
### Arguments
- In C, arguments are  **passed  by value**: when a function is called, each argument is evaluated and its value assigned to the corresponding parameter.
- Since the parameter contains a copy of the argument's value, any changes made to the parameter during the execution of the function don't affect the argument.
#### Argument Conversions
- The rules governing how the arguments are converted depend on whether or not the compiler has seen a parototype  for the function (or the function's full definition) prior to the call:
  - **The compiler has encountered a prototype prior to the call**
    - The value of each argument is implicitly convert to t he type of the corresponding parameter as if by assignment.
  - **The compiler has not encountered a  prototype prior to the call**
    - The compiler performs  the **default argument promotions:** (1) `float` arguments are converted to `double`. (2) The integral promotions are performed, causing `char` and `short` arguments to be converted to `int`.
#### Array Arguments
- The argument can be any one-dimensional array whose elements are  of the proper type.
- C doesn't provide any easy  way for a function to determine the length of an array passed to it.  Instead, we have to supply the length as an additional argument.
#### Variable-Length Array Parameters
- C99 allows the length of an array to be specified using a non-constant expression.  Variable-length arrays can also be parameters.
- **Legal:**
  - ```c
    int sum_array(int n, int a[n]) {
        ..
    }
- **Illegal:**
  - ```c
    int sum_array(int a[n], int n) {   /*** WRONG ***/
        ...
    }
- Order mattters.  The compiler must see `int n` being passed to the function as a parameter before the function can accept `int a[n]` as a parameter.
- There are several ways to write the prototype for our new version of `sum_array`:
  - `int sum_array(int n, int a[n]);   /* Version 1 */
- Another possibility is to replace the array length by an asterisk (`*`):
  - `int sum_array(int n, int a[*]);   /* Version 2 */
- The reason for using the `*` notation is that parameter  names are optional in function declarations.
- If the name of the first parameter is omitted, it wouldn't be possible to specify that the length of the array is `n`, but the `*` provides a clue that the length of  the array is related to parameters that come earlier in the list:
  - `int sum_array(int, int [*]);     /* Version 2b */`
- It is also legal to leave the brackets empty, as we normally do when declaring an array parameter:
  - `int sum_array(int n, int a[]);   /* Version 3a */`
  - `int sum_array(int, int []);      /* Version 3b */`
#### Using `static` in Array Parameter Declarations
- C99 allows the use of the keyword `static` in the declaration of array parameters.
- In the following example, putting `static` in front of the number `3` indicates that the length of `a` is guaranteed to be at least `3`:
  - ```c
    int sum_array(int a[static 3], int n) {
        ...
    }
- Using `static` in this way has no effect on the behavior of the program.  Thre presence of `static` is merely a "hint" that may allow a C compiler to generate faster instructions for accessing the array.
- If an array parameter has more than one dimension, `static` can be used only in the first dimension.
#### Compound Literals
- An unnamed array that's created "on the fly" by simply specifying which elements it contains:
  - `total = sum_array(`**`(int []){3,0,3,4,1}`**`, 5);`
### The `return` Statement
- A non-`void` function must use the `return` statement to specify what value it will return.  The `return` statement has the form:
  - `return experssion ;`
### Program Termination
- Omitting the return type of a function isn't legal in C99.
#### The `exit` Function
- Executing a `return` statement in `main` is one way to terminate a program.
- Another is calling the `exit` function, which belongs to `<stdlib.h>`.
- The argument passed to `exit` has the same meaning as `main`'s return value: both indicate the program's status as termination.
- To indicate normal termination, we'd pass `0`:
  - `exit(0);             /* normal termination */`
- C allows us to pass `EXIT_SUCCESS` instead (the effect is the same):
  - `exit(EXIT_SUCCESS);  /* normal termination */`
- Passing `EXIT__FAILURE` indicates abnormal termination:
  - `exit(EXIT_FAILURE);  /* abnormal termination */`
- `EXIT_SUCCESS` and `EXIT_FAILURE` are macros defined in `<stdlib.h>`.
### Recursion
- A function is **recursive** if it calls itself.
- Example: The following function computes `n!` recursively, using the formula `n! = n ⨯ (n - 1)!`:
  - ```c
    int fact(int n) {
      if (n <= 1) {
        return 1;
      } else {
        return n * fact(n - 1);
      }
    }
#### The Quicksort Algorithm
- Reursion often arises naturally as a result of an algorithm design technique known as **divide-and-conquer**, in which a large problem is divided into smaller pieces that are then tackled by the same algorithm.
- An example of this is the sorting algorithm known as **Quicksort**.
- The Quicksort algorithm goes as follows (for simplicity the array is assumed to be indexed from 1 to n):
  1) Choose any array element `e` (the "partitioning element"), then rearrange the array so that elements `1`, `...`, `i - 1` are less than or equal to `e`, element `i` conatins `e`, and elements `i + 1`, `...`, `n` are greater than or equal to `e`.
  2) Sort elements `1`, `...`, `i - 1` by using Quicksort recursively.
  3) Sort elements `i + 1`, `...`, `n` by using Quicksort recursively.
## Program Organization
### Local Variables
- A variable declared in the body of a function is said to be **local** to the function.
- In the following functionn, `sum` is a local variable:
  - ```c
    int sum_digits(int n) {
      int sum = 0;    /* local variable */

      while (n > 0) {
        sum += n % 10;
        n /= 10;
      }

      return sum;
    }
- By default, local variables have the following properties:
  - **Automatic storage duration**
    - The **storage duration** (or **extent**) of a variable is the portion of program execution during which storage for the variable exists.
    - Storage for a local variable is "automatically" allocated when the enclosing function is called and deallocated when the function returns, so the variable is said to have **automatic storage duration**.
  - **Block scope**
    - The **scope** of a variable is the portion of the program text in which the variable can be referenced.
    - A local variable has **block scope:** it is visible from its point of declaration to the end of the enclosing function body.
    - ```
      void f(void) {
        ...
        int i;   ──╮
        ...        ├─── scope of i
                 ──╯
      }
#### Static Local Variables
- Putting the word `static` in the declaration of a local variable causes it to have **static storage duration** instead of automatic storage duration.
- A variable with static storage duration has permanent storage location, so it retains its value throughout the execution of the program.
#### Parameters
- Parameters have the same properties (automatic storage duration and block scope) as local variables.
### External Variables
- Functions can also communicate through **external variables**--variables that are declared outside the body of any function.
- The properties of external variables (known as **global variables**) are different from those of local variables:
  - **Static storage duration**
    - External variables have statuc storage duration, just like local variables that have been delared `static`.
    - A value stored in an external variable will stay there idenfinitely.
  - **File scope**
    - An external variable has **file scope:** it is visible from its point of declaration to the end of the enclosing file.
    - As a result, an external variable can be accessed (and potentially modified) by all functions that follow its declaration.
#### Example: Using External Variables to Implement a Stack
- Looking at a data structure know as a **stack**:
  - A stack, like an array, can store multiple data items of the same type.
  - However, the operations on a stack are limited:
    - We can either **push** an item onto the stack (add it to the end).
    - Or **pop** it from the stack (remove it from the same end).
- Example:
  - ```c
    #include <stdbool.h>   /* C99 only */

    #define STACK_SIZE 100

    /* external variables */
    int contents[STACK_SIZE];
    int top = 0;

    void make_empty(void) {
      top = 0;
    }

    bool is_empty(void) {
      return top == 0;
    }

    bool is_full(void) {
      return top == STACK_SIZE;
    }

    void push(int i) {
      if (is_full()) {
        stack_overflow();
      } else {
        contents[top++] = i;
      }
    }

    int pop(void) {
      if (is_empty()) {
        stack_underflow();
      } else {
        return contents[--top];
      }
    }
#### Pros and Cons of External Variables
- If we change an external variable during maintenance we'll need to check every function in the same file to see how the change affects it.
- If an external variable is assigned an incorrect value, it may be difficult to identify the guilt function.
- Functions that rely on external variables are hard to reuse in other programs.
## Pointers
### Pointer Variables
- Memory is divided into **bytes**, with each byte capable of storing eight bits of information:
  - ```
      ╭───┬───┬───┬───┬───┬───┬───┬───╮
      │ 0 │ 1 │ 0 │ 1 │ 0 │ 0 │ 1 │ 1 │
      ╰───┴───┴───┴───┴───┴───┴───┴───╯
- Each byte has a unique **address** to distinguish it from the other bytes in memory.
- If there are `n` bytes in memory, we can think of addresses as numbers that range from `0` to `n-1`.
  - ``` Address   Contents
                ╭──────────╮
           0    │ 01010011 │
                ├──────────┤
           1    │ 01110101 │
                ├──────────┤
           2    │ 01110011 │
                ├──────────┤
           3    │ 01100001 │
                ├──────────┤
           4    │ 01101110 │
                ├──────────┤
          ...   │   ....   │
                ├──────────┤
          n-1   │ 01000011 │
                ╰──────────╯
- The address of the first byte is said the be the address of the variable.
- If `i` occupies the bytes at addresses `2000` and `2001`, so `i`'s address is `2000`:
  - ```
           ╭──────────╮
           │   ....   │
           ├──────────┤ ──╮
     2000  │          │   │ 
           ├──────────┤   ├── i
     2001  │          │   │
           ├──────────┤ ──╯ 
           │   ....   │ 
           ╰──────────╯
- Although addresses are represented by numbers, their range of values may differ from theat of integers, se we cannot necessarily store them in ordinary integer variables.
- However, they can be stored in special **pointer variables**.
- When the address of variable `i` is stored in the pointer variable `p`, we say that `p` "points to" `i`.
- In other words, a **pointer is nothing more than an address**, and a **pointer variable is just a variable that can store an address**.
  - ```
      ╭───╮  ╭────────╮
    p │  ─┼─→│        │ i
      ╰───╯  ╰────────╯
#### Declaring Pointer Variables
- A pointer variable is declared in a similar way as an ordinary variable, but the name of the pointer variable must be preceded by an asterisk:
  - `int *p;`
- This declaration states that `p` is a pointer variable capable of pointing to **objects** of type `int`.
- Pointer variables can appear in declarations along with other variables:
  - `int i, j, a[10], b[20], *p, *q;`
- C requires that every pointer variable point only to objects of a particular type (the **referenced type**):
  - ```c
    int *p;     /* points only to integers   */
    double *q;  /* points only to doubles    */
    char *r;    /* points only to characters */
### The Address and Indirection Operators
- C provides a pair of operators designed specifically for use with pointers.
- To find the address of a variable, we use the `&` (address) operator.
  - If `x` is a variable, then `&x` is the address of `x` in memory.
- To gain access to the object that a pointer points to, we use the `*` (**indirection**) operator.
  - if `p` is a pointer, then `*p` represents the object to which `p` currently points.
#### The Address Operator
- To intialize a pointer:
  - ```c
    int i, *p;
    ...
    p = &i;
- Or:
  - ```c
    int i;
    int *p = &i;
- Or even:
  - `int i, *p = &i;`
#### The Indirection Operator
- Once a pointer variable points to an object, we can use the `*` (indirection) operator to access what's stored in the object.
  - `printf("%d\n", *p);`
  - `printf` will display the *value* of `i`, not the *address* of `i`.
- As long as `p` points to `i`, `*p` is an **alias** for `i`.
- Not only does `*p` have the same value as `i`, but changing the value of `*p` also changes the value of `i`.
- ```
  p = &i;
      ╭───╮  ╭────────╮
    p │  ─┼─→│   ?    │ i
      ╰───╯  ╰────────╯
  i = 1;
      ╭───╮  ╭────────╮
    p │  ─┼─→│   1    │ i
      ╰───╯  ╰────────╯
  
  printf("%d\n", i);     /* prints 1 */
  printf("%d\n", *p);    /* prints 1 */
  
  *p = 2;
      ╭───╮  ╭────────╮
    p │  ─┼─→│   2    │ i
      ╰───╯  ╰────────╯

  printf("%d\n", i);    /* prints 2 */
  printf("%d\n", *p);   /* prints 2 */
#### Pointer Assignment
- C allows the use of the assignment operator to copy pointers, provided that they have the same type.
- Suppose that `i`, `j`, `p`, and `q` have been declared as follows:
  - `int i, j, *p, *q;`
  - The statement: `p = &i;` is an example of pointer assignment: the address of `i` is copied into `p`.
  - Here is another example of pointer assignment: `q = p;`
    - This statement copies the contents of `p` (the address of `i`) into `q`, in effect making `q` point to the same place as `p`:
      - ```
          ╭───╮  
        p │  ─┼─╮       
          ╰───╯ │    ╭────────╮
                ├───→│    ?   │ i
          ╭───╮ │    ╰────────╯
        q │  ─┼─╯
          ╰───╯
    - Both `p` and `q` now point to `i`, so we can change `i` by assigning a new value to either `*p` or `*q`:
      - ```
        *p = 1;
          ╭───╮  
        p │  ─┼─╮       
          ╰───╯ │    ╭────────╮
                ├───→│    1   │ i
          ╭───╮ │    ╰────────╯
        q │  ─┼─╯
          ╰───╯

        *q = 2;
          ╭───╮  
        p │  ─┼─╮       
          ╰───╯ │    ╭────────╮
                ├───→│    2   │ i
          ╭───╮ │    ╰────────╯
        q │  ─┼─╯
          ╰───╯
      - Any number of pointer variables may point to the same object.
      - Do not confuse:
        - `q = p;` with `*q = *p;`
        - The first statement is a pointer assignment.
        - The second isn't, as the following example shows:
          - ```
            p = &i;
            q = &j;
            i = 1;

               ╭───╮  ╭────────╮
             p │  ─┼─→│   1    │ i
               ╰───╯  ╰────────╯
               ╭───╮  ╭────────╮
             q │  ─┼─→│   ?    │ j
               ╰───╯  ╰────────╯

            *q = *p;
               ╭───╮  ╭────────╮
             p │  ─┼─→│   1    │ i
               ╰───╯  ╰────────╯
               ╭───╮  ╭────────╮
             q │  ─┼─→│   1    │ j
               ╰───╯  ╰────────╯
        - The assignment `*q = *p` copies the value that `p` points to (the value of `i`) into the object that `q` points to (the variable `j`).
### Pointers as Arguments
- A variable supplied as an argument in a function call is protected against change, because C passes arguments by value.
- Pointers offer a solution to this problem: instead of passing a variable `x` as the argument to a function, we'll supply `&x`, a pointer to `x`.
- When the function is called, `p` will have the value `&x`, hence `*p` (the object `p` points to) will be an alias for `x`.
- Each appearance of `*p` in the body of the function will be an indirect reference to `x`, allowing the function both to read `x` and to modify it.
- Example:
  - ```c
    void decompose(double x, long *int_part, double *frac_part) {
      *int_part  = (long) x;
      *frac_part = x - *int_part;
    }
- We call `decompose` in the following way: `decompose(3.14159, &i, &d);`
### Pointers as Return Values
- We can not only pass pointers to functions but also write function that *return* pointers.
- Example:
  - ```c
    int *max(int *a, int *b) {
      if (*a > *b) {
        return a;
      } else {
        return b;
      }
    }
  - Calling the function:
  - ```c
    int *p, i, j;
    ...
    p = max(&i, &j);
## Pointers and Arrays
### Pointer Arithmetic
- Suppose that `a` and `p` have been declare as follows: `int a[10], *p;`
- We can make `p` point to `a[0]` by writing: `p = &a[0];`
  - ```
      ╭───╮
    p │   │
      ╰─┼─╯
        ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
- We can now access `a[0]` through `p`.
- Example:
  - ```
    *p = 5;
      ╭───╮
    p │   │
      ╰─┼─╯
        ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │ 5 │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
- By performing **pointer arithmetic** (or **address arithmetic**) on `p`, we can access the other elements of `a`.
- C supports three (and only three) forms of pointer arithmetic:
  - Adding an integer to a pointer
  - Subracting an integer from a pointer
  - Subracting one pointer from another
- Looking at each of these operations, the following examples assume this declaration:
  - `int a[10], *p, *q, i;`
#### Adding an Integer to a Pointer
- if `p` points to the array element `a[i]`, then `p + j` points to `a[i+j]`.
  - ```
    p = &a[2];
              ╭───╮
            p │   │
              ╰─┼─╯
                ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
        
    q = p + 3;
              ╭───╮       ╭───╮
            p │   │     q │   │
              ╰─┼─╯       ╰─┼─╯
                ↓           ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
        
    p += 6;
                          ╭───╮       ╭───╮
                        q │   │     p │   │
                          ╰─┼─╯       ╰─┼─╯
                            ↓           ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
#### Subracting an Integer from a Pointer
- If `p` points to t he array element `a[i]`, then `p - j` points to `a[i-j]`.  
  - ```
    p = &a[8];
                                      ╭───╮
                                    p │   │
                                      ╰─┼─╯
                                        ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
        
    q = p - 3;
                          ╭───╮       ╭───╮
                        q │   │     p │   │
                          ╰─┼─╯       ╰─┼─╯
                            ↓           ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
        
    p -= 6;
              ╭───╮       ╭───╮
            p │   │     q │   │
              ╰─┼─╯       ╰─┼─╯
                ↓           ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
#### Subtracting One Pointer from Another
- When one pointer is subtracted from another, the result is the distance (measured in array elements) between the pointers.
- If `p` points to `a[i]` and `q` points to `a[j]`, then `p - q` is equal to `i - j`.
  - ```
    p = &a[5];
    q = &a[1];
          ╭───╮           ╭───╮
        q │   │         p │   │
          ╰─┼─╯           ╰─┼─╯
            ↓               ↓
      ╭───┬───┬───┬───┬───┬───┬───┬───┬───┬───╮
    a │   │   │   │   │   │   │   │   │   │   │
      ╰───┴───┴───┴───┴───┴───┴───┴───┴───┴───╯
        0   1   2   3   4   5   6   7   8   9
    i = p - q;   /* i is 4  */
    i = q - p;   /* i is -4 */
#### Comparing Pointers
- It is legal for pointer to point to an element within an array created by a compound literal:
  - `int *p = (int []){3, 0, 3, 4, 1};
### Using Pointers for Array Processing
- ```c
  #define N 10
  ...
  int a[N], sum, *p;
  ...
  sum = 0;
  for (p = &a[0]; p < &a[N]; p++) {
    sum += *p;
  }
- First iteration:
  - ```
      ╭───╮        
    p │   │        
      ╰─┼─╯        
        ↓               
      ╭────┬────┬────┬───┬────┬────┬────┬────┬────┬────╮
    a │ 11 │ 34 │ 82 │ 7 │ 64 │ 98 │ 47 │ 18 │ 79 │ 20 │
      ╰────┴────┴────┴───┴────┴────┴────┴────┴────┴────╯
         0    1    2   3    4    5    6    7    8    9
        ╭────╮        
    sum │ 11 │        
        ╰────╯
- Second iteration:
  - ```
           ╭───╮        
         p │   │        
           ╰─┼─╯        
             ↓               
      ╭────┬────┬────┬───┬────┬────┬────┬────┬────┬────╮
    a │ 11 │ 34 │ 82 │ 7 │ 64 │ 98 │ 47 │ 18 │ 79 │ 20 │
      ╰────┴────┴────┴───┴────┴────┴────┴────┴────┴────╯
         0    1    2   3    4    5    6    7    8    9
        ╭────╮        
    sum │ 45 │        
        ╰────╯
- Third iteration:
  - ```
                ╭───╮        
              p │   │        
                ╰─┼─╯        
                  ↓               
      ╭────┬────┬────┬───┬────┬────┬────┬────┬────┬────╮
    a │ 11 │ 34 │ 82 │ 7 │ 64 │ 98 │ 47 │ 18 │ 79 │ 20 │
      ╰────┴────┴────┴───┴────┴────┴────┴────┴────┴────╯
         0    1    2   3    4    5    6    7    8    9
        ╭─────╮        
    sum │ 127 │        
        ╰─────╯
#### Combining the `*` and `++` Operators
- Consider: `a[i++] = j;`
- If `p` is pointing to an array element, the corresponding statement would be:
  - `*p++ = j;`
- Because the postfix version of `++` takes precedence over `*`, the compiler sees this as:
  - `*(p++) = j;`

- | Expression         | Meaning |
  | ---                | ---     |
  | `*p++` or `*(p++)` | Value of expression is `*p` before increment; increment `p` later |
  | `(*p)++`           | Value of expression is `*p` before increment; increment `*p` later |
  | `*++p` or `*(++p)` | Increment `p` first; value of expression is `*p` after increment |
  | `++*p` or `++(*p)` | Increment `*p` first; value of expression is `*p` after increment |
#### Using an Array Name as a Pointer
- *The name of an array can be used as a pointer to the first element in the array.*
  - `int a[10];`
- Using `a` as a pointer to the first element in the array, we can modify `a[0]`:
  - `*a = 7;  /* stores 7 in a[0] */`
- We can modify `a[1]` through the pointer `a + 1`:
  - `*(a+1) = 12;  /* stores 12 in a[1] */` 
#### Array Arguments (Revisted)
- When passed to a function, an array name is always treated as a pointer.
  - ```c
    int find_largest(int a[], int n) {
      int i, max;

      max = a[0];
      for (i = 1; i < n; i++) {
        if (a[i] > max) {
          max = a[i];
        }
      }
    }
- When an oardinary variable is passed to a function, its value is copied; any changes to the corresponding parameter don't affect the variable.
- In contranst, an array used as an argument isn't protected against change, since no copy is mad of the array itself.
#### Using a Pointer as an Array Name
- ```c
  #define N 10
  ...
  int a[N], i, sum = 0, *p = a;
  ...
  for (i = 0; i < N; i++) {
    sum += p[i];
  }
- The compiler treats `p[i]` as `*(p+i)`, which is a perfectly legal us of pointer arithmetic.
### Pointers and Multidimensional Arrays
- Just as pointers can point to elements of one-dimensional arrays, they can also point to elements of multidimensional arrays.
#### Processing the Elements of a Multidimensional Array
- ```
               row 0                row 1                     row r - 1
       ╭──────────────────╮ ╭──────────────────╮        ╭──────────────────╮
      ╭──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────╮
      │      │ .... │      │      │ .... │      │ .... │      │ .... │      │
      ╰──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────╯
- Suppose that the array has been declared as follows:
  - `int a [NUM_ROWS][NUM_COLS];`
- The obvious technique would be to use nested `for` loops:
- ```c
    int row, col;
    ...
    for (row = 0; row < NUM_ROWS; row++) {
      for (col = 0; col < NUM_COLS; col++) {
        a[row][col] = 0;
      }
    }
- But if we view `a` as a one-dimensional array of integers (which is how it's stored), we can replace the pair of loops by a single loop:
  - ```c
    int *p;
    ...
    for (p = &a[0][0]; p <= &a[NUM_ROWS-1][NUM_COLS-1]; p++) {
      *p = 0;
    }
#### Processing the Rows of a Multidimensional Array
- To visit the elements of row `i`, we'd initialize `p` to point to element `0` in row `i` in the array `a`:
  - `p = &a[i][0];`
- Or we could simply write:
  - `p = a[i];`
- Since, for any two-dimensional array `a`, the expression `a[i]` is a pointer to the first element in row `i`.
  - ```c
    int a[NUM_ROWS][NUM_COLS], *p, i;
    ...
    for (p = a[i]; p < a[i] + NUM_COLS; p++) {
      *p = 0;
    }
#### Processing the Columns of a Multidimensional Array
- ```c
  int a[NUM_ROWS][NUM_COLS], (*p)[NUM_COLS], i;
  ...
  for (p = &a[0]; p < &a[NUM_ROWS]; p++) {
    (*p)[i] = 0;
  }
#### Using the Name of a Multidimensional Array as a Pointer
- Consider: `int a[NUM_ROWS][NUM_COLS];`
- `a` is **not** a pointer to `a[0][0]`; instead it's a pointer to `a[0]`.
- C regards `a` not as a two-dimensional array but as a one-dimensional array.
### Pointers and Variable-Length Arrays
- ```c
  void f(int n) {
    int a[n], *p;
    p = a;
    ...
  }
- ```c
  void f(int m, int n) {
    int a[m][n], (*p)[n];
    p = a;
  }
- Since the type of `p` depends on `n`, which isn't constant, `p` is said to have a **variably modified type**.
## Strings
### String Literals
- A **string literal** is a sequence of characters enclosed within double quotes:
  - `"When you come to a fork in the road, take it."`
#### Continuing a String Literal
- ```c
  printf("When you come to a fork in the road, take it.  \
  --Yogi Berra");
#### How String Literals Are Stored
- C treats string literals as character arrays.
- This area of memory will contain the characters in the string, plus one extra character--the **null character**--to mark the end of the string.
  - `null character == \0`
- The string literal `"abc"` is stored as an array of four characters:
  - ```
      ╭───┬───┬───┬────╮
      │ a │ b │ c │ \0 │
      ╰───┴───┴───┴────╯
### Reading and Writing Strings
#### Writing Strings Using `printf` and `puts`
- The `%s` conversion specification allows `printf` to write a string.
  - ```c
    char str[] = "Are we having fun yet?";
    printf("%s\n", str);
- Or:
  - `puts(str);`
#### Reading String Using `scanf` and `gets`
- `scanf("%s", str);
  - There is no need to put the `&` operator in front of `str` in the call of `scanf`; like any array name, `str` is treated as a pointer when passed to a function.
#### Reading String Character by Character
- ```c
  int read_line(char str[], int n) {
    int ch, i = 0;

    while ((ch = getchar()) != '\n') {
      if (i < n) {
        str[i++] = ch;
      }
    }
    str[i] = '\0';      /* terminantes string */
    return i;           /* number of characters stored */
  }
### Accessing the Characters in a String
- ```c
  int count_spaces(const char s[]) {
    int count = 0, i;

    for (i = 0; s[i] != '\0'; i++) {
        if (s[i] == ' ') {
            count++;
        }
    }

    return count;
  }
### Using the C String Library
- Direct attempts to copy or compare strings will fail.
- Assume `str1` and `str2` have been declared as follows:
  - `char str1[10], str2[10];`
- Copying a string into a character array using the `=` operator is not possible:
  - ```c
    str1 = "abc";          /*** WRONG ***/
    str2 = str1;           /*** WRONG ***/
    if (str1 == str2) ...  /*** WRONG ***/
- The above compares `str1 ` and `str2` as *pointers*; it dosen't compare the contents of the two arrays.
- The C library provides a rich set of functions for performing operations on strings in the `<string.h>` header.
#### The `strcpy` (String Copy) Function
- The `strcpy` function has the following prototype in `<string.h>`: `char *strcpy(char *s1, const char *s2);`
- `strcpy` copies the string `s2` into the string `s1`.
- Example: `strcpy(str2, "abcd");   /* str2 now cont ains "abcd" */`
#### The `strlen` (String Length) Function
- The `strlen` function has the following prototype: `size_t strlen(const char *s);`
-  Example:
  - ```c
    int len;

    len = strlen("abc");    /* len is now 3 */
    len = strlen("");       /* len is now 0 */
    strcpy(str1, "abc");
    len = strlen(str1);     /* len is now 3 */
#### The `strcat` (String Concatenation) Function
- The `strcat` function has the following prototype: `char *strcat(char *s1, const char *s2);`
- `strcat` appends the contents of the string `s2` to t he end of the string `s1`; it returns `s1` (a pointer to the resulting string).
- Example:
  - ```c
    strcpy(str1, "abc");
    strcat(str1, "def");    /* str1 now contains "abcdef" */
    strcpy(str1, "abc");
    strcpy(str2, "def");
    strcat(str1, str2);     /* str1 now contains "abcdef" */
- The `strncat` function is a safer but slower version of `strcat`.  It has a thrid argument that limits the number of characters it will copy.
  - `strncat(str1, str2, sizeof(str1) - strlen(str1) - 1);`
#### The `strcmp` (String Comparison) Function
- The `strcmp` function has the following prototype: `int strcmp(const char *s1, const char *s2);`
- `strcmp` compares the strings `s1` and `s2`, returning a value less than, equal to, or greater than 0, depending on whether `s1` is less than, equal to, or greater than `s2`.
### String Idioms
#### Searching for the End of a String
- ```c
  size_t strlen(const char *s) {
    size_t n;

    for (n = 0; *s != '\0'; s++) {
      n++;
    }

    return n;
  }
#### Copying a String
- ```c
  char *strcat(char *s1, const char *s2) {
    char *p = s1;

    while (*p != '\0') {
      p++;
    }

    while (*s2 != '\0') {
      *p = *s2;
      p++;
      s2++;
    }

    *p = '\0';
    return s1;
  }
### Arrays of Strings
- Consider the example:
  - ```c
    char planets[][8] = {"Mercury", "Venus", "Earth",
                         "Mars", "Jupiter", "Saturn",
                         "Uranus", "Neptune", "Pluto"};
- ```
        0   1   2   3   4   5   6   7
      ╭───┬───┬───┬───┬───┬───┬───┬───╮
    0 │ M │ e │ r │ c │ u │ r │ y │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    1 │ V │ e │ n │ u │ s │\0 │\0 │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    2 │ E │ a │ r │ t │ h │\0 │\0 │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    3 │ M │ a │ r │ s │\0 │\0 │\0 │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    4 │ J │ u │ p │ i │ t │ e │ r │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    5 │ S │ a │ t │ u │ r │ n │\0 │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    6 │ U │ r │ a │ n │ u │ s │\0 │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    7 │ N │ e │ p │ t │ u │ n │ e │\0 │
      ├───┼───┼───┼───┼───┼───┼───┼───┤
    8 │ P │ l │ u │ t │ o │\0 │\0 │\0 │
      ╰───┴───┴───┴───┴───┴───┴───┴───╯
- What is needed is a **ragged array**: a two-dimensional array whose rows can have different lengths.
- C doesn't proved a "ragged array type", but it does give us the tools to simulate one.
- By creating an array whose elements are *pointers* to strings.
- ```c
    char *planets[] = {"Mercury", "Venus", "Earth",
                       "Mars", "Jupiter", "Saturn",
                       "Uranus", "Neptune", "Pluto"};
- ```
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    0 │  ─┼─→│ M │ e │ r │ c │ u │ r │ y │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    1 │  ─┼─→│ V │ e │ n │ u │ s │\0 │\0 │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    2 │  ─┼─→│ E │ a │ r │ t │ h │\0 │\0 │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    3 │  ─┼─→│ M │ a │ r │ s │\0 │\0 │\0 │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    4 │  ─┼─→│ J │ u │ p │ i │ t │ e │ r │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    5 │  ─┼─→│ S │ a │ t │ u │ r │ n │\0 │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    6 │  ─┼─→│ U │ r │ a │ n │ u │ s │\0 │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    7 │  ─┼─→│ N │ e │ p │ t │ u │ n │ e │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
      ╭───╮  ╭───┬───┬───┬───┬───┬───┬───┬───╮
    8 │  ─┼─→│ P │ l │ u │ t │ o │\0 │\0 │\0 │
      ╰───╯  ╰───┴───┴───┴───┴───┴───┴───┴───╯
#### Command-Line Arguments
- To obtian access to **command-line arguments** (called **program parameters** in the C standard), we must define `main` as a function with two parameters, which are customarily named `argc` and `argv`:
  - ```c
    int main(int argc, char *argv[]) {
      ...
    }
- `argc` ("argument count") is the number of command-line arguments (including the name of the program itself).
- `argv` ("argument vector") is an array of pointers to the command-line arguments, which are stored in string form.
  - `argv[0]` points to the name of the program.
  - `argv[argc-1]` point to the remaining command-line arguments.
  - `argv` has one additional element, `argv[argc]`, which is always a **null pointer**--a special pointer that points to nothing.
## The Preprocessor
- The `#define` and `#include` directives (and other directives) are handled by the **preprocessor**, a piece of software that edits C programs just prior to compilation.
### How the Preprocessor Works
- The behavior of the preprocessor is controlled by **preprocessing directives:** commands that begin with a `#` character.
- The `#define` directive defines a **macro**--a name that represents something else, such as a constant or frequently used expression.
  - When the macro is used later in the program, the preprocessor "expands" the macro, replacing it by its defined value.
- The `#include` directive tells the preprocessor to open a particular file and "include" its contents as part of the file being compiled.
- ```
       C program
           ↓
    ╭──────────────╮
    │ Preprocessor │
    ╰──────────────╯
           ↓
  Modified C program
           ↓
      ╭──────────╮
      │ Compiler │
      ╰──────────╯
           ↓
       Object code
### Preprocessing Directives
- Most preprocessing directives fall into one of three categories:
  - **Macro definition**
    - The `#define` directive defines a macro; the `#undef` directive removes a macro definition.
  - **File inclusion**
    - The `#include` directive causes the contents of a specified file to be included in a program.
  - **Conditional compilation**
    - The `#if`, `#ifdef`, `#ifndef`, `#elif`, `#else`, and `#endif` directives allow blocks of text to be either included in or excluded from a program, depending on conditions that can be tested by the preprocessor.
- The remaining directives--`#error`, `#line`, and `#pragma`--are more specialized and therefore used less often.
- Rules that apply to all directives:
  - **Directives always begin with the `#` symbol**
  - **Any number of spaces and horizontal tab characters may separate the tokens in a directive**
    - `#      define      N      100`
  - **Directives always end at the first new-line character, unless explicity continued**
    - ```c
      #define DISK_CAPACITY (SIDES *             \
                             TRACKS_PER_SIDE *   \
                             SECTORS_PER_TRACK * \
                             BYTES_PER_SECTOR)
  - **Directives can appear anywhere in a program**
  - **Comments may appear on the same line as a directive**
### Macro Definitions
#### Simple Macros
- The definition of a **simple macro** (or **object-like macro**, as it's called in the C standard) has the form:
  - `#define identifier replacement-list`
  - *replacement-list* is any sequence of **preprocessing tokens**.
- Using `#define` to create names for constants has several significant advantages:
  - **It makes programs easier to read**
  - **It makes programs easier to modify**
  - **It helps avoid inconsistencies and typographical errors**
- Other applications:
  - **Renaming types**
  - **Controlling conditional compilation**
    - i.e. differenc compilation in `DEBUG` mode
#### Parameterized Macros
- The definition of a **parameterized macro** (also known as a **function-like macro**) has the form:
  - `#define identifier( x1, x2, ..., xn ) replacement_list`
  - Where x1, x2, ..., xn are identifiers (the macro's **parameters**).
  - ```c
    #define MAX(x,y)   ((x) > (y) ? (x) : (y))
    #define IS_EVEN(n) ((n) % 2 == 0)
- Using a parameterized macro instead of a true function has a couple of advantages:
  - **The program may be slightly faster**
  - **Macros are "generic"**
    - Macro parameters, unlike function parameters, have no particular type.
- But parameterized macros also have disadvantages:
  - **The compiled code will often be larger**
  - **Arguments aren't type-checked**
  - **It's not possible to have a pointer macro**
  - **A macro may evaluate its arguments more than once**
#### The `#` Operator
- The `#` operator converts a macro argument into a string literal.
#### The `##` Operator
- The `##` operator can "paste" two tokens together to form a single token.
  - `#define MK_ID(n) i##n`
  - Called: `int MK_ID(1), MK_ID(2), MK_ID(3);`
  - Becomes: `int i1, i2, i3;`
#### General Properties of Macros
  - **A macro's replacement list may contain invocations of other macros**
    - ```c
      #define PI     3.14159
      #define TWO_PI (2*PI)
    - When it encounters `TWO_PI` later in the program, the preprocessor replaces it by `(2*PI)`.
    - The preprocessor then **rescans** the replacement list to see if it contains invocations of other macros.
  - **The preprocessor replaces only entire tokens, not portions of tokens**
  - **A macro definition normally remains in effect until the end of the file in which it appears**
  - **A macro may not be defined twic unless the new definition is identical to the old one**
  - **Macros may be "undefined" by the `#undef` directive**
    - `#undef identifier`
#### Creating Longer Macros
- The comma operator can be useful for creating more sophisticated macros by allowing us to make the replacement list a series of expressions.
  - ```c
    #define ECHO(s) (gets(s), puts(2))
    
    ECHO(str);   /* becomes (gets(str), puts(str)); */
#### Predefined Macros
- C has several predefined macros:
  - | Name | Description |
    | ---  | ---         |
    | `__LINE__` | Line number of file being compiled |
    | `__FILE__` | Name of file being compiled |
    | `__DATE__` | Date of compilation (in the form "MMM DD YYYY") |
    | `__TIME__` | Time of compilation (in the form "hh:mm:ss") |
    | `__STDC__` | 1 if the compilerr conforms  to the C standard (C89 or C99) |
#### Additional Predefined Macros in C99
- C99 provides a few additional predefined macros:
  - | Name | Description |
    | ---  | ---         |
    | `__STDC__HOSTED__` | 1 if this is a hosted implementation; 0 if it is freestanding |
    | `__STDC__VERSION__` | Version of C standard supported |
    | `__STDC_IEC_559__` | 1 if IEC 60559 floating-point arithmetic is supported |
    | `__STDC_IEC_559_COMPLEX__` | 1 if IEC 60559 complex arithmetic is supported |
    | `__STDC_ISO_10646__` | `yyyymmL` if `wchar_t` values match the ISO 10646 standard of the specified year and month |
#### Macros with a Variable Number of Arguments
- The primary reason for having a macro with a variable number of arguments is that it can pass these arguments to a function that accepts a variable number of arguments, such as `printf` or `scanf`.
  - ```c
    #define TEST(condition, ...) ((condition)? \
    printf("Passed test: %s\n", #condition):   \
    printf(__VA_ARGS__))
#### The `__func__` Identifier
- Every function has access to the `__func__` identifier, which behaves like a string variable that stores the name of the currently executing function.
  - `static const char __func__[] = "function-name";`
### Conditional Compilation
- The C preprocessor recognizes a number of directives that support **conditional compilation**--the inclusion or exclusion of a section of program text depending on the outcome of a test performed by the preprocessor.
#### The `#if` and `#endif` Directives
- ```c
  #define DEBUG 1

  #if DEBUG
  printf("Value of i: %d\n", i);
  printf("Value of j: %d\n", j);
  #endif
- In general, the `#if` directive has the form:
  - `#if constant-expression`
- In general, the `#endif` directive has the form:
  - `#endif`
#### The `defined` Operator
- When `defined` is applied to an identifier, it produces the value `1` if the identifer is a currently defined macro.
  - ```c
    #if defined(DEBUG)
    ...
    #endif
#### The `#ifdef` and `#ifndef` Directives
- The `#ifdef` directive tests whether an identifier is currently defined as a macro.
  - `#ifdef identifier`
- Using `#ifdef` is similar to using `#if`:
  - ```c
    #ifdef identifier
    // Lines to be included if identifer is defined as a macro
    #endif
- The `#ifndef` directive is similar to `#ifdef`, but tests whether an identifier is **not** defined as a macro.
  - `#ifndef identifier`
#### The `#elif` and `#else` Directives
- `#if`, `#ifdef`, and `#ifndef` blocks can be nested just like ordinary `if` statements.
  - ```c
    #if expr1
    \\ Lines to be included if expr1 is nonzero
    #elif expr2
    \\ Lines to be included if expr1 is zero but expr2 is nonzero
    #else
    \\ Lines to be included otherwise
    #endif
#### Uses of Conditional Compilation
- **Writing programs that are portable to several machines or operating systems**
  - ```c
    #if defined(WIN32)
    ...
    #elif defined(MAC_OS)
    ...
    #elif defined(LINUX)
    ...
    #endif
- **Writing programs that can be compiled with different compilers**
- **Providing a default definition for a macro**
- **Temporarily disabling code that contains comments**
### Miscellaneous Directives
#### The `#error` Directive
- The `#error` directive has the form:
  - `#error message`
  - Where `message` is any sequence of tokens.
  - If the preprocessor encounters an `#error` directive, it prints an error message which must include `message`.
#### The `#line` Directive
- The `#line` directive has two forms. In one form, we specify a line number:
  - `#line n`
- In the second form of the ``#line` directive, both a line number and a file name are specified:
  - `#line n "file"`
- One effect of the `#line` directive is to change the value of the `__LINE__` macro (and possibly the `__FILE__` macro).
## Writing Large Programs
### Header Files
- If we want several source files to have access to the same information, we'll put that information in a file and then use `#include` to bring the file's contents into each of the soruce files.
- Files that are included in the fashion are called **header files** (or sometimes **include files**).
- By convention, header files ahve the extension `.h`.
  - `#include tokens`
### Building a Multiple-File Program
- **Compiling**
  - Each source file in the program must be compiled separately.
  - Header files don't need to be compiled; the contents of a header file are automatically compiled whenever a source file that includes it is compiled.
- **Linking**
  - The linker combines the object files created in the previous step--along with code for library functions--to produce an executable file.
  - Among other duties, the linker is responsible for resolving external references left behind by the compiler.
#### Makefiles
- To make it easier to build large programs the concept of the **makefile** exists, a file containing the information necessary to build a program.
- A make file not only lists the files that are part of the program, but also describes **dependencies** among the files.
- Example
  - ```make
    justify: justify.o word.o line.o
            gcc  -o justify justify.o word.o line.o
    
    justify.o: justify.c word.h line.h
            gcc -c justify.c

    word.o: word.c word.h
            gcc -c word.c

    line.o: line.c line.h
            gcc -c line.c
- Each group of lines is known as a **rule**.
- The first line in each rule gives a **target** file, followed by the files on which  it depends.
- The second line is a **command** to be executed if the target should need to be rebuilt because of a  change to  one of  its dependent files.
- Details to know:
  - Each command in a makefile must be preceded by a tab character, **not a series of space**
  - A makefile is normally stored in a file name `Makefile` (or `makefile`).
  - To invoke `make`, use the command: `make target`
  - If no target is specified when `make` is invoked, it will build the target of the first rule.
#### Errors During Linking
- Some of the most common causes:
  - **Misspellings**
  - **Missing files**
  - **Missing libraries**
## Structures, Unions, and Enumerations
### Structure Variables
- The properties of a **structure** are quite different from those of an array.
- The elements of a structure (its **members**) aren't required to have the same type.
- Furthermore, the members of a structure have names; to select a particular member, we specify its name, not position.
#### Declaring Structure Variables
- ```c
  struct {
    int number;
    char name[NAME_LEN+1];
    int on_hand;
  } part1, part2;
- In memory:
  - ```
          ╭───╮
          │...│
          ├───┤
     2000 │   │ ─╮
          ├───┤  │
     2001 │   │  │
          ├───┤  ├─ number
     2002 │   │  │
          ├───┤  │
     2003 │   │ ─╯
          ├───┤
     2004 │   │ ─╮
          ├───┤  │
          │,,,│  ├─ name
          ├───┤  │
     2029 │   │ ─╯
          ├───┤
     2030 │   │ ─╮
          ├───┤  │
     2031 │   │  │
          ├───┤  ├─ on_hand
     2032 │   │  │
          ├───┤  │
     2033 │   │ ─╯
          ├───┤
          │...│
          ╰───╯
- Each structure represent a new scop; any name declared in that scope won't conflict with other names in a program.
- Each structure has a separate **name space** for its members.
#### Initializing Structure Variables
- ```c
  struct {
    int number;
    char name[NAME_LEN+1];
    int on_hand;
  } part1 = {528, "Disk drive", 10},
    part2 = {914, "Printer cable", 5};
#### Designated Initializers
- `{528, "Disk drive", 10}` is equivalant to `{.on_hand = 10, .name = "Disk drive", .number = 528}`
### Structure Types
#### Declaring a Structure Tag
- A **structure tag** is a name used to identify a particular kind of structure.
  - ```c
    struct part {
        int number;
        char name[NAME_LEN+1];
        int on_hand;
    };

    struct part part1, part2;
### Nested Arrays and Structures
- Structures and arrays can be combined without restriction.
- Arrays may have structures as their elements,  and structures may contain arrays and structures as members.
#### Nested Structures
- ```c
  struct person_name {
    char first[FIRST_NAME_LEN+1];
    char middle_initial;
    char last[LAST_NAME_LEN+1];
  };

  struct student {
    struct person_name name;
    int id, age;
    char sex;
  } student1, student2;

  strcpy(student1.name.first, "Fred");
### Unions
- A **union**, like a structure, consists of one or more members, possibly of different types.
- However, the compiler allocates only enough space for the largest of the members,  which overlay each other within this space.
- Union:
  - ```c
    union {
        int i;
        double d;
    } u;
- Structure:
  - ```c
    struct {
        int i;
        double d;
    } s;
- The members of `s` are stored at *different* addresses in memory, while the members of `u` are stored at the `same` address.
- *Assuming that `int` values require four bytes and `double` values take eight bytes:*
  - ```
            Structure                 Union
           ╭─────────╮             ╭─────────╮
        ╭─ │         │          ╭─ │         │ ─╮
        │  ├─────────┤          │  ├─────────┤  │
        │  │         │          │  │         │  │
     i ─┤  ├─────────┤       i ─┤  ├─────────┤  │
        │  │         │          │  │         │  │
        │  ├─────────┤          │  ├─────────┤  │
        ╰─ │         │          ╰─ │         │  │
           ├─────────┤             ├─────────┤  ├─ d
           │         │ ─╮          │         │  │
           ├─────────┤  │          ├─────────┤  │
           │         │  │          │         │  │
           ├─────────┤  │          ├─────────┤  │
           │         │  │          │         │  │
           ├─────────┤  │          ├─────────┤  │
           │         │  │          │         │ ─╯
           ├─────────┤  ├─ d       ╰─────────╯
           │         │  │               u
           ├─────────┤  │
           │         │  │
           ├─────────┤  │
           │         │  │
           ├─────────┤  │
           │         │ ─╯
           ├─────────┤
           │         │
           ╰─────────╯
                s
- Since the compiler overlays storage for the members of a union,  chanbging one member alters any value previously stored in any of the other members.  Thus, if we store a value in `u.d`, any value previously stored in `u.i` will be lost.
- Similarly, changing `u.i` corrupts `u.d`.
- Because of this property, we can think of `u` as a place to store either `i` *or* `d`, not both. (The structure `s` allows us to store `i` *and* `d`.)
- The properties of unions are almost identical to the properties of structures.
- We can declare union tags and union t ypes in the same way we declare structure tags and types.
### Enumerations
- C provides a special kind of type designed specifically for variables that have a small number of possible values.
- An **enumerated type** is a type whose values are listed ("enumerated") by the programmer, who must create a name (an **enumeration constant**) for each of the values.
  - `enum {CLUBS, DIAMONDS, HEARTS, SPADES} s1, s2;
#### Enumeration Tags and Type Names
- ```c
  enum suit {CLUBS, DIAMONDS, HEARTS, SPADES};

  enum suit s1, s2;
## Advanced Uses of Pointers
### Dynamic Storage Allocation
- C supports **dynamic storage allocation:** the ability to allocate storage during program execution.
#### Memory Allocation Functions
- To allocate storage dynamically, we'll need to call one of the three memory allocation functions declared in the `<stdlib.h>` header:
  - `malloc` -- Allocates a block of memory but doesn't initialize it.
  - `calloc` -- Allocates a block of memory and clears it.
  - `realloc` -- Resizes a previously allocated block of memory.
#### Null Pointers
- When a  memory allocation function is called, there's a possibility that it won't be able to locate a block of memory  large enough to satisfy the request.
- If that happens, the function will return a **null pointer**.
- A *null pointer* is a "pointer to nothing"--a special value that can be distinguished from all valid pointers.
- The null pointer is represented by a macro named `NULL`, so we can test `malloc`'s return value the following way:
  - ```c
    p = malloc(10000);
    if (p == NULL) {
        /* allocation failed; take appropriate action */
    }
- Or:
  - ```c
    if ((p = malloc(10000)) == NULL) {
        /* allocation failed; take appropriate action */
    }
- The `NULL` macro is defined in six headers: `<locale.h>`, `<stddef.h>`, `<stdio.h>`, `<stdlib.h>`, `<string.h>`, and `<time.h>`. (Also, in `<wchar.h>` in C99).
### Dynamically Allocated Strings
#### Using `malloc` to Allocate Memory for a String
- The `malloc` function has the following prototype:
  - `void *malloc(size_t size);`
- `malloc` allocates a block of `size` bytes and returns a pointer to it.
- Using `malloc` to allocate memory for a string:
  - `p = malloc(n + 1);`
- Or, cast the return value:
  - `p = (char *) malloc(n + 1);`
### Dynamically Allocated Arrays
#### Using `malloc` to Allocate Storage for an Array
- ```c
  int *a;
  a = malloc(n * sizeof(int));
### Deallocating Storage
- `malloc` and the other memory allocation functions obtain memory blocks from the **heap**.
- A block of memory that's no longer accessible to a program is said to be **garbage**.
- A program that leaves garbage behind has a **memory leak**.
- Some languages provide a **garbage  collector** that automatically locates the recycles garbage, but doesn't.
- Each C program is responsible for recycling its own garbage by calling the `free` function to release unneeded memory.
#### The `free` Function
- The `free` function has the following prototype in `<stdlib.h>`:
  - `void free(void *ptr);`
- Using `free`, pass a pointer to a memory block that we no longer need:
  - ```c
    p = malloc(...);
    q = malloc(...);
    free(p);
    p = q;
#### The "Dangling Pointer" Problem
- The call `free(p)` deallocates the memory block that `p` points to,  but doesn't change `p` itself.
- Attempting to access or modify a deallocated memory block causes undefined behavior.
- Trying to modify a deallocated memory block is likely to have disastrous consequences that may include a program crash.
### Linked Lists
- A **linked list** consists of a chain of structures (called **ndoes**), with each node containing a pointer to the next node in the chain:
  - ```
    ╭───┬───╮  ╭───┬───╮  ╭───┬───╮
    │   │  ─┼─→│   │  ─┼─→│   │─┼─│
    ╰───┴───╯  ╰───┴───╯  ╰───┴───╯
- The last node in the list contains a null pointer.
#### Declaring a Node Type
- ```c
  struct node {
    int value;         /* data stored in the node  */
    struct node *next; /* pointer to the next node */
  };
  
  struct node *first = NULL;
#### Creating a Node
- Requires three steps:
  1) Allocate memory for the node.
  2) Store data in the node.
  3) Insert the node into the list.
- ```c
  struct node *new_node;
  new_node = malloc(sizeof(struct node));
  (*new_node).value = 10;
- ```
             ╭─────╮  ╭─────┬─────╮
    new_node │   ──┼─→│     │     │
             ╰─────╯  ╰─────┴─────╯
                     value   next
#### The -> Operator
- Accessing amember of a structure using a pointer is so common that C provides a special operator just for this purpose.
- This operator, known as **right arrow selection**, is a  minus  sign followed by `>`.
  - `new_node->value = 10;` instead of `(*new_node).value = 0;`
#### Inserting a Node at the Beginning for a Linked List
- One of the advantages of a linke list is that nodes can be added at any point in the list.
#### Searching a Linked List
- **idiom:**
  - ```c
    for (p = first; p != NULL; p = p->next) {
        ...
    }
#### Deleting a Node from a Linked List
- Involves three steps:
  1) Locate the node to be deleted.
  2) Alter the previous node so that it "bypasses" the deleted node.
  3) Call `free` to reclaim the space occupied by the deleted node.
- Deleting the first node in the list is a special case.  The `prev == NULL` test checks for this case, which requires a different bypass step.
#### Ordered Lists
- When the nodes of a list are kept in order--sorted by the data stored inside teh nodes--we say that the list is **ordered**.
### Pointers to Functions
#### Function Pointers as Arguments
- We can use function pointers in much the same way we use pointers to data.
#### The `qsort` Function
- Since the elements of the array that it sorts may be of any type--even a structure or union type--`qsort` must be told how to determine which of two array elements is "smaller".
- We'll provide this information  to `qsort` by writing a **comparison function**.
### Restricted Pointers (C99)
- A pointer that's been declared using `restrict` is called a **restricted pointer**.
- The intent is that if `p` points to an object that is later modified, then that object is not accessed in a ny way other than through `p`.
- Having  more  than one  way to access an  object is often called **aliasing**.
- `restrict` provides information to the compiler that may enable it to produce more efficient code--a process known as **optimization**.
## Declarations
### Declaration Syntax
- In general, a declaration has the following appearance:
  - `declaration-specifiers declarators ;`
- **Declaration specifiers** describe the properties of the variables or functions being declared.
- **Declarators** give their names and may provide additional information about their properties.
- Declaration specifiers fall into three categories:
  - **Storage classes**
    - There are four storage classes: `auto`, `static`, `extern`, and `register`.
    - At most one storage  class may appear in a declaration; if present, it should come first.
  - **Type qualifiers**
    - In C89, there are only two type qualifiers: `const` and `volatile`.
    - C99 has a thrid type qualifier, `restrict`.
    - A declaration may contain zero or more type qualifiers.
  - **Type specifiers**
    - The keywords `void`, `char`, `short`, `int`, `long`, `float`, `double`, `signed`, and `unsigned` are all type specifiers.
  - C99 has a fourth kind of declaration specifier, the **function specifier**, which is used only in function declarations.
    - This category has just one member, the keyword `inline`.
- ```
  storage class    declarators
        ↓           ↙ ↓ ↘
      static float x, y, *p;
               ↑
         type specifier
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
  type qualifier declarator
        ↓           ↓
      const char month[] = "January";
              ↑                ↑
        type specifier    initializer
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
   storage class       type specifiers
         ↓              ↙    ↓  ↘
      extern const unsigned long int a[10];
               ↑                       ↑
         type qualifier            declarator
  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
  storage class   declarator
         ↓             ↓
      extern int square(int);
              ↑
        type specifier
### Storage Classes
#### Properties of Variables
- Every variable in a C program has three properties:
  - **Storage duration**
    - The storage duration of a variable determines when memory is set aside for the variable and when that memory is released.
    - Storage for a variable with **automatic storage duration** is allocated when the surrounding block is executed.
    - Storage is deallocated when the block terminates, cause the variable to lose its value.
    - A variable with **static storage duration** stays at the same storage  location as long as the program is running, allowing it to retain its value indefinitely.
  - **Scope**
    - The scope of a variable is the portion of the program text in which the variable can be referenced.
    - A variable can have either **block scope** (the variable is visible from its point of declaration to the end of the enclosing block) or **file scope** (the variable is visible from its point of declaration to the end of the enclosing file).
- **Linkage**
  - The linkage of a variable determines the extent to which it can be shared by different parts of a program.
  - A variable with **external linkage** may be shared by several (perhaps all) files in a program.
  - A variable with **internal linkage** is restricted to a single file, but may be shared by the functions in that file.
  - A variable with **no linkage** belongs to a single function and can't be shared at all.
- The default storage duration, scope, and linkage of a variable depend on where it's declared:
  - Variables declared *inside* a block (including a function body) have *automatic* storage duration, *block* scope, and *no* linkage.
  - Variables declared *outside* any block, at the outermost level of a program, have *static* storage  duration, *file* scope, and *external* linkage.
#### The `auto` Storage Class
- The `auto` storage class is legal only for variables that belong to a block.
- An `auto` variable has automatic storage duration, block scope, and no linkage.
- The `auto` storage class is almost never specifed explicitly, since it's the default for variables declared inside a block.
#### The `static` Storage Class
- The `static` storage class can be used with all variables, regardless of where they're declared, but it has a different effect on a variable declared outside a block than it does on a variable declared inside a block.
- When used `outside` a block, the word `static` specifies that a variable has internal linkage.
- When used `inside` a block, `static` changes the variable's storage duration from automatic to static.
- A `static` variable declared within a block resides at the same storage location throughout program execution.
- `static` variables have some interesting properties:
  - A `static` variable in a block is initialized only once, prior to program execution.
    - An `auto` variable is initialized every time it comes into existence.
  - Each time a function is called recursively, it gets anew set of `auto` variables.
    - If it has a `static` variable that variable is shared by all calls of the function.
  - Although a function shouldn't return a pointer to an  `auto` variable, there's nothing wrong iwth it returning a pointer to a `static` variable.
#### The `extern` Storage Class
- The `extern` storage class enables several source files  to share the same variable.
   - `extern int i;`
#### The `register` Storage Class
- Using the `register` storage class in the declaration of a variable asks the compiler to store the variable in a register instead of keeping it in main memory like other variables.
- A **register** is a storage area located in a computer's CPU. Data stored in a register can be accessed and updated faster than data stored in ordinary memory.
#### The Storage Class of a Function
- Function declarations and definitions, like variable declarations, may include a storage class, but the only options are `extern` and `static`.
- `extern` at the beginning of a function declaration specifies that the function has external linkage, allowing it to be called from other files.
- `static` indicates internal linkage, limiting use of the function's name to the file in which it's defined.
- If no storage class is specified, the function is assumed to have external linkage.
### Initializers
#### Uninitialized Variables
- Variables with *automatic* storage duration have no default initial value.
- Variables with *static* storage duration have the value zero by default.
## Program Design
### Modules
- When designing a C program it's often useful to view it as a number of independent **modules**.
  - A module is a collection of services, some of  which are made available to other parts of the program (the **clients**).
  - Each module has an **interface** that describes the available services.
- Dividing a program into modules has several advantages:
  - **Abstraction**
  - **Reusability**
  - **Maintainability**
#### Cohesion and Coupling
- In a well designed program, modules should have two properties:
  - **High cohesion**
    - The elements of each module should be closely related to one another.
  - **Low coupling**
    - Modules should be as independent of each other as possible.
#### Types of Modules
- Because of the need for high cohesion and low coupling, modules tend to fall into certain typical categories:
  - A **data pool** is a collection of related variables and/or constants. In  C, a module of this type is often just a header file. Example: `<float.h>` and `<limits.h>`.
  - A **library** is a collection of related functions. The `<string.h>` header, for example, is the interface to a library of string-handling functions.
  - An **abstract object** is a collection of functions that operate on a hidden data structure.
  - An **abstract data type (ADT)** is a type whose representation is hidden.
### Infomration Hiding
- Deliberately concealing information from the clients of a module is known as **information hiding**. It has two primary advantages:
  - **Security**
    - If clients don't know how the stack is stored, they won't be able to corrupt it by tampering with its internal workings.
  - **Flexibility**
    - Making changes--no matter how large--to a module's internal workings won't be difficult.
#### A Stack Module
- Using two implementations of a stack module:
  - One ussing an array
  - The other using a linked list
- It dosen't matter which we use to implement the stack module as long as both versions match the module's interface.
### Abstract Data Types
- A serious disadvantage of using a module that serves as an abstract object, like the stack module previously, there's no way to have multiple instances of the object (more than one stack).
- Need to go one step further and create a new *type*.
- For a `Stack` type:
  ``` C
  #define STACK_SIZE 100

  typedef struct {
    int contents[STACK_SIZE];
    int top;
  } Stack;

  void make_empty(Stacks *s);
  bool is_empty(const Stack *s);
  bool is_full(const Stack *s);
  void push(Stack *s, int i);
  int pop(Stack *s);
  ```
  ``` c
  Stack s1, s2;

  make_empty(&s1);
  make_empty(&s2);
  push(&s1, 1);
  push(&s2, 2);
  if (!is_empty(&s1)) {
    printf("%d\n", pop(&s1));    /* prints "1" */
  }
  ```
#### Encapsulation
- What is needed is a way to prevent clients from knowing how the `Stack` type is represented.
- C only has limited support for **encapsulating** types in this  way, newer languages are better suited for this.
#### Incomplete Types
- The C standard defines incomplete types as:
  > types that describe objects but lack  information needed to determine their sizes
  - Example:
    `struct t;   /* incomplete declaration of t */`
  - This declaration tells the compiler that `t` is a structure tag but desn't describe the members of the structure.
  - The intent is that an incomplete type will be completed elsewhere in the program.
  - `struct t s;   /*** WRONG ***/`
  - It is perfectly legal to define a pointer type that references an incomplete type:
  - `typedef struct t *T;`
  - The size of a pointer doesn't depend on what it points to, which is why C allows this.
### A Stack Abstract Data Type
#### Defining the Interface for the Stack ADT
- What the `stackADT.h` file will look like:
  ``` C
  #ifndef STACKADT_H
  #define STACKADT_H

  #include <stdbool.h>    /* C99 only */

  typedef struct stack_type *Stack;

  Stack create(void);
  void destroy(Stack s);
  void make_empty(Stack s);
  bool is_empty(Stack s);
  bool is_full(Stack s);
  void push(Stack s, int i);
  int pop(Stack s);

  #endif
  ```
- Clients that include `stackADT.h` will be able to declare variables of type `Stack`, each  o fwhich is capable of pointeing to a `stack_type` structure.
- Clients can then call the functions declared in `stackADT.h` to perform operations on stack variables.
- However, clients can't access the members of the `stack_type` structure, since that structure will be defined in a separate file.
### Design Issues for Abstract Data Types
#### Naming Conventions
- For example: `stack_create` instead of `create`
#### Error Handling
- Alternatively have the `push` and `pop` functions return a `bool` instead to indicate whether or not they succeeded.
#### Generic ADTs
- Would like to have a single 'generic' stack type from which we could create a stack of integers, a stack of string, or any other stack that we might need.
- The most common approach uses `void *` as the item type, which allows arbitrary pointers to be pushed and poped.
- For example:
  ``` C
  void push(Stack s,  void *p);
  void *pop(Stack s);
  ```
## Low-Level Programming
### Bitwise Operators
- C provides six **bitwise operators**, which operate on integerr data at the bit level.
#### Bitwise Shift Operators
| Symbol | Meaning |
| :-:    | :-:     |
| `<<`   | left shift |
| `>>`   | right shfit |
- *For portability, it is best to perform shifts only on unsigned numbers*
- Example:
  ``` C
  unsigned short i, j;

  i = 13;        /* i is now 13 (binary 0000000000001101) */
  j = i << 2;    /* j is now 52 (binary 0000000000110100) */
  j = i >> 2;    /* j is now  3 (binary 0000000000000011) */

  i = 13;        /* i is now 13 (binary 0000000000001101) */
  i <<= 2;       /* i is now 52 (binary 0000000000110100) */
  i >>= 2;       /* i is now 13 (binary 0000000000001101) */
  ```
#### Bitwise Complement, *And*, Exclusive *Or*, and Inclusive *Or*
- | Symbol | Meaning |
  | :-:    | :-:     |
  | `~`    | bitwise complement |
  | `&`    | bitwise *and* |
  | `^`    | bitwise exclusive *or* |
  | `|`    | bitwise inclusive *or* |
- Example:
  ``` C
  unsigned short i, j, k;

  i = 21;        /* i is now    21 (binary 0000000000010101) */
  j = 56;        /* j is now    56 (binary 0000000000111000) */
  k = ~i;        /* k is now 65514 (binary 1111111111101010) */
  k = i & j;     /* k is now    16 (binary 0000000000010000) */
  k = i ^ j;     /* k is now    45 (binary 0000000000101101) */
  k = i | j;     /* k is now    61 (binary 0000000000111101) */
  ```
#### Using the Bitwise Operators to Access Bits
##### Setting a bit
- Suppose that we want to set bit `4` of `i`.
- (Assume that the leftmost--or **most significant**--bit is numbered `15` and the least significant is numbered `0`).
- The easiest way to set bit `4` is to `or` the value of `i` with the constant `0x0010`:
  ``` C
  i =  0x0000;      /* i is now 0000000000000000 */
  i |= 0x0010;      /* i is now 0000000000010000*/
  ```
- **idom**    `i |= 1 << j;    /* sets bit j */`
##### Clearing a bit
- To clear bit `4` of `i`, we use a mask with a `0` bit in position `4` and `1` bits everywhere else:
  ``` C
  i  =  0x00ff;                /* i is now 0000000011111111 */
  i &= ~0x0010;              /* i is now 0000000011101111 */
  ```
- **idom**     `i &= ~(1 << j);        /* clears bit j */`
##### Testing a bit
- `if(i & 0x0010) ...      /* tests bit 4 */`
- **idom**     `if(i & 1 << j) ... /* tests bit j */`
- For ease of use, see this example:
  ``` C
  #define BLUE  1
  #define GREEN 2
  #define RED   4

  i |=  BLUE;       /* sets BLUE bit   */
  i &= ~BLUE;       /* clears BLUE bit */
  if(i & BLUE) ...  /* tests BLUE bit  */

  i |= BLUE | GREEN;           /* sets BLUE and GREEN bits   */
  i &= ~(BLUE | GREEN);        /* clears BLUE and GREEN bits */
  if(i & (BLUE | GREEN)) ...   /* tests BLUE and GREEN bits  */
  ```
#### Using the Bitwise Operators to Access Bit-Fields
- **bit-field**: A group of several consecutive bits.
##### Modifying a bit-field
- Requres a bitwise *and* (to clear the bit-field), followed by a bitwise *or* (to store new bits in the bit-field).
- `i = i & ~0x0070 | 0x0050;    /* stores 101 in bits 4-6 */`
- `i = (i & ~0x0070) | (j << 4);   /* stores j in bits 4-6 */`
##### Retrieving a bit-field
- When hte bit-field is at the right end of a number (in the leas significant bits), fetching its value is easy:
- `j = i & 0x0007;    /* retrieves bits 0-2 */`
- If the bit-field isn't at the right end, you can first shift the bit-field to the end before extracting:
- `j = (i >> 4) & 0x0007;    /* retrieves bits 4-6 */`
#### XOR Encryption
- One of the simplest ways to encrypt data is to exclusive-*or* (XOR) each character with a secret key.
- Example:
  ```
      00100110 (ASCII code for '&')
  XOR 01111010 (ASCII code for 'z')
      --------
      01011100 (ASCII code for `\`)
  ```
- To decrypt, just apply the same algorithm:
  ```
      00100110 (ASCII code for '&')
  XOR 01111010 (ASCII code for 'z')
      --------
      01011100 (ASCII code for '\')
  ```
### Bit-Fields in Structures
#### How Bit-Fields Are Stored
- The size of a storage  unit is implementation-defined; typical values are 8 bits, 16 bits, and 32 bits.
### Other Low-Level Techniques
#### Defining Machine-Dependent Types
- Since the `char` type--by definition--occupies one byte, we sometimes threat characters as bytes, using them to store data that's not necessarily in character form:
- `typedef unsigned char BYTE;`
#### The `volatile` Type Qualifier
- The `volatile` type qualifier allows us to inform the compiler if any of the data used in a  program is volatile.
- `volatile` typically appears in the declaration of a pointer variable that will point to a volatiel memory location:
- `volatile BYTE *p;    /* p will point to a volatile byte */`
- To see why `volatile` is needed, suppose that `p` points to a memory location that contains the mos recent character typed on the keyboard.
- This location is volatile: its value changes each time a character is entered.
- Might use the following loop to obtain characters from the keyboard and store in a buffer array:
  ``` C
  while(buffer not full) {
    wait for input;
    buffer[i] = *p;
    if(buffer[i++] == '\n') {
      break;
    }
  }
- A compiler might notice that this loop changes neither `p`  nor `*p`, so it could optimize the program by altering it so that `*p` is fetched just once:
  ``` C
  store *p in a register;
  while(buffer not full) {
    wait for input;
    buffer[i] = value stored in register;
    if(buffer[i++] == '\n') {
      break;
    }
  }
- The optimized program will fill the buffer with many copies of the same character; declaring that `p` points to volatile data avoids this problem by telling the compiler that `*p` must be fetched from  memory each time it's needed.
## The Standard Library
### Using the Library
#### Standard Library headers
  ```
  <assert.h>   <inttypes.h>*  <signal.h>   <stdlib.h>
  <complex.h>* <iso646.h>*    <stdarg.h>   <string.h>
  <ctype.h>    <limits.h>     <stdbool.h>* <tgmath.h>*
  <errno.h>    <locale.h>     <stddef.h>   <time.h>
  <fenv.h>*    <math.h>       <stdint.h>*  <wchar.h>*
  <float.h>    <setjmp.h>     <stdio.h>    <wctype.h>*
  * C99 only
  ```
#### Restrictions on Names Used in the Library
- Cannot use the names of macros defined in a header for any other purpose.
- Library names with file scope can't be redefined at the file level.
- Identifiers that begin with an underscore followed by an upper-case letter or a second underscore are reserved for use within the library.
- Identifies that begin with an underscore are reserved for use as identifiers and tags with file scope.
- Every identifier with external inkage in the standard libaray is reserved for use as an identifier with  external linkage.
### C89 Library Overview
#### `<assert.h>` Diagnostics
- Contains only the `assert` macro, which allows to insert self-check into a program.
- If any check fails, the program terminates.
#### `<ctype.h>` Character Handling
- Functions for classifying characters and for converting letters from lower to upper case or vice versa.
#### `<errno.h>` Errors
- Provides `errno` ("error number"), an lvalue that can be tested afterr a call of certain libarary functions to see if an error occured during the calls.
#### `<float.h>` Characteristics of Floating Types
- Provides macros that describe the characteristics of floating types, including range and accurary.
#### `<limits.h>` Size of Integer Types
- Macros that describe the characteristics of integer type (uncluding character types), including maximum and minimum values.
#### `<locale.h>` Localization
- Functions to help a  program adapt its behavior to a country or other geographic region.
#### `<math.h>` Mathematics
- Common mathematical functions, including trigonometric, hyperbolic, exponential, logarithmic, power, nearest integer, absolute value, and remainder functions.
#### `<setjmp.h>` Nonlocal Jumps
- Provides the `setjmp` and `longjmp` functions.
- `setjmp` marks a place in a program.
- `longjmp` can then be used to return to that place later.
#### `<signal.h>` Signal Handling
- Functions that deal with exceptional conditions (signals), including interupts and run-time errors.
#### `<stdarg.h>` Variable Arguments
- Tools for writing functions that, like `printf` and `scanf`, can have a variable number of arguments.
#### `<stddef.h>` Common Definitions
- Definitions of frequently used types and macros.
#### `<stdio.h>` Input/Output
- Large assortment of input/output functions, including operations on both sequential and random-access files.
#### `<stdlib.h>` General Utilities
- A "catchall" header for functions that don't fit into any of the other headers.
#### `<string.h>` String Handling
- Functions that perform string operations.
#### `<time.h>` Date and Time
- Functions for determining the time (and date).
### C99 Library changes
- Changes fall into three groups:
  - **Additional headers**
  - **Additional macros and functions**
  - **Enhanced versions of existing functions**
#### `<complex.h>` Complex Arithmetic
- Defines the `complex` and `I` macros, which are useful when working with complex numbers.
#### `<fenv.h>` Floating-Point Environment
- Access to floating-point status flags and control modes.
#### `<inttypes.h>` Format Conversion of Integer Types
- Defines macros that can be used in format strings for input/output of the integer types declared in `<stdint.h>`.
#### `<iso646.h>` Alternative Spellings
- Defines macros that represent certain operators (the ones containing the characters `&`, `|`, `~`, `!`, and `^`).
#### `<stdbool.h>` Boolean Type and Values
- Defines the `bool`, `true`, and `false` macros, as well as a macro that can be used to test whether these macros have been defined.
#### `<stdint.h> Integer Types
- Declares integer types with specified widths and defines related macros.
#### `<tgmath.h>` Type-Generic Math
- There are multiple versions of many math functions in the `<math.h>` and `<complex.h>` headers.
- The "type-generic" macros in `<tgmath.h>` can detect the types of the arguments passed to them and subsitute a call of the appropriate `<math.h>` or `<complex.h>` function.
#### `<wchar.h>` Extended Multibyte and Wide-Character Utilities
- Functions for wide-character input/output and wide string manipulation.
#### `<wctype.h>` Wide-Character Classification and Mapping Utilities
- The wide-character version of `<ctype.h>`.
### The `<stddef.h>` Header: Common Definitions
- The `<stddef.h>` header provides definitions of frequently used types and macros; it doesn't declare any functions. The types are:
  - `ptrdiff_t` The type of the result when two pointers are subracted.
    - Must be a signed type.
  - `size_t` The type returned by the `sizeof` operator.
    - Must be an unsigned type.
  - `wchar_t` A type large neough to represent all possible characters in all supported locales.
- The `<stddef.h>` headers also defines two macros:
  - `NULL` Which represents the null pointer.
  - `offsetof` Requires two arguments:
    - type: a structure type
    - member-designator: a member of the structure
### The `<stdbool.h>` Header (C99): Boolean Type and Values
- The `<stdbool.h>` header defines four macros:
  - `bool` (defined to be `_Bool`)
  - `true` (defined to be `1`)
  - `false` (defined to be `0`)
  - `__bool_true_false_are_defined` (defined to be `1`)
## Input/Output
- Now looking at functions that read and write unformatted data:
  - `getc`, `putc`, and related functions, which read and write one *character* at a time.
  - `gets`, `puts`, and related functions, which read and write one *line* at a time.
  - `fread` and `fwrite`, which read and write *blocks* of data.
### Streams
- The term **stream** means any source of input or any destination for output.
- Functions in `<stdio.h>` work equally well with all streams, not just ones that represent files.
#### File Pointers
- Accessing a stream in C is done through a **file pointer**, which has type `FILE*`.
- A program may declare any number of `FILE*` variables although operating systems usually limit the number of streams that can be open at one time.
#### Standard Streams and Redirection
- `<stdio.h>` provides three standard streams. They are ready to use--don't declare them, and don't open or close them.
  | **File Pointer** | **Stream** | **Default Meaning** |
  | :-:              | :-:        | :-:                 |
  | `stdin`          | Standard Input | Keyboard |
  | `stdout`         | Standard Output | Screen |
  | `stderr`         | Standard Error | Screen |
#### Text Files versus Binary Files
- `<stdio.h>` supports two kinds of files: text and binary
  - The bytes in a **text file** represent characters.
  - In a **binary file**, bytes don't necessarily represent characters.
### File Operations
#### Opening a File
  `FILE *fopen(const char * restrict filename, const char * restrict mode);`
- Opening a file for use as a stream requires a call of the `fopen` function.
- Note that `restrict` appears twice in the prototype for the `fopen` function.
- `restrict`, which is a C99 keyword, indicates that `filename` and `mode` should point to strings that don't share memory locations.
#### Modes
- For text files:

| **String** | **Meaning** |
| :-:        | :-:         |
| `"r"`      | Open for reading |
| `"w"`      | Open for writing (file need not exist) |
| `"a"`      | Open for appending (file need not exist) |
| `"r+"`     | Open for reading and writing, starting at beginning |
| `"w+"`     | Open for reading and writing (truncate if file exists) |
| `"a+"`     | Open for reading and writing (append if file exists) |

- For binary files:

| **String** | **Meaning** |
| :-: |  :-: |
| `"rb"` | Open for reading |
| `"wb"` | Open for writing (file need not exist) |
| `"ab"` | Open for appending (file need not exist) |
| `"r+b"` or `"rb+"` | Open for reading and writing, starting at beginning |
| `"w+b"` or `"wb+"` | Open for reading and writing (truncate if file exists) |
| `"a+b"` or `"ab+"` | Open for reading and writing (append if file exists) |

 #### Closing a File
 `int fclose(FILE *stream);`
 - The `fclose` function allows a program to close a file that it's no longer using.
#### Attaching a File to an Open Stream
`FILE *freopen(const char * restrict filename, const char * restrict mode, FILE * restrict stream);`
- `freopen` attaches a different file to a stream that's already open.
#### Obtaining File Names from the Command Line
`int main(int argc, char *argv[]) {}`
- `argc` is the number of command-line arguments.
- `argv` is an array of  pointers to the argument strings.
- `argv[0]` points to the program name.
- `argv[1]` through `argv[argc-1]` point to the remaining arguments.
- `argv[argc]` is a null pointer.
```
    argv
   ╭────╮
   │    │
  0│  ──┼────→ program name
   │    │
   ├────┤
   │    │     ╭─┬─┬─┬─┬─┬─┬─┬─┬─┬──╮
  1│  ──┼────→│n│a│m│e│s│.│d│a│t│\0│
   │    │     ╰─┴─┴─┴─┴─┴─┴─┴─┴─┴──╯
   ├────┤
   │    │     ╭─┬─┬─┬─┬─┬─┬─┬─┬─┬──╮
  2│  ──┼────→│d│a│t│e│s│.│d│a│t│\0│ 
   │    │     ╰─┴─┴─┴─┴─┴─┴─┴─┴─┴──╯
   ├────┤
   │    │
  3│    │
   │    │
   ╰────╯
```
#### File Buffering
``` c
int fflush(FILE *stream);
void setbuf(FILE * restrict stream,
            char * restrict buf);
int setvbuf(FILE * restrict stream,
            char * restrict buf,
            int mode, size_t size);
```
- The secret to achieving acceptable performance is **buffering**: data written to a stream is actually stored in a buffer area in memory; when it's full (or the stream is closed), the buffer is "flushed" (written to the actual output device).
- `fflush(fp);     /* flushes buffer for fp */`
- flushes the buffer for the file associated with `fp`.
- `fflush(NULL);   /* flushes all buffers */`
- flushes *all* output streams. `fflush` returns zero if it's successful and `EOF` if an error occurs.
- `setvbuf` allows to change the way a stream is buffered and to control the size and location of the buffer.
- The third argument, which specifies the kind of buffering desired, should be one of the following macros:
  - `_IOFBF` (full buffering) Data is read from the stream when the buffer is empty or written to the stream when it's full.
  - `_IOLBF` (line buffering) Data is read from the stream or written to the stream one line at a time.
  - `_IONBF` (no buffering) Data is read from the stream or written to the stream directly, without buffer.
#### Miscellaneous File Operations
``` c
int remove(const char *filename);
int rename(const char *old, const char *new);
```
- The functions `remove` and `rename` allow a program to perform basic file management operations.
- `remove` deletes a file:
  - `remove("foo");    /* deletes the file named "foo" */`
- `rename` changes the name of a file:
  - `rename("foo", "bar");    /* renames "foo" to "bar" */`
### Formatted I/O
#### The `...printf` Functions
``` c
int fprint(FILE * restrict stream, const char * restrict format, ...);
int printf(const char * restrict format, ...);
```
- The prototypes for both functions end with the `...` symbol (and **ellipsis**), which indicates a variable number of additional arguments.
#### `...printf` Conversion Specifications
```
      precision    conversion
          │         specifier
 flags    │   ╭─────────╯
   ↓      ↓   ↓
╭─┬──┬──┬──┬─┬─╮
│%│#0│12│.5│L│g│
╰─┴──┴──┴──┴─┴─╯
      ↑     ↑
   minimum  ╰────╮     
 field width   length
              modifier
```
- **Flags** (optional; more than one permitted)
  - | **Flag** | **Meaning** |
    | :-: | :-: |
    | `-` | Left-justify within field. (The default is right justification.) |
    | `+` | Numbers produced by signed conversions always begin with `+` or `-`. (Normally, only negative numbers are preceded by a sign.) |
    | *space* | Nonnegative numbers produced by signed conversions are preceded by a space. (The `+` flag overrides the *space* flag.) |
    | `#` | Octal numbers begin withi `0`, nonzero hexadecimal numbers with `0x` or `0X`.  Floating-point numbers always have a decimal point. Trailing zeros aren't removed from numbers printed with the `g` or `G` conversions. |
    | `0` | Numbers are padded with leading zeros up to the field width.  The `0` flag is ignored if the conversion is `d`, `i`, `o`, `u`, `x`, or `X` and a precision is specified. (The `-` flag overrides the `0` flag.) |
- ** Minimum field width** (optional)
  - An item that's too small to occupy this number of characters will be padded.
- **Precision** (optional)
  - The meaning of the precision depends on the conversion:
    - `d`, `i`, `o`, `u`, `x`, `X`: minimum number of digits (leading zeros are added if the number has fewer digits)
    - `a`, `A`, `e`, `E`, `f`, `F`: number of digits after the decimal point
    - `g`, `G`: number of significant digits
    - `s`: maximum number of bytes
- **Length modifier** (optional)
  - The presence of a length modifier indicates that the item to be displayed has a type that's longer or shorther than is normal for a particular conversion specification.
  - |**Length Modifier** |   **Conversion Specifiers**   | **Meaning** |
    | :----------------: | :---------------------------: | :---------: |
    | `hh`*              | `d`, `i`, `o`, `u`, `x`, `X`  | `signed char`, `unsigned char` |
    | `hh`*              | `n`                           | `signed char *` |
    | `h`                | `d`, `i`, `o`, `u`, `x`, `X`  | `short int`, `unsigned short int` |
    | `h`                | `n`                           | `short int *` |
    | `l`                | `d`, `i`, `o`, `u`, `x`, `X`  | `long int`, `unsigned long int` |
    | `l`                | `n`                           | `long int *` |
    | `l`                | `c`                           | `wint_t` |
    | `l`                | `s`                           | `wchar_t *` |
    | `l`                | `a`, `A`, `e`, `E`, `f`, `F`, `g`, `G` | no effect |
    | `ll`*              | `d`, `i`, `o`, `u`, `x`, `X`  | `long long int`, `unsigned long long int |
    | `ll`*              | `n`                           | `long long int *` |
    | `j`*               | `d`, `i`, `o`, `u`, `x`, `X`  | `intmax_t`, `uintmax_t` |
    | `j`*               | `n`                           | `intmax_t *` |
    | `z`*               | `d`, `i`, `o`, `u`, `x`, `X`  | `size_t` |
    | `z`*               | `n`                           | `size_t *` |
    | `t`*               | `d`, `i`, `o`, `u`, `x`, `X`  | `ptrdiff_t` |
    | `t`*               | `n`                           | `ptrdiff_t *` |
    | `L`                | `a`, `A`, `e`, `E`, `f`, `F`, `g`, `G` | `long double` |
    | * C99 Only | |
- **Conversion specifier**
  - The conversion specifier must be one of the characters listed in the below table.
  - | **Conversion Specifier** | **Meaning** |
    | :----------------------: | :---------: |
    | `d`, `i` | Converts an `int` value to decimal form. |
    | `o`, `u`, `x`, `X` | Converts an `unsigned int` value to base 8 (`o`), base 10 (`u`), or base 16 (`x`,`X`). `x` displays the hexadecimal digita `a`-`f` in lower case; `X` displays them in upper case. |
    | `f`, `F`* | Converts a `double` value to decimal form, putting the dcimal point in the correct position.  If no precision is specified, displays six digits after the decimal point. |
    | `e`, `E` | Converts a `double` value to scientific notation.  If no precision is specified, displays six digits after the decimal poit. If `e` is chosen, the exponent is preceded by the letter `e`; if `E` is chosen, the exponent is preceded by `E`. |
    | `g`, `G` | `g` converts a `double` value to either `f` form or `e` form. `e` form is selected if the number's exponent is less than `-4` *or* greater than or equal to the precision. Trailing zeros are not displayed (unless the `#` flag is used); a decimal point appears only when followeb by a digit. `G` chooses between `F` and `E` forms. |
    | `a`*, `A`* | Converts a `double` value to hexadecimal scientific notation using the form [-]0x*h.hhh*p±*d*, where [-] is an optional minus sign, the *h*'s represent hex digits, ± is either a plus or minus sign, and *d* is the exponent. *d* is a decimal number that represents a power of 2.  If no precision is specifed, enough digits are displayed after the decimal point to represent the exact value of the number (if possible). `a` displays the hex digits `a`-`f` in lower case; `A` displays tem in uppder case. The choice of `a` or `A` also affects the case fo the letters `x` and `p`. |
    | `c` | Displays an `int` value as an unsigned character. |
    | `s` | Writes the characters pointed to by the argument. Stops writing when the number of byte specified by the precision (if present) is reached or a null character is encountered. |
    | `p` | Converts a `void *` value to printable form. |
    | `n` | The corresponding argument must point to an object of type `int`.  Stores in this object then number of characters written so far by this call of `...printf`; produces no output. |
    | `%` | Write the character `%`. |
    | * C99 Only | |
    #### C99 Changeds to `...printf` Conversion Specifications
    - **Additional length modifiers**
      - Adds the `hh`, `ll`, `j`, `z`, and `t` length modifiers.
    - **Additional conversion specifies**
      - Adds the `F`, `a`, and `A` conversion specifiers.
    - **Ability to write infinity and NaN**
    - **Support for wide characters**
      - The `%lc` conversion specification is used to write a single wide character.
      - `%ls` is used for a string of wide characters
    - **Previously undefined conversion specifications now allowed**
#### Examples of `...printf` Conversion Specifications
- | **Conversion Specification** | **Result of Applying Conversion to `123`** | **Result of Applying Conversion to `-123`** |
  | :-----: | :--------: | :--------: |
  | `%8d`   | `·····123` | `····-123` |
  | `%-8d`  | `123·····` | `-123····` |
  | `%+8d`  | `····+123` | `····-123` |
  | `% 8d`  | `·····123` | `····-123` |
  | `%08d`  | `00000123` | `-0000123` |
  | `%-+8d` | `+123····` | `-123····` |
  | `%- 8d` | `·123····` | `-123····` |
  | `%+08d` | `+0000123` | `-0000123` |
  | `% 08d` | `·0000123` | `-0000123` |

- | **Conversion Specification** | **Result of Applying Conversion to `123`** | **Result of Applying Conversion to `123.0`** |
  | :----: | :--------: | :--------: |
  | `%8o`  | `·····173` | |
  | `%#8o` | `····0173` | |
  | `%8x`  | `······7b` | |
  | `%#8x` | `····0x7b` | |
  | `%8X`  | `······7B` | |
  | `%#8X` | `····0X7B` | |
  | `%8g`  | | `·····123` |
  | `%#8g` | | `·123.000` |
  | `%8G`  | | `·····123` |
  | `%#8G` | | `·123.000` |
- | **Conversion Specification** | **Result of Applying Conversion to `"bogus"`** | **Result of Applying Conversion to `"buzzword"`** |
  | :-: | :-: | :-: |
  | `%6s`    | `·bogus` | `buzzword` |
  | `%-6s`   | `bogus·` | `buzzword` |
  | `%.4s`   | `bogu`   | `buzz` |
  | `%6.4s`  | `··bogu` | `··buzz` |
  | `%-6.4s` | `bogu··` | `buzz··` |
- | **Number** | **Result of Applying `%.4g` Conversion to Number** |
  | - | - |
  | `123456.`            | `1.235e+05` |
  | ` 12345.6`           | `1.235e+04` |
  | `  1234.56`          | `1235` |
  | `   123.456`         | `123.5` |
  | `    12.3456`        | `12.35` |
  | `     1.23456`       | `1.235` |
  | `      .123456`      | `0.1235` |
  | `      .0123456`     | `0.01235` |
  | `      .00123456`    | `0.001235` |
  | `      .000123456`   | `0.0001235` |
  | `      .0000123456`  | `1.235e-05` |
  | `      .00000123456` | `1.235e-06` |
#### The `...scanf` Functions
``` c
int fscanf(FILE * restrict stream, const char * restrict format, ...);
int scanf(const char * restrict format, ...);
```
- `fscanf` and `scanf` read data items from an input stream, using a format string to indicate the layout of the input.
- `scanf` always reads from `stdin`.
- `fscanf` reads from the stream indicated by its first argument.
``` c
scanf("%d%d", &i, &j);        /* reads from stdin */
fscanf(fp, "%d%d", &i, &j);   /* reads from fp */
```
- The `...scanf` functions return prematurely if an **input failure** occurs (no more input characters could be read) or if a **matching failure** occurs (the input character didn't match the format string).
- In C99, an input failure can also occur because of an **encoding error**, which means that an attempt was made to read a multibyte character, but the input characters didn't correspond to any valid multibyte character.
**idom**:
``` c
while(scanf("%d", &i) == 1) {
  ...
}
```
#### `...scanf` Format String
- A `...scanf` format string may contain three things:
  - **Conversion specifications**
    - Conversion specifications in a `...scanf` format string resemble those in a `...printf` format string.
  - **White-space characters**
    - One or more consecutive white-space characters in a `...scanf` format string match zero or more white-space characters in the input stream.
  - **Non-white-space characters**
    - A non-white-space character other than `%` matches the same character in the input stream.
#### `...scanf` Conversion Specification
- A `...scanf` conversion specification consists of the character `%` followed by the items listed below (in the order shown):
  - `*` (optional). The presence of `*` signifies **assignment suppression**: an input item is read but not assigned to an object.  Items matched using `*` aren't included in the count that `...scanf` returns.
  - **Maximum field width** (optional). The maximum field width limits the number of characters in an input item; conversion of the item ends if this number is reached.
  - **Length modifier** (optional). The presence of a length modifier indicates that the object in which the input item will be store has a type that's longer or shorter than normal.
  - | **Length Modifier** | **Conversion Specifiers** | **Meaning** |
    |:-:|:-:|:-:|
    | `hh`\* | `d`, `i`, `o`, `u`, `x`, `X`, `n` | `signed char *`, `unsigned char *` |
    | `h` |  `d`, `i`, `o`, `u`, `x`, `X`, `n` | `short int *`, `unsigned short int *` |
    | `l` | `d`, `i`, `o`, `u`, `x`, `X`, `n` | `long int *`, `unsigned long int *` |
    | `l` | `a`, `A`, `e`, `E`, `f`, `F`, `g`, `G` | `double *` |
    | `l` | `c`, `s`, or `[` | `wchar_t *` |
    | `ll`* | `d`, `i`, `o`, `u`, `x`, `X`, `n` | `long long int *`, `unsigned long long int` |
    | `j`\* | `d`, `i`, `o`, `u`, `x`, `X`, `n` | `intmax_t *`, `uintmax_t *` |
    | `z`\* | `d`, `i`, `o`, `u`, `x`, `X`, `n` | `size_t *` |
    | `t`\* | `d`, `i`, `o`, `u`, `x`, `X`, `n` | `ptrdiff_t *` |
    | `L` | `a`, `A`, `e`, `E`, `f`, `F`, `g`, `G` | `long double *` |
    | * C99 Only | ||
  - **Conversion specifier**
  - |**Conversion Specifier** | **Meaning** |
    | :-: | :-: |
    | `d` | Matches decimal integer, assumed to be type `int *`. |
    | `i` | Matches an integer, assumed to have type `int *`, assumed to be base 10 unless specified otherwise |
    | `o` | Matches an octal integer, assumed to have type `unsigned int *` |
    | `u` | Matches a decimal integer, assumed to have type `unsigned int *` |
    | `x`, `X` | Matches a hexadecimal integer, assumed to have type `unsigne int *` |
    | `a`\*, `A`\*, `e`, `E`, `f`, `F`\*, `g`, `G` | Matches a floating-point number, assumed to have type `float *` |
    | `c` | Matches *n* characters, where *n* is the max field width, assumed to be a pointer to a character array. Doesn't add a null character to the end. |
    | `s` | Matches a sequence of non-white-space characters, then adds a null character at the end, assumed to be a pointer to a character array. |
    | `[` | Matches a nonempty sequence of characters from a scanset, then adds a null character at the end, assumed to be a pointer to a character array. |
    | `p` | Matches a pointer value in the form that `...printf` would have written it, assumed to be a pointer to a `void *` object. |
    | `n` | The corresponding argument must point to an object of type `int`. Stores in this object the number of characters read so far by this call of `...scanf`. No input is consumed and the return value of `...scanf` isn't affected. |
    | `%` | Matches the character `%` |
    | * C99 Only | |
#### C99 Changes to `...scanf` Conversion Specifications
- **Additional length modifiers**
  - C99 adds the `hh`, `ll`, `j`, `z`, and `t` length modifiers.
- **Additional conversion specifiers**
  - C99 adds the `F`, `a`, and `A` conversion specifiers.
- **Ability to read infinity and NaN**
- **Support for wide characters**
#### Detecting End-of-File and Error Conditions
``` c
void clearerr(FILE *stream);
int feof(FILE *stream);
int ferror(FILE *stream);
```
- Then `...scanf` function is asked to read and store *n* data itmes; the return value is expected to be *n*.
- If the return value is less than *n*, there are three posibilities:
  - **End-of-file** The function encountered end-of-file before matching the format string completely.
  - **Read error** The function was unable to read characters from the stream.
  - **Matching failure** A data item was in the wrong format.
### Character I/O
#### Output Functions
``` c
int fputc(int c, FILE *stream);
int putc(int c, FILE *stream);
int putchar(int c);
```
- `putchar` write one character to the `stdout` stream:
  - `putchar(ch);   /* writes ch to stdout */`
- `fputc` and `putc` are more general versions of `putchar` that write a character to an arbitrary stream:
  - `fputc(ch, fp);   /* writes ch to fp */`
  - `putc(ch, fp);    /* writes ch to fp */`
#### Input Functions
``` c
int fgetc(FILE *stream);
int getc(FILE *stream);
int getchar(void);
int ungetc(int c, FILE *stream);
```
- `getchar` reads a character from the `stdin` stream:
  - `ch = getchar();   /* reads a character from stdin */`
- `fgetc` and `getc` read a character from an arbitrary stream:
  - `ch = fgetc(fp);   /* reads a character from fp */`
  - `ch = getc(fp);    /* reads a character from fp */`
**idom**

```c
while((ch = getc(fp)) != EOF) {
  ...
}
```

- `ungetc` "pushes back" a character read from a stream and clears the stream's end-of-file indicator:

  - ```c
      while(isdigit(ch = getc(fp))) {
        ...
      }
      ungetc(ch, fp);   /* pushes back last character read */

### Line I/O
#### Output Functions

``` c
int fputs(const char * restrict s, FILE * restrict stream);
int puts(const char *s);
```

- `puts` Writes a string of characters to `stdout`:
  - `puts("Hi, there!");   /* writes to stdout */`
- `fputs` A more general version of `puts`, it's second argument indicates the stream to which the output should be written:
  - `fptus("Hi, there!", fp);    /* writes to fp */`
#### Input Functions
``` c
char *fgets(char * restrict s, int n, FILE * restrict stream);
char *gets(char *s);
```

- `gets` reads a line of input from `stdin`:
  - `gets(str);   /* reads a line from stdin */`
- `fgets` A more general version of `gets` that can read from any stream. Safer that `gets` since it limits the number of characters:
  - `fgets(str, sizeof(str), fp);    /* reads a line from fp */`
### Block I/O
``` c
size_t fread(void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);
size_t fwrite(const void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);
```

- `fwrite` Designed to copy an array from memory to a stream. First argument is array address, second is the size of each array element (in bytes), third is the number of elements:
  - `fwrite(a, sizeof(a[0]), sizeof(a) / sizeof(a[0]), fp);`
- `fread` Read the elements of an array from a stream, argumens are similar to `fwrite`:
  - `n = fread(a, sizeof(a[0]), sizeof(a) / sizeof(a[0]), fp); `
### File Positioning
``` c
int fgetpos(FILE * restrict stream, fpos_t * restrict pos);
int fseek(FILE *stream, long int offset, int whence);
int fsetpos(FILE * stream, const fpos_t *pos);
long int ftell(FILE *stream);
void rewind(FILE *stream);
```
- Every stream has an associated **file position**
- `fseek` The function changes the file position associated with the first argument (a file pointer), the third specifies whether the new position is to be calculate with respect to the beginning of the file, the current position, or the end of the file.
  - `<stdio.h>` defines three macros for this purpose:
    - `SEEK_SET` Beginning of file
    - `SEEK_CUR` Current file position
    - `SEEK_END` End of file
  - The second argument is a (possibly negative) byte count to move to the beginning of a file.
  - ``` C
    fseek(fp, 0L, SEEK_SET);   /* move to beginning of file */
    fseek(fp, 0L, SEEK_END);   /* move to end of file */
    fseek(fp, -10L, SEEK_CUR); /* move back 10 bytes */
    ```
- `ftell` Returns the current file position as a long integer.
  - ``` C
    long file_pos;
    ...
    file_pos = ftell(p);            /* saves current position */
    ...
    fseek(fp, file_pos, SEEK_SET);  /* returns to old position */
    ```
- `rewind` Sets the file position at the beginning.
  - The call `rewind(fp)` is nearly equivalant to `fseek(fp, 0L, SEEK_SET);`, the difference being that rewind doesn't return a value but does clear the error indicator for `fp`.
- `fgetpos` and `fsetpos` can handle large file because they use values of type `fpos_t` to represent file position.
  - An `fpos_t` value isn't neccessarily an integer; it could be a structure.
  - ``` C
    fpos_t file_pos;
    ...
    fgetpos(fp, &file_pos);    /* saves current position */
    ...
    fsetpos(fp, &file_pos);    /* returns to old position */
    ```
### String I/O
#### Output Functions

- ``` C
  int sprintf(char * restrict s, const char * restrict format, ...);
  int snprintf(char * restrict s, size_t n, const char * restrict format, ...);
  ```

- `sprintf` Similar to `printf` and `fprintf` except that it writes output into a character array (pointed to by the first argument) instead of a stream.
  - `sprintf(date, "%d/%d/%d", 9, 20, 2010);`
- `snprintf` Same as `sprintf` except for the additional parameter `n`, no more than `n-1` characters will be written to the string, not counting the terminating null character.
  - `snprintf(name, 13, "%s, %s", "Einstein", "Albert");`
#### Input Functions

``` C
int sscanf(const char * restrict s, const char * restrict format, ...);
```

- `sscanf` Similar to `scanf` and `fscanf` except that it reads from a string instead of reading from a stream.
  - ``` C
    fgets(str, sizeof(str), stdin);   /* reads a line of input */
    sscanf(str, "%d%d", &i, &j);      /* extracts two integers */
    ```
## Library Support for Numbers and Character Data
### The `<float.h>` Header: Characteristics of Floating Types
- Two macros apply to all floating types:
  - `FLT_ROUNDS` represents the current rounding direction for floating-point addition.
    - | **Value** | **Meaning** |
      | :-:       | :-:         |
      | `-1` | Indeterminable |
      | `0` | Toward zero |
      | `1` | To nearest |
      | `2` | Toward positive infinity |
      | `3` | Toward negative infinity |
  - `FLT_RADIX` specifies the radix of exponent representation; it has a minimum value of 2 (indicating binary representation).
- The remaining macros describe the characteristics of specifi types. 
  - Significant-Digit Macros:
    - | **Name** | **Value** | **Description** |
      | :-: | :-: | :-: |
      | `FLT_MANT_DIG` | | Number of significant digits (base `FLT_RADIX`) |
      | `DBL_MANT_DIG` | | |
      | `LDBL_MANT_DIG` | | |
      | `FLT_DIG` | ≥6 | Number of significant digits (base 10) |
      | `DBL_DIG` | ≥10 | |
      | `LDBL_DIG` | ≥10 | |
  - Exponent Macros:
    - | **Name** | **Value** | **Description** |
      | :-: | :-: | :-: |
      | `FLT_MIN_EXP` | | Smallest (most negative) power to which `FLT_RADIX` can be raised |
      | `DBL_MIN_EXP` | | Smallest (most negative) power to which `FLT_RADIX` can be raised |
      | `LDBL_MIN_EXP` | | Smallest (most negative) power to which `FLT_RADIX` can be raised |
      | `FLT_MIN_10_EXP` | ≤-37 | Smallest (most negative) power to which 10 can be raise |
      | `DBL_MIN_10_EXP` | ≤-37 | Smallest (most negative) power to which 10 can be raise |
      | `LDBL_MIN_10_EXP` | ≤-37 | Smallest (most negative) power to which 10 can be raise |
      | `FLT_MAX_EXP` | | Largest power to which `FLT_RADIX` can be raised |
      | `DBL_MAX_EXP` | | Largest power to which `FLT_RADIX` can be raised |
      | `LDBL_MAX_EXP` | | Largest power to which `FLT_RADIX` can be raised |
      | `FLT_MAX_10_EXP` | ≥+37 | Largest power to which 10 can be raised |
      | `DBL_MAX_10_EXP` | ≥+37 | Largest power to which 10 can be raised |
      | `LDBL_MAX_10_EXP` | ≥+37 | Largest power to which 10 can be raised |
  - Max, Min, and Epsilon Macros:
    - | **Name** | **Value** | **Description** |
      | :-: | :-: | :-: |
      | `FLT_MAX` | ≥10⁺³⁷ | Largest finite value |
      | `DBL_MAX` | ≥10⁺³⁷ | Largest finite value |
      | `LDBL_MAX` | ≥10⁺³⁷ | Largest finite value |
      | `FLT_MIN` | ≤10⁻³⁷ | Smallest positive value |
      | `DBL_MIN` | ≤10⁻³⁷ | Smallest positive value |
      | `LDBL_MIN` | ≤10⁻³⁷ | Smallest positive value |
      | `FLT_EPSILON` | ≤10⁻⁵ | Smallest representable difference between two numbers |
      | `DBL_EPSILON` | ≤10⁻⁹ | Smallest representable difference between two numbers |
      | `LDBL_EPSILON` | ≤10⁻⁹ | Smallest representable difference between two numbers |
  - Evaluation Methods:
    - | **Value** | **Meaning** |
      | :-: | :-: |
      | `-1` | Indeterminable |
      | `0` | Evaluate all operations and constants just to the range and precision of the type |
      | `1` | Evaluate operations and constants of type `float` and `double` to the range and precision of the `double` type |
      | `2` | Evaluate all operations and constants to the range and precision of the `long double` type |
### The `<limits.h>` Header: Sizes of Integer Types
- Character Macros:
  - | **Name** | **Value** | **Description** |
    | :-:| :-:| :-:|
    | `CHAR_BIT` | ≥8 | Number of bits per byte |
    | `SCHAR_MIN` | ≤-127 | Minimum `signed char` value |
    | `SCHAR_MAX` | ≥+127 | Maximum `signed char` value |
    | `UCHAR_MAX` | ≥255 | Maximum `unsigned char` value |
    | `CHAR_MIN` | * | Minimum `char` value |
    | `CHAR_MAX` | ** | Maximum `char` value |
    | `MB_LEN_MAX` | ≥1 | Maximum number of bytes per multibyte character in any supported locale |
    | * | `CHAR_MIN` is equal to `SCHAR_MIN` if `char` is treated as a signed type; otherwise `CHAR_MIN` is 0 |
    | ** | `CHAR_MAX` has the same value as either `SCHAR_MAX` or `UCHAR_MAX`, depending on whether `char` is tread as a signed type or unsigned type |
- Integer Macros:
  - | **Name** | **Value** | **Formula** | **Description** |
    | :-: |  :-: |  :-: |  :-: | 
    | `SHRT_MIN` | ≤-32767 | -(2¹⁵-1) | Minimum `short int` value |
    | `SHRT_MAX` | ≥+32767 | 2¹⁵-1 | Maximum `short int` value |
    | `USHRT_MAX` | ≥65535 | 2¹⁶-1 | Maximum `unsigned short int` value |
    | `INT_MIN` | ≤-32767 | -(2¹⁵-1) | Minimum `int` value |
    | `INT_MAX` | ≥+32767 | 2¹⁵-1 | Maximum `int` value |
    | `UINT_MAX` | ≥65535 | 2¹⁶-1 | Maximum `unsigned int` value |
    | `LONG_MIN` | ≤-2147483647 | -(2³¹-1) | Minimum `long int` value |
    | `LONG_MAX` | ≥+2147483647 | 2³¹-1 | Maximum `long int` value |
    | `ULONG_MAX` | ≥4294967295 | 2³²-1 | Maximum `unsigned long int` value |
    | `LLONG_MIN`* | ≤-9223372036854775807 | -(2⁶³-1) | Minimum `long long int` value |
    | `LLONG_MAX`* | ≥+9223372036854775807 | 2⁶³-1 | Maximum `long long int` value |
    | `ULLONG_MAX`* | ≥18446744073709551615 | 2⁶⁴-1 | Maximum `unsigned long long int` value |
    | * C99 Only | |
### The `<math.h>` Header (C89): Mathematics
- Functions in the C89 version of `<math.h>` fall into five groups:
  - Trigonometric functions
  - Hyperbolic functions
  - Exponential and logarithmic functions
  - Power functions
  - Nearest integer, absolute value, and remainder functions
#### Errors
- Functions in `<math.h>` detect two kinds of errors:
  - **Domain error**: An argument is outside a function's domain.
    - If a domain error occurs, the return value is `EDOM` is stored in `errno`.
  - **Range error**: Return value of a function is outside the range of `double` values.
    - If the return value's magnitude is too large (overflow), the function returns positive or negative `HUGE_VAL`, depending on sign to correct result.
    - `ERANGE` is stored in `errno`.
#### Trigonometric Functions
- ``` C
  double acos(double x);
  double asin(double x);
  double atan(double x);
  double atan2(double y, double x);
  double cos(double x);
  double sin(double x);
  double tan(double x);
  ```
- `cos`, `sin`, `tan` : compute the cosine, sine, and tangent. In radians not degrees.
- `acos`, `asin`, `tan`: compute the arc cosine, arc sine, and arc tangent.
- `atan2`: computes the arc tangent of `y/x`.
#### Hyperbolic Functions
- ``` c
  double cosh(double x);
  double sinh(double x);
  double tanh(double x);
  ```
- `cosh`, `sinh`, `tanh`: compute the hyperbolic cosine, sine, and tangent.
#### Exponential and Logarithmic Functions
- ``` C
  double exp(double x);
  double frexp(double value, int *exp);
  double ldexp(double x, int exp);
  double log(double x);
  double log10(double x);
  double modf(double value, double *iptr);
  ```
- `exp`: Returns *e* raised to a power.
- `log`: Inverse of `exp`, returns the logarithm of a number to the base *e*.
- `log10`: Compute the "common" (base 10) logarithm.
- `modf`: Splits the first argument into integer and fractional parts. It returns the fractional part and store the integer part in the object pointed to by the second argument.
- `frexp`: Splits a floating-point number into a fractional part *f* and an exponent *n* in such a way that the original number equals *f*×2ⁿ.
- `ldexp`: Undoes `frexp` by combining a fraction and an exponent into a single number.
#### Power Functions
- ``` C
  double pow(double x, double y);
  double sqrt(double x);
  ```
- `pow`: Raises its first argument to the power specified by the second argument.
- `sqrt`: Computes the square root.
#### Nearest Inteer, Absolute Value, and Remainder Functions
- ``` c
  double celi(double x);
  double fabs(double x);
  double floor(double x);
  double fmod(double x, double y);
  ```
- `celi`: Returns the smalles integer that is great than or equal to its argument.
- `floor`: Returns the largest integer thats less than or equal to its argument.
- `fabs`: Computes the absolute value of a number.
- `fmod`: Returns the remainder when its first argument is divided by its second argument.
### The `<math.h>` Header (C99): Mathematics
- Changes from C89 to C99 in `<math.h>`:
  - **Provide better support for the IEEE floating-point standard**
  - **Provide more control over floating-point arithmetic**
  - **Make C more attractive to Fortran programmers**
### The `<ctype.h>` Header: Character Handling
- Provides two kinds of functions:
  - character-classification functions
  - character case-mapping functions
#### Character-Classification Functions
- | **Function** | **Test** |
  | :-: | :-: |
  | `isalnum(c)` | Is `c` alphanumeric? |
  | `isalpha(c)` | Is `c` alphabetic? |
  | `isblank(c)` | Is `c` a blank? |
  | `iscntrl(c)` | Is `c` a control character? |
  | `isdigit(c)` | Is `c` a decimal digit |
  | `isgraph(c)` | Is `c` a printing character (other than a space)? |
  | `islower(c)` | Is `c` lower-case letter? |
  | `isprint(c)` | Is `c` a printing character (including a space)? |
  | `ispunct(c)` | Is `c` punctuation? |
  | `isspace(c)` | Is `c` a white-space character? |
  | `isupper(c)` | Is `c` an upper-case letter? |
  | `isxdigit(c)` | Is `c` a hexadecimal digit? |
### The `<string.h>` Header: String Handling
- `<string.h>` provides five kinds of functions:
  - **Copying functions**
  - **Concatenation functions**
  - **Comparison functions**
  - **Search functions**
  - **Miscellaneous functions**