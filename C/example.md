# C Examples
- Reference: [Tutorials Point](https://www.tutorialspoint.com/cprogramming/index.htm)

[TOC]

## Getting Started
### Include Header Files
- `stdio.h`
  - Provides input and output functions like printf and scanf.
  - `#include <stdio.h>
- `stdlib.h`
  - Contains functions involving memory allocation, rand function, and other utility functions.
  - `#include <stdlib.h>`
- `math.h`
  - Includes mathematical functions like **sqrt**, **sin**, **cos**, etc.
  - `#include <math.h>`
- `string.h`
  - Includes functions for manipulating strings, such as **strcpy**, **strlen**, etc.
  - `#include <string.h>`
- `ctype.h`
  - Functions for testing and mapping characters, like **isalpha**, **isdigit**, etc.
  - `#include <ctype.h>`
- `stdbool.h`
  - Defines the boolean data type and values **true** and **false**.
  - `#include <stdbool.h>`
- `time.h`
  - Contains functions for working with date and time.
  - `#include <time.h>`
- `limits.h`
  - Defines various implementation-specific limits on integer types.
  - `#include <limits.h>
### Macros and Constants
- Example:
  - ```c
    #include <stdio.h>
    #define PI 3.14159

    int main() {
        float radius = 5.0;
        float area = PI * radius * radius;

        printf("Area of the circle: %f\n", area);

        return 0;
    }
### Global Declarations
- Example:
  - ```c
    #include <stdio.h>

    int globalVariable;

    int main() {
        // code

        return 0;
    }
### Functions
- Example:
  - ```c
    #include <stdio.h>

    // declaration
    void samplefunction();

    int main() {
        // code

        return 0;
    }

    // definition
    void samplefunction () {
        // code
    }
## Program Structure
### Preprocessor Section
- Their are several library files, having "`.h`" extension.  A "`.h`" file (called a "header file") consists of one or more predefined function (also called "library functions") to be used in the program.
- The "**#include**" statement is used to include a header file.  It is a "**preprocessor directive**".
- Their are other preprocessor directives such as **#define** (used to define constants and macros) and **#ifdef** for conditional definitions.
- Any statement begining with "**#**" does not end in a semicolon (`;`) and his handled by the preprocessor.
- Example:
  - ```c
    #include <stdio.h>
    #define PI 3.14159
    #define AREA(r) (PI*r*r)

    int main() {
        int radius = 5;
        float area = AREA(radius);

        return 0;
    }
### The `main()` Function
- There are two types of functions: **library functions** and **user-defined functions**.
- There must be at least one user-defined function whose name must be **`main()`**.
## Compiling a C Program
- ```
                      ╭────────────╮       ╭────────────────╮
                      │  Compiler  │       │  Machine Code  │
               ╭────➤│    for     ├─────➤│ for Computer A │
               │      │ Computer A │       │                │
               │      ╰────────────╯       ╰────────────────╯
               │
  ╭────────╮   │
  │   C    │   │
  │ Source ├───┤
  │  Code  │   │
  ╰────────╯   │
               │
               │      ╭────────────╮       ╭────────────────╮
               │      │  Compiler  │       │  Machine Code  │
               ╰────➤│    for     ├─────➤│ for Computer A │
                      │ Computer A │       │                │
                      ╰────────────╯       ╰────────────────╯
- The process of converting the source code from a high-level language to its equivalent **machine code** is called **compilation**.
### C Compilation Process Steps
- Four steps:
  1) Preprocessing
    - Removes all the comments in the source files.
    - Includes the code of the header files.
    - Replaces all of the macros by their values.
  2) Compiling
    - Compiler generates the IR (Intermediate Representation) code from the preprocessed file.
    - This will produce a "**`.s`**" file. (Symbols File)
  3) Assembling
    - The assembler takes the IR code and transforms it into object code.
    - This will produce a file ending in "**`.o`**". (Object File)
  4) Linking
    - The linker create the final executable.
    - It links object codes of all of the source files together.
    - The linker knows where to look for the function definitions in the **static libraries** or the **dynamic libraries**.
- ```
                        ╭─────────────╮
                        │ Source Code │
                        │  (.c) file  │
                        ╰──────┬──────╯
                               │
                      ╭────────┼────────╮
                      │        ↓        │
                      │╭───────────────╮│
                      ││ Pre-Processor ││
                      │╰───────┬───────╯│
                      │        │        │
                      │        ↓        │
                      │╭───────────────╮│
                      ││    Compiler   ││
                      │╰───────┬───────╯│
                      │        ╰────────┼───────────╮
                      │                 │           ↓
                      │ THE COMPILATION │   ╭───────────────╮
                      │     PROCESS     │   │ Assembly Code │
                      │                 │   ╰───────┬───────╯
                      │        ╭────────┼───────────╯
                      │        ↓        │
                      │╭───────────────╮│
                      ││   Assembler   ││
                      │╰───────┬───────╯│
                      │        ╰────────┼─────────────╮
                      │                 │             ↓
  ╭───────────╮       │                 │   ╭────────────────────╮
  │ Libraries ├───────┼───────╮         │   │  Object File (.o)  │
  ╰───────────╯       │       │         │   ╰─────────┬──────────╯
                      │       │  ╭──────┼─────────────╯
                      │       ↓  ↓      │
                      │╭───────────────╮│
                      ││    Linker     ││
                      │╰───────┬───────╯│
                      ╰────────┼────────╯
                               ↓
                      ╭─────────────────╮
                      │ Executable File │
                      ╰─────────────────╯
## Tokens
- A **token** is the smallest unit in the source code of a computer language.
- The first stage in the compilation process is a tokenizer.
## Keywords
- | | | | |
  | :---: | :---: | :---: | :---: |
  | `auto` | `double` | `int` | `struct` |
  | `break` | `else` | `long` | `switch` |
  | `case` | `enum` | `register` | `typedef`|
  | `char` | `extern` | `return` | `union` |
  | `continue` | `for` | `signed` | `void` |
  | `do` | `if` | `static` | `while` |
  | `default` | `goto` | `sizeof` | `volatile` |
  | `conts` | `float` | `short` | `unsigned` |
- C99 Keywords Added:
  - `_Bool`
  - `_Complex`
  - `_Imaginary`
  - `inline`
- C11 Keywords Added:
  - `_Alignas`
  - `_Alignof`
  - `_Atomic`
  - `_Generic`
  - `_Noreturn`
  - `_Static_assert`
- C23 Keywords Added:
  - `alignas`
  - `alignof`
  - `bool`
  - `constexpr`
  - `false`
  - `nullptr`
  - `static_assert`
  - `thread_local`
  - `true`
  - `typeof`
  - `typeof_unqual`
  - `_Decimal128`
## C Identifiers
- Identifiers are the user-defined names given to make it easy to refer to the memory.
- Also used to define various elements in the program, such as the function, user-defined type, labels, etc.
- When a variable or function is defined with an identifier the compiler allocates it the memory and associates the memory location to the **identifier**.
### Naming Rules
- Keywords cannot be used.
- Only alphabets (upper and lowercase), digits, and underscores are allowed in the identifier name.
- Must start with alphabet or underscore as first character in name (not numbers).
- Same name cannot be used for two different entities.
## User Input
- The standard library `stdio.h` has the function **`scanf()`** is most commonly used to **accept user input from the standard input stream**.
- `int scanf(const char *format, &var, &var2, ...);`
- | Format Specifier | Type |
  | :---: | :---: |
  | `%c` | Character |
  | `%d` | Signed integer |
  | `%f` | Float values |
  | `%i` | Unsigned integer |
  | `%l` or `%ld` or `%li` | Long |
  | `%lf` | Double |
  | `%Lf` | Long double |
  | `%lu` | Unsigned  int or unsigned  long |
  | `%lli` or `%lld` | Long long |
  | `%llu` | Unsigned long long |
- Example:
  - ```c
    #include <stdio.h>

    int main() {
      int price, qty, ttl;

      printf("Enter price and quantity: ");
      scanf("%d %d", &price, &qty);

      ttl = price*qty;

      printf("Total: %d", ttl);

      return 0;
    }
## Basic Syntax
- Example:
  - ```c
    #include <stdio.h>                   // Header File
    int a=10;                            // Global Declarations

    int main() {                         // Main Function
      char message[] = "Hello World";    // Local Variable
      printf("%s", message);

      return 0;
    }
## Data Types
- Classified as follows:
  1) **Basic Types**
    - They are arithmetic types and are further classified into:
      a) integer types
      b) floating-point types
  2) **Enumerated Types**
    - Arithmetic types and they are used to define variables that can only assign certain discrete integer variables that can only assign certain discrete integer value throughout the program.
  3) **Void Type**
    - The type specifier `void` indicates that no value is available.
  4) **Derived Types**
    - They include:
      a) Pointer types
      b) Array types
      c) Structure types
      d) Union types
      e) Function types
### Integer Data Types
- | Type | Storage Size | Value Range |
  | :--: |:--: |:--: |
  | `char` | 1 byte | `-128` to `127` or `0` to `255` |
  | `unsigned char` | 1 byte | `0` to `255` |
  | `signed char` | 1 byte | `-128` to `127` |
  | `int` | 2 or 4 bytes | `-32,768` to `32,767` or `-2,147,483,648` to `2,147,483,647` |
  | `unsigned int` | 2 or 4 bytes | `0` to `65,535` or `0` to `4,294,967,295` |
  | `short` | 2 bytes | `-32,768` to `32,767` |
  | `unsigned short` | 2 bytes | `0` to `65,535` |
  | `long` | 8 bytes | `-9,223,372,036,854,775,808` to `9,223,372,036,854,775,807` |
  | `unsigned long` | 8 bytes | `0` to `18,446,744,073,709,551,615` |
### Floating-Point Data Types
- | Type | Storage Size | Value Range | Precision |
  | :---: | :---: | :---: | :---: |
  | `float` | 4 byte | `1.2E-38` to `3.4E+38` | 6 decimal places |
  | `double` | 8 byte | `2.3E-308` to `1.7E+308` | 15 decimal places |
  | `long double` | 10 byte | `3.4E-4932` to `1.1E+4932` | 19 decimal places |
### Void Data Type
- Specifies taht no value is available.
- Is used in three kinds of situations:
  1) **Function returns as void**
    - There are varius functions that do not return any value.
    - You can say the return **void**.
    - A function  with no return value has the return type as `void`:
      - `void exit (int status);`
  2) **Function arguments as void**
    - There are various functions which do not accept any parameter.
    - A function with no parameter can accept a `void`.
      - `int rand(void);`
  3) **Pointers to void**
    - A pointer of type `void *` represents the address of an object, but not its type.
    - A memory allocation function `void *malloc( size_t size );` returns a pointer to void which can be casted to any data type.
### Array Data Types
- An array is a collection of multiple values of the same data type stored in *consecutive memory* locations.  `int marks[5];`
### Pointer Data Types
- A pointer is a special variable that stores address or reference of aanother variable/object in t he memory.
- The name of a pointer variable is prefixed by asterisk (`*`).
- The type of the pointer variable and the variable/object to be pointed must be the same.
- ```c
  int x;
  int *y;
  y = &x;
- Here `y` is a pointer variable that stores the address of variable `x` which is of `int` type. 
## Variables
- Variable name must start with alphabet (upper or lowercase) or an underscore.
- It may consist of alphabets (upper or lowercase), digits, and underscore characters.
- No other characters can be a part of the name of a variable.
- Variable names are case-sensitive.
- There are as many number of types of variables as the number of data types:
  - **char**
    - Typically a single octet (one byte) . It is an integer type.
  - **int**
    - The most natural size of integer for the machine.
  - **float**
    - A single-precision floating point value.
  - **double**
    - A double-precision floating point value.
  - **void**
    - Represents the absence of type.
### Integer Promotions
- In general principle, integer types smaller than `int` are promoted when an operation is performed on them.
- If all values of the original type can be represented as an `int`, the value of the smaller type is converted to an `int`.
- Otherwise, it is converted to an `unsigned int`.
#### Rules
- **Byte and short values:** They are promoted to int.
- **If one operand is a long:** The entire expression is promoted to long.
- **If one operand is a float:** The eintire expression is promoted to float.
- **If any of the operands is double:** The result is promoted to double.
### Scope Rules
- There are three places where variables can be declared:
  1) Inside a function or a block which is  called **local** variables.
  2) Outside of all functions which is called **global** variables.
  3) In the definitionof function parameters which are called **formal** parameters.
- When a local variable is defined, it is not initialized by the system, you must initialize it yourself.
- Global variables are initialized automatically by the  system when you define them as follows:
  - | Data Type | Initial Default Value |
    | :---: | :---: |
    | `int` | `0` |
    | `char` | `'\0'` |
    | `float` | `0` |
    | `double` | `0` |
    | `pointer` | `NULL` |
### Static Variables
- By default, a C variable is classified as an **auto storage type**.
- A static variable is useful when you want to preserve a certain value between calls to different functions.
- Statuc variables are also used to store data that should be shared between multiple functions.
- The **static variable** belong to the static storage class, they are initialized only once and preserve the values till the end of the program.
- Feature of static variables:
  - The compiler allocates space to a static variable in the computer's main memroy.
  - Unlike `auto`, a `static` variable is initialized to zero and not garbage.
  - A static variable is not re-initialized on every function call, if it is declared inside a function.
  - A static variable has local scope.
- Syntax:
  - `static datatype var = value;`
### Accessing Global Variables With `extern` Keyword
- If you want to access a global variable when a local variable with the same name is also there in the program, then you should use the `extern` keyword.
- Example:
  - ```c
    #include <stdio.h>

    int x = 50;

    int main() {

      int x = 10;{
        extern int x;
        printf("Value of global x is %d\n", x);
      }

      printf("Value of local x is %d\n", x);

      return 0;
    }
### Type Casting
- Type casting refers to converting one datatype into another.
- Also known as "type conversion".
- There are certain instances when the compiler does the conversion on its own (*implicit type conversion*).
- You can use the typecasting operator to explicity convert the values from one type to another:
  - `(type_name) expression`
### Booleans
- ANSI C standard doesn't have a built-in or primary Boolean type.
- A Boolean or **bool** generally refers to the data type that can hold one of two binary values: *true* or *false*.
- Even if the bool type is not available, you can implement the behaviour with the help of an **enum** type.
- Exam:
  - ```c
    #include <stdio.h>

    int main() {

      enum bool {false, true};
      enum bool x = true;
      enum bool y = false;

      printf("%d\n", x);    // 1
      printf("%d\n", y);    // 0

    }
- To make it more concise, you can use the **typedef** keyword to call enum bool by the name `BOOL`.
  - ```c
    #include <stdio.h>

    int main() {
      typedef enum {false, true} BOOL;

      BOOL x = true;
      BOOL y = false;

      printf("%d\n", x);
      printf("%d\n", y);

    }
- The `#define` perprocessor directive is used to define constants.
- We can use this to define the Boolean constans, `FALSE` as 0 and `TRUE` as 1.
  - ```c
    #include <stdio.h>

    #define FALSE 0
    #define TRUE  1

    int main() {

      printf("False: %d\n True: %d\n", FALSE, TRUE);

      return 0;
    }
- C99 introduced the `stdbool.h` header file.
- It contains the definition of `bool` type, which actually is a `typedef` alias for `_bool` type.
- It also defines the macros `true` which expands to 1, and `false` which expands to 0.
  - ```c
    #include <stdio.h>
    #include <stdbool.h>

    int main() {

      bool a = true;
      bool b = false;

      printf("True:  %d\n", a);
      printf("False: %d\n", b);

      return 0;
    }
### Constants
- A user-assigned name to a location in memory, whose value cannot be modified once declared.
- Can be declared in either of the two following ways:
  - Using the `const` keyword.
  - Using the `#define` directive.
- `const type NAME = val;`
- The compiler assigns a random garbage value at the time of declaration, which you cannot change afterwords.
- Hence, you must declare and initialize the constant at once.
## Escape Sequences
- | Escape Sequence | Meaning |
  | :---: | :---: |
  |`\\` | `\` character |
  | `\'` | `'` character |
  | `\"` | `"`" character |
  | `\?` | `?` character |
  | `\a` | Alert or bell |
  | `\b` | Backspace |
  | `\f` | Form feed |
  | `\n` | Newline |
  | `\r` | Carriage return |
  | `\t` | Horizontal tab |
  | `\v` | Vertical tab |
  | `\ooo` | Octal number of one to three digits |
  | `xhh...` | Hexadecimal number of one or more digits |
## Format Specifiers
- | Format Specifier | Type |
  | :---: | :---: |
  | `%c` | Character |
  | `%d` | Signed integer |
  | `%e` or `%E` | Scientific notation of floats |
  | `%f` | Float values |
  | `%g` or `%G` | Similar as `%e` or `%E` |
  | `%hi` | Signed integer (short) |
  | `%hu` | Unsigned integer (short) |
  | `%i` | Unsigned integer |
  | `%l` or `%ld` or `%li` | Long |
  | `%lf` | Double |
  | `%Lf` | Long double |
  | `%lu` | Unsigned int or unsigned long |
  | `%lli` or `%lld` | Long long |
  | `%llu` | Unsigned long long |
  | `%o` | Octal representation |
  | `%p` | Pointer |
  | `%s` | String |
  | `%u` | Unsigned int |
  | `%x` or `%X` | Hexadecimal representation |

  - A minus symbol (`-`) tells left alignment.
  - A number after `%` specifies the minimum field width.
  - If a string is less than the width, it will be filled with spaces.
  - A period (`.`) is used to separate field width and precision.
## Operators
- **Unary operators:** `++` (increment), `--` (decrement), `!` (NOT), `~` (compliment), `&` (address of), `*` (dereference)
- **Binary operators:** arithmetic, logical and relational operators except `!`.
- **Ternary operators:** The `?` operator.
### Arithmetic Operators
- | Opearator | Description | Example |
  | :---: | :---: | :---: |
  | `+` | Adds two operands | `A + B` |
  | `-` | Subracts second operand from the first | `A - B` |
  | `*` | Multiplies both operands | `A * B` |
  | `/` | Divides numerator by denumerator | `B / A` |
  | `%` | Modulus operator and remainder of after an integer division | `B % A` |
  | `++` | Increment operator increases the integer value by one | `A++` |
  | `--` | Decrement operator decreases the integer value by one | `A--` |
### Relational Operators
- | Operator | Description | Example |
  | :---: | :---: | :---: |
  | `==` | Checks if the vaules of two operands are equal or not<br>If yes, then the condition becomes true | `A == B` |
  | `!=` | Checks if the values of two operands are equal or not<br>If the values are not equal, then the condition becomes true | `A != B` |
  | `>` | Checks if the value of left operand is greater than the value of right operand<br>If yes, then the condition becomes true | `A > B` |
  | `<` | Checks if the value of left operand is less than the value of right operand<br>If yes, then the condition becomes true | `A < B` |
  | `>=` | Checks if the value of left operand is greater than or equal to the value of right operand<br>If yes, then the condition becomes true | `A >= B` |
  | `<=` | Checks if the value of left operand is less than or equal to the value of right operand<br>If yes, then the condition becomes true | `A <= B` |
### Logical Operators
- | Operator | Description | Example |
  | :---: | :---: | :---: |
  | `&&` | Called *Logical AND* operator<br>If both the operands are non-zero, then the condition becomes true | `A && B` |
  | `\|\|` | Called *Logical OR* operator<br>If any of the two operands is non-zero, then the condition becomes true | `A \|\| B` |
  | `!` | Called *Logical NOT* operator<br>It is used to reverse the logical state of its operand<br>If a condition is true, then *Logical NOT* operator will make it false | `!(A && B)` |
### Bitwise Operators
- | Operator | Description | Example |
  | :---: | :---: | :---: |
  | `&` | *Binary AND* operator copies a bit to the result if it exists in both operands | `A & B` |
  | `\|` | *Binary OR* operator copies a bit if it exists in either operand | `A \| B` |
  | `^` | *Binary XOR* operator copies the bit if it is set in one operand but not both | `A ^ B` |
  | `~` | *Binary One's Complement* operator is unary and has the effect of '*flipping*' bits | `~A` |
  | `<<` | *Binary Left Shift* operator<br>The left operands value is moved left by the number of bits specified by the right operand | `A << B` |
  | `>>` | *Binary Right Shift* operator<br>The left operands value is moved right by the number of bits specified by the right operand | `A >> B` |
- | p | q | p & q | p \| q | p ^ q |
  | :---: | :---: | :---: | :---: | :---: |
  | 0 | 0 | 0 | 0 | 0 |
  | 0 | 1 | 0 | 1 | 1 |
  | 1 | 1 | 1 | 1 | 0 |
  | 1 | 0 | 0 | 1 | 1 |
- ```
  ╭─────────────────╮
  │  A  = 0011 1100 │
  │  B  = 0000 1101 │
  ├─────────────────┤
  │ A&B = 0000 1100 │
  │ A|B = 0011 1101 │
  │ A^B = 0011 0001 │
  │  ~A = 1100 0011 │
  ╰─────────────────╯
### Assignment Operators
- | Operator | Description | Example |
  | :---: | :---: | :---: |
  | `=` | Simple assignment operator<br>Assigns values from right side operands to left side operand | `C = A + B` |
  | `+=` | Add and assignment operator<br>It adds the right operand to the left operand and assign the result to the left operand | `C += A` equivalent `C = C + A` |
  | `-=` | Subtract and assignment operator<br>It subracts the right operand from theleft oeprnad and assigns the result to the left operand | `C -= A` equivalent `C = C - A` |
  | `*=` | Multiply and assignment operator<br>It multiplies the right operand with the left operand and assigns the result to the left operand | `C *= A` equivalent `C = C * A` |
  | `/=` | Divide and assignment operator<br>It divides the left operand with the right operand and assigns the result to the left operand | `C /= A` equivalent `C = C / A` |
  | `%=` | Modulus and assignment operator<br>It takes modulus using two operands and assigns the result to the left operand | `C %= A` equivalent `C = C % A` |
  | `<<=` | Left shift and assignment operator | `C <<= A` equivalent `C = C % A` |
  | `>>=` | Right shift and assignment operator | `C >>- A` equivalent `C = C >> A` |
  | `&=` | Bitwise *AND* and assignment operator | `C &= A` equivalent `C = C & A` |
  | `^=` | Bitwise *Exclusive OR* and assignment operator | `C ^= A` equivalent `C = C ^ A` |
  | `\|=` | Bitwise *Inclusive OR* and assignment operator | `C \|= A` equivalent `C = C \| A` |
  ### Misc Operators
  - | Operator | Description | Example |
    | :---: | :---: | :---: |
    | `sizeof()` | Returns the size of a variable | `sizeof(var)` |
    | `&` | Returns the address of a variable | `&var` |
    | `*` | Pointer to a variable | `*var` |
    | `?:` | Conditional expression | if condition is true `?` then value `X :` otherwise value `y` |
    | `.` | Member access operator | `var.member` |
    | `->` | Access members of a struct variable with pointer | `ptr -> member;` |
- `exp1 ? exp2 : exp3`
  - **exp1** A boolean expression evaluating to true or false.
  - **exp2** Returned by the `?` operator when exp1 is true.
  - **exp3** Returned by the `?` operator when exp1 is false.
### The `sizeof` Operator
- The `&` operator return the address of an existing variable:
  - ```c
    #include <stdio.h>

    int main() {
      int var = 100;

      printf("var: %d address: %d", var, &var);

      return 0;
    }
### The Dereference Operator
- To declare a pointer variable, the following syntax is used: 
  - `type *var;`
- The variable must be prefixed with an asterisk (`*`).
- The data type indicates it can store the address of that data type.
  - `int *x;`
- The `&` operator returns the address of an existing variable.
  - ```c
    int a;
    int *x = &a;
- The `*` operator is called the Dereference operator.
- It returns the value stored in the address which is stored in the pointer, i.e., the value of the variable it is pointing to.
  - ```c
  #include <stdio.h>

  int main() {
    float var1 = 10.55;
    float *floatptr = &var1;

    printf("var1: %f address of var1: %d\n", var1, &var1);
    printf("floatptr: %d address of floatptr: %d\n", floatptr, &floatptr);
    printf("var1: %f value at floatptr: %f\n", var1, *floatptr);
  }
## Decision Making
- The following decision making statements are available:
  - **if statement**
    - An **if** statement consists of a boolean expression followed by one or more statements.
  - **if...else statement**
    - An **if** statement can be followed by and optional **else** statement, which executes when the Boolean expression is false.
  - **nested if statements**
    - You can use one **if** or **else-if** statement inside another **if** or **else-if** statements.
  - **switch statements**
    - A **switch** statement allows a variable to be tested for equality against a list of values.
  - **nested switch statements**
    - You can use one **switch** statement inside another **switch** statements.
### The Break Statement
- When used  inside a loop, it causes the repetition to be abandoned.
- ```
                ╭─╮
                ╰┬╯
                 │
                 ↓
          ╭─────────────╮
  ╭──────→│ Conditional ├────────╮
  │       │    Code     │        │
  │       ╰──────┬──────╯        │
  │ if condition │               │
  │ is ture      ↓               │
  │        ╭───────────╮     ╭───┴───╮
  ╰────────┤ Condition │     │ Break │
           ╰─────┬─────╯     ╰───┬───╯
                 │ if condition  │
                 │ is false      │
                ╭┴╮←─────────────╯
                ╰─╯   
### The Continue Statement
- Causes the conditional test and increment portions of the loop to execute.
- ```
                ╭─╮
                ╰┬╯
                 │
                 ↓
          ╭─────────────╮
  ╭──────→│ Conditional ├────────╮
  │       │    Code     │        │
  │       ╰──────┬──────╯        │
  │ if condition │               │
  │ is ture      ↓               │
  │        ╭───────────╮   ╭─────┴────╮
  ╰────────┤ Condition │←──┤ Continue │
           ╰─────┬─────╯   ╰──────────╯
                 │ if condition  
                 │ is false      
                ╭┴╮
                ╰─╯   
## Loops
- Looping Statements
  - **while loop**
    - Repeats a statement or ghroup of statements while a given condition is true.
    - It tests the condition before executing the loop body.
  - **for loop**
    - Executes a sequence of statements multiple times and abbreviates the code that manages the loop variable.
  - **do-while loop**
    - It is more like a while statement, excpet that it tests the condition at the end of the loop body.
  - **nested loops**
    - Can use one or more loops inside any other **while**, **for** or **do-while** loop.
- Control Statements
  - **break statement**
    - Terminates the **loop** or **switch** statement and trnasfers execution to the statement immediately following the loop or switch.
  - **continue statement**
    - Causes the loop to skip the remainder of its body and immediately retest it condition prior to reiterating.
  - **goto statement**
    - Transfers the control to the labeled statement.
### While Loop
- Syntax:
  - ```c
    while(expression) {
      statement(s);
    }
### For Loop
- Syntax:
  - ```c
    for (init; condition; increment) {
      statements(s);
    }
### Do... While Loop
- Syntax:
  - ```c
    do {
      statements(s);
    } while(condition);
### Infinite Loop
- Example:
  - ```c
    #include <stdio.h>

    int main() {
      while (1) {
        printf("Hello World");
      }
      
      return 0;
    }
  - ```c
    #include <stdio.h>

    int main() {

      int i;

      for ( ; ; ) {
        i++;
        printf("Hello World\n");
      }
    }
  - ```c
    #include <stdio.h>

    int main() {
      do {
        printf("Hello World\n");
      } while(1);

      return 0;
    }
## Functions
- A function declaration has the following parts:
  - **Return Type**
    - A function may return a value.
    - The `return_type` is the data type of the value the function returns.
    - Some functions perform the desired operations without returning a value.
  - **Function Name**
    - This is the actual name of the function.
    - The *function_name* and the parameter list together constitute the *function signature*.
  - **Argument List**
    - An argument (also called a parameter) is like a placeholder.
    - When a function is invoked, you pass a value as a parameter.
    - This value referred to as the actual parameter or argument.
    - The `parameter list` refers to the type, order, and number of the parameters of a function.
    - Parameters are optional; that is, a function may contain no parameters.
  - **Function Body**
    - The `body of the function` contains a collection of statements that defines what the function does.
  - ```c
    return_type function_name(parameter list) {
      body  of the function
    }
- There are two ways in which arguments can be passed to a function:
  1) **Call by value**
    - This method copies the actual value of an argument into the formal parameter of the function.
    - In this case, changes made  to the parameter  inside the function have no effect on the argument.
  2) **Call by reference**
    - This method copies the address of an argument into the formal parameter.
    - Inside the function, the address is used to access the actual argument used in the call.
    - This means that changes made to the parameter affect the argument.
### Main Function
- Valid signatures of `main()` function:
  - ```c
    int main() {
      ...
      return 0;
    }
  - ```c
    int main(void) {
      ...
      return 0;
    }
  - ```c
    int main(int argc, char *argv[]) {
      ...
      return 0;
    }
- Requirements of `main()` function:
  - A C program must have a `main()` function.
  - The main is **not** a C keyword.
  - It is classified as a user-defifned function because its body is not pre-decided, it depends on the processing logic of the program.
  - By convention, `int` is the return type of `main()`.
  - The last statement in the function body of `main()` return 0, to indicate that the function has been successfully executed.
  - Any non-zero return value indicates failure.
  - Some old C compilers let you define `main()` function with `void` return type.
  - However, this is considered to be non-standard and is not recommended.
  - As compared to other functions, the `main()` function:
    - Can't be declared as inline.
    - Can't be declared as static.
    - Can't have its address taken.
    - Can't be called from your program.
- The following is the syntax of `main()` function with command-line arguments:
  - ```c
    int main(int argc, char *argv[]) {
      ...
      return 0;
    }
  - The argument definitions are as follows:
    - **`argc`**
      - The first argument is an integer that contains the count of arguments that follow in `argv`.
      - The `argc` parameter is always greater than  or equal to 1.
    - **`argv`**
      - The second argument is an array of null-terminated strings representing command-line arguments entered by the user of the program.
      - By convention, `argv[0]` is the command with which the program is invoked.
      - `argv[1]` is the first command-line argument.
      - The last argument from the command line is `argv[argc - 1]`, and `argv[argc]` is always `NULL`.
### Function Call by Value
- `type function_name(type var1, type var2, ...)`
### Function Call by Reference
- ```c
  #include <stdio.h>

  int swap(int *x, int *y) {

    int z;

    z = *x;   /* save the value at address x */
    *x = *y;  /* put y int x */
    *y = z;   /* put z into y */

    retunr 0;
  }

  int main() {

    int a = 10;
    int b = 20;

    printf("Before swap, value of a: %d\n", a);
    printf("Before swap, value of b: %d\n", b);

    swap(&a, &b);

    printf("After swap, value of a: %d\n", a);
    printf("After swap, value of b: %d\n", b);

    return 0;
  }
### Variadic Function
- A  function that can take a variable number of arguments.
- Syntax:
  - `return_type function_name(type arg1, ...);`
- Will need to include the `stdarg.h` header file to handle varadic functions.
- The library provides the following methods:
  - | Methods | Description |
    | :---: | :---: |
    | `va_start(va_list ap, arg)` | Arguments after the last fixed argument are stored in the `va_list` |
    | `va_arg(va_list ap, type)` | Each time, the next argument in the variable list `va_list` and coverts it to the given type, till it reaches the end of the list. |
    | `va_copy(va_list dest, va_list src)` | This creates a copy of the arguments in `va_list` |
    | `va_end(va_list ap)` | This ends the traversal of the variadic function arguments.<br>As the end of `va_list` is reached, the list object is cleaned up. |
- Example:
  - ```c
    #include <stdio.h>
    #include <stdarg.h>

    int addition(int n, ...) {

      va_list args;                   /* initialize args as va_list type */
      int i, sum = 0;

      va_start(args, n);              /* get the argument list */

      for (i = 0; i < n; i++) {
        sum += va_arg(args, int);     /* get next item in list, convert to int, and add to sum */
      }

      va_end(args);                   /* cleanup the args variable */

      return sum;
    }

    int main() {
      printf("Sum = %d ", addition(5, 1, 2, 3, 4, 5));

      return 0;
    }
### Callback Functions
- Any executable code that is passed as an argument to other code.
- Example:
  - ```c
    #include <stdio.h>

    void hello() {
      printf("Hello World\n");
    }

    void callback(void (*ptr)()) {
      printf("Calling a function with its pointer\n");

      (*ptr)();    // calling the callback function
    }

    main() {

      void (*ptr)() = hello;

      callback(ptr);
    }
### Function returning an array
- A function can return only one value.
- It is not possible to return an entire array.
- However, you can return a pointer to an array by specifying the array's name without an index.
- Example:
  - ```c
    #include <stdio.h>

    int* test(int *);

    int main () {
      int a[] = {1,2,3,4};
      int i;
      int  *b = test(a);
      for (i=0; i<4; i++) {
        printf("%d\n", b[i]);
      }

      return 0;
    }

    int * test(int*a) {
      int i;
      for (i=0; i<4; i++) {
        a[i] = 2*a[i];
      }

      return a;
    }
### Recursion
- A recursive function is a function that calls itself.
- Example:
  - ```c
    void recursive_function() {
      recursion();   // function calls itself
    }

    int main() {
      recursive_function();
    }
## Arrays
- Array elements are store in **contiguous memory** locations.
- Each element is identified by an index starting with `0`.
- The lowest address corresponds to the first element and the highest address to the last element.
- ```
  First Element                                     Last Element
        │                                                │
        │                                                │
  ╭─────┴──────┬────────────┬────────────┬────────────┬──┴──╮
  │ Numbers[0] │ Numbers[1] │ Numbers[2] │ Numbers[3] │ ... │
  ╰────────────┴────────────┴────────────┴────────────┴─────╯
- Declaration syntax:
  - `type arrayName[size];`
- Initialization syntax:
  - `data_type array_name [size] = {value1, value2, value3, ...};`
- Important array concepts:
  - **Multi-dimensional arrays**
    - The simplest form of a multidimensional array is the two-dimensional array.
  - **Passing arrays to functions**
    - You can pass to the function a pointer to an array by specifying the array's name without an index.
  - **Return array from a function**
    - Allows a function to return an array by reference.
- Properties of arrays:
  - Collection the the same data type
  - Contiguous memory allocation
  - Fixed size
  - Length depends on type
  - indexing
  - Pointer relationship
  - Lower and upper bounds
  - Multi-dimensional array
  - Implementation of complex data structures
- Jagged Array
  - ```
    ╭─────╮
    │     │            ╭─────┬─────╮
    │     ├─────────── │  1  │  2  │
    │     │            ╰─────┴─────╯
    ├─────┤
    │     │            ╭─────┬─────┬─────╮
    │     ├─────────── │  3  │  4  │  5  │
    │     │            ╰─────┴─────┴─────╯
    ├─────┤
    │     │            ╭─────┬─────┬─────┬─────╮
    │     ├─────────── │  6  │  7  │  8  │  9  │
    │     │            ╰─────┴─────┴─────┴─────╯
    ╰─────╯
- Array element length:
  - ```c
    int a[] = ....;
    int i = 0;

    for (i = 0; i < sizeof(a) / sizeof(a[i]); i++) {
      ...
    }
## Pointers
- A derived data type that is used to store the address of another variable and can be used to access and manipulate the variable's data stored in that location.
- With pointers, you can access and modify  the data located in memory.
### Pointer Declaration
- To declare a pointer, use the **dereferencing operator (`*`)** followed by the data type.
  - Syntax:
    - `type *var-name;`
  - Example declarations:
    - ```c
      int    *ip;   /* pointer to an integer */
      double *dp;   /* pointer to a double */
      float  *fp;   /* pointer to a float */
      char   *ch;   /* pointer to a character */
### Pointer Initialization
- After declaring a pointer variable, you need to initialize it with the address of another variable using the **address of (`&`) operator**.
- This process is known as **referencing a pointer**.
  - Syntax: 
    - ```c
      int x = 10;
      int *ptr = &x;
### Referencing and Dereferencing Pointers
- A pointer references a location in memory.
- Obtaining the value stored at that location is known as **dereferencing the pointer**.
  - **The `&` Operator**
    - It is also known as the "Address-of operator".
    - It is used for **Referencing** which means taking the address of an existing variable (using `&`) to set a pointer variable.
  - **The `*` Operator**
    - It is also known as the "dereference operator".
    - It is used for **Dereferencing** a pointer and is carried out using the `*` operator to get the value from the memory address that is pointed by the pointer.
### Pointer to Pointer
- We may have a pointer variable that stores the address of another pointer itself.
  - ```
        1000            2000            3000
    ╭──────────╮    ╭──────────╮    ╭──────────╮
    │    10    │<───┤   1000   │<───┤   2000   │  
    ╰──────────╯    ╰──────────╯    ╰──────────╯        
    int a = 10;     int *x = &a;    int **y = &x;
### `NULL` Pointers
- It is always a good practice  to assign a `NULL` value to a pointer variable in case you do not have an exact address to be assigned.
- Example:
  - ```c
    #include <stdio.h>

    int main() {
      int *ptr = NULL;

      printf("The value of ptr is : %x\n", ptr);

      return 0;
    }
### Pointers in Detail
- **Pointer arithmetic**
  - There are four arithmetic operators that can be used in pointers:
    - `++`
    - `--`
    - `+`
    - `-`
- **Array of pointers**
  - You can define arrays to hold a number of pointers.
- **Pointer to  pointer**
  - Allowed to have a pointer to a pointer and so on.
- **Passing pointers to functions**
  - Passing an argument by reference or by address enable the passed argument to be changed in the calling function by the called function.
- **Return pointer from functions**
  - Allows a function to return a pointer to the local variable, static variable, and dynamically allocated memory as well.
- Assuming:
  - ```c
    #include <stdio.h>

    int main() {
      int arr[5] = {1, 2, 3, 4, 5};
      int *b     = arr;

    }
  - `&arr[0]` is equivalent to `b` and `arr[0]` to `*b`
  - `&arr[1]` is equivalent to `b + 1` and `arr[1]` is equivalent to `*(b + 1)`
  - `&arr[2]` is equivalent to `b + 2` and `arr[2]` is equivalent to `*(b + 2)`
  - `&arr[i]` is equivalent to `b + i` and `arr[i]` is equivalent to `*(b + i)`
### Applications of Pointers
#### Access Array Elements
- ```c
  #include <stdio.h>

  int main() {

    int arr[] = {1,2,3,4,5};
    int *ptr  = arr;

    for(int i = 0; i <=4; i++) {
      printf("arr[%d]: %d\n", i, *ptr);
      ptr++;
    }

    return 0;
  }
#### Allocating Memory Dynamically
- Functions provided to allocate and release memory dynamically:
  - `malloc()` function
    - Allocates an array of num elements eacho of which size in bytes will be size.
  - `calloc()` function
    - Allocates an array of num bytes and leaves them uninitalized.
  - `realloc()` function
    - Reallocates memory extending it up to newsize.
- This function is defined in the `stdlib.h` header file.
- It allocates a block of memory of the size required and returns a `void` pointer.
  - `void *malloc (size)`
- The `size` parameter refers to the block of memory in bytes.
- To allocate the memory required for a specified data type, you need to use the typecasting operator:
  - ```c
    int *ptr;
    ptr = (int *) malloc (sizeof (int));
- Here we need to define a pointer without defining how much memory is required and later we can allocate memory.
- The function `calloc` (contiguous allocation) allocates the requested memory and return a pointer to it.
  - `void *calloc(n, size);`
- Where `n` is the number of elements to be allocated and `size` is t he byte size of each element.
  - ```c
    int *ptr;
    ptr = (int *) calloc(25, sizeof(int));
- The `realloc()` function is used to dynamically change the memory allocation of a previously allocated memory.
- You can increase or decrease the size of an allocated memory block by calling the `realloc()` function.
  - `void *realloc(*ptr, size);`
- The first parameter `ptr` is the pointer to a memory block previously allocated with `malloc`, `calloc` or `realloc` to be reallocated.
#### Returning Multiple Values From A Function
- ```c
  #include <stdio.h>

  void funAddSub(int a, int b, int* add, int* sub) {
    *add = a + b;
    *sub = a - b;
  }

  int main() {
    int num1 = 10;
    int num2 = 3;

    int res1, res2;

    funAddSub(num1, num2, &res1, &res2);

    printf("Addition is %d and subtraction is %d", res1, res2);

    return 0;
  }
#### Pointer Arithmetics
- The following are some of the important pointer arithmetic operations:
  - Increment and decrement of a pointer
  - Addition and subtraction of integer to pointer
  - Subtraction of pointers
  - Comparison of  pointers
##### Increment and Decrement of a Pointer
- ```c
  int x  = 10;
  int *y = &x;

  y++;
#### Comparison of Pointers
- Pointers may be compared by using relational operators such as `==`, `<`, and `>`.
- If two pointers point to variables  that are related to each other (such as elements of the same array), then they can be meaningfully compared.
#### Pointer to Pointer
- ```c
  #include <stdio.h>

  int main() {
    int a = 100;

    int *ptr = &a;

    int **dptr = &ptr;

    printf("Value of 'a' is : $d\n", a);
    printf("Value of 'a' using pointer (ptr) is : %d\n", *ptr);
    printf("Value of 'a' using double pointer (dptr) is : %d\n", **dptr);

    return 0;
  }
### `NULL` Pointer
- A `NULL` pointer is a pointer that desn't point to any memory location.
- The `NULL` constant is defined in the header files `stdio.h`, `stddef.h`, and `stdlib.h`.
- **A pointer is initialized to `NULL` to avoid the unpredicted behavior of a program or to prevent segmentation fault errors.**
#### Declare and Initialize a `NULL` Pointer
- `type *ptr = NULL;`
- Or, this syntax:
- `type *ptr = 0;`
#### Applications of `NULL` Pointer
- To initialize a pointer variable when that pointer variable isn't assigned any valid memory address yet.
- To pass a null pointer to a function argument when we don't want to pass any valid memory address.
- To check for a `NULL` pointer before accessing any pointer variable so that we can perform error handling in pointer-related code.
  - Example: Dereference a pointer variable only if it's not `NULL`.
- A `NULL` pointer is always used to detect the endpoint of trees, linked lists, and other dynamic data structures.
#### Check Whether a Pointer is `NULL`
- It is always recommended to check whether a pointer is `NULL` before dereferencing it to fetch the value of its target variable.
  - ```c
    #include <stdio.h>

    int main() {

      int *ptr = NULL;

      if (ptr == NULL) {
        printf("Pointer is a NULL pointer");
      } else {
        printf("Value stored in the address referred by the pointer: %d", *ptr);
      }

      return 0;
    }
#### Check Memory Allocation Using `NULL` Pointer
- ```c
  #include <stdio.h>
  #include <stdlib.h>

  int main() {

    int* ptr = (int*)malloc(sizeof(int));

    if (ptr == NULL) {
      printf("Memory Allocation Failed");
      exit(0);
    } else {
      printf("Memory Allocated Successfully");
    }

    return 0;
  }
#### `NULL` File Pointer
- ```c
  #include <stdio.h>
  #include <string.h>

  int main() {
    FILE *fp;
    char *s;
    int i, a;
    float p;

    fp = fopen("file3.txt", "r");

    if (fp == NULL) {
      puts("Cannot open file");
      return 0;
    }

    while (fscanf(fp, "%d %f %s", &a, &p, s) != EOF) {
      printf("Name: %s Age: %d Precent: %f\n", s, a, p);
    }
    fclose(fp);

    return 0;
  }
#### `void` Pointer
- A `void` pointer is a type of pointer that is not associated with any data type.
- A `void` pointer can hold an address of any type and can by typecasted to any type.
- They are also called *general-purpose* or *generic pointers*.
- Example:
  - ```c
    #include <stdio.h>

    int main() {

      int  a = 10;
      char b = 'x';

      void *ptr = &a;
      printf("Address of 'a': %d", &a);
      printf("\nVoid pointer points to: %d", ptr);

      ptr = &b;
      printf("\nAddress of 'b': %d", &b);
      printf("\nVoid pointer points to: %d", ptr);
    }
##### Application of `void` Pointers
- The `malloc()` function is available as a library function in the header file `stdlib.h`.
- It dynamically allocates a block of memory during the runtime of a program.
- Normal declaration of variables causes the memory to be allocated at compile time.
- `void *malloc(size_t size);
- `void` pointers are used to  implement generic functions
- The dynamic allocation functions `malloc()` and `calloc()` return `void *` type and this feature allows these functions to be used to allocate memory of any data type.
- The most common application of `void` pointers is in the implementation of data structures such as linked lists, trees, and queues, i.e. dynamic data structures.
##### Limitations of `void` Pointers
- Pointer arithmetic is not possible with void pointer due to its concrete size.
- It can't be dereferenced.
- A `void` pointer cannot work with increment or decrement operators because it doesn't have a specific type.
#### Dangling Pointers
- A **dangling pointer** is the behavior of a pointer when its target (the variable it is pointing to) has been deallocated or is no longer accessible.
- It is a pointer that doesn't point to a valid variable of the appropriate type.
- The situation of dangling pointers can occur due to the following reasons:
  - De-allocation of memory
  - Accessing an out-of-bounds memory location
  - When a variable goes out of scope
## Strings
- A string is a one-dimensional array of `char` type, with the last character in the array being a "null character" represent by `'\0'`.
  - `char greeting[6] = {'H', 'e', 'l', 'l', 'o', '\0'};`
  - ```
    Index         0         1         2         3         4         5
             ╭─────────┬─────────┬─────────┬─────────┬─────────┬─────────╮
    Variable │    H    │    e    │    l    │    l    │    o    │    \0   │
             ╰─────────┴─────────┴─────────┴─────────┴─────────┴─────────╯
             ╭─────────┬─────────┬─────────┬─────────┬─────────┬─────────╮
    Address  │ 0x23451 │ 0x23452 │ 0x23453 │ 0x23454 │ 0x23455 │ 0x23456 │
             ╰─────────┴─────────┴─────────┴─────────┴─────────┴─────────╯
- ```c
  #include <stdio.h>

  int main () {

    //char greeting[] = {'H', 'e', 'l', 'l', 'o', '\0'};
    // OR
    char greeting[] = "Hello World";

    printf("Greeting message: %s\n", greeting);

    return 0;
  }
## Special Characters
### Parentheses `( )`
- Used to group one or more operands in an expression and control the order of operations in a statement.
### Braces `{ }`
- Used to define blocks of code, such as function bodies and loops.
- Also used to initialize arrays and struct variables.
### Square Brackets `[ ]`
- Used to declare arrays and access elements of an array with the subscript index.
### Asterisk `*`
- Used (other than multiplication) to declare a pointer variable and dereference it to obtain the value of the target variable.
### Ampersand `&`
- Used as the *address-of operator*.
- It returns the address of a variable.
### Comma `,`
- Used as separator between a statement or a function call.
### Semicolon `;`
- Used to indicate the end of a statement.
### Dot `.`
- Used to access the members of a structure or a union.
### Arrow `->`
- Used to access the members of a structure or a union through a pointer.
## Structures
- We use the keyword `struct` to define a custom dat type that groups together the elements of different tyeps.
- The difference between an array and a structure is that an array is a homogenous collection of similar types, whereas a structure can have elements of different types stored adjacently and identified by a name.
### Declare (Create) a Structure
- ```c
  struct [structure tag] {
    member definition;
    member definition;
    ...
    memeber definition;
  } [one or more structure variables];
- At the end of the structure's definition, before the final semicolon, you can specify one or more structure variables but it is *optional*.
- ```c
  #include <stdio.h>
  #include <string.h>

  struct book {
    char   title[10];
    char   author[20];
    double price;
    int    pages;
  } book1;

  int main() {
    strcpy(book1.title,  "Learn C");
    strcpy(book1.author, "Dennis Ritchie");
    book1.price = 675.50;
    book1.pages = 325;

    printf("Title: %d \n",  book1.title);
    printf("Author: %s \n", book1.author);
    printf("Price: %lf \n", book1.price);
    printf("Pages: %d \n",  book1.pages);

    return 0;
  }
### Bit Fields
- Allow the packing of data in a structure.
- Examples:
  - Packing several objects into a machine word, for example, 1-bit flags can be compacted.
  - Reading external file format -- non-stnadard file formats could be read in, for example, 9-bit integers.
- Allowed to do this in a structure definition by putting `:bit` length after the variable.
  - ```c
    struct packed_struct {
      unsigned int f1:1;
      unsigned int f2:1;
      unsigned int f3:1;
      unsigned int f4:1;
      unsigned int type:4;
      unsigned int my_int:9;
    } pack;
### `struct` in Memory
- `struct` members are stored in  the orderr they are declared (Required by C99.
- If needed, padding is added between `struct` members, to ensure that the latter one used the correct alignment.
- Each primitive `type T` requires an alignment of `sizeof(T)` bytes.
- Given the following:
  - ```c
    struct ST {
      char      ch1;
      short     s;
      char      ch2;
      long long ll;
      int       i;
    };
  - `ch1` is at offset `0`
  - A padding byte is inserted to align...
  - `s` is at offset `2`
  - `ch2` is at offset `4`, immediately after `s`
  - `3` padding bytes are added to align...
  - `ll` is at offset `8`
  - `i` is at offset `16`, right after `ll`
  - `4` padding bytes are added at the end so that the overall `struct` is a multiple of `8` bytes. (32-bit systems may allow struct to have 4-byte alignment)
- So `sizeof(ST)` is `24`
- It can be reduced to `16` bytes by rearrangin the members to avoid padding:
  - ```c
    struct ST {
      long long ll;  // @ 0
      int       i;   // @ 8
      short     s;   // @ 12
      char      ch1; // @ 14
      char      ch2; // @ 15
    };
### Self-Referential Structures
- A `struct` data type where one or more of its elements are pointer to variables of its own type.
- Extensively used ot build complex and dynamic data structures such as linked lists and trees.
- Self-referential structures let you emulate the arrays by andling the size dynamically.
- ```
         ╭──────┬──────╮  ╭──────┬──────╮  ╭──────┬──────╮  
  head ─>│ data │ next │─>│ data │ next │─>│ data │ next │─> null
         ╰──────┴──────╯  ╰──────┴──────╯  ╰──────┴──────╯  
- Self-referential structures are also used to consturct non-linear data structures such as *trees*
- ```
                                               A
                           ╭────────────┬────────────┬────────────╮
                           │      B     │     100    │      C     │
                           ╰────────────┴────────────┴────────────╯
                            ╱                                ╲
                           ╱                                  ╲
                          ╱                                    ╲
                         B                                      C
              ╭──────┬──────┬──────╮                  ╭────────────┬────────────┬────────────╮
              │   D  │  75  │   E  │                  │      F     │    125     │      G     │
              ╰──────┴──────┴──────╯                  ╰────────────┴────────────┴────────────╯
               ╱                    ╲                       ╲                         ╲
              ╱                      ╲                       ╲                         ╲
             ╱                        ╲                       ╲                         ╲
            D                          E                        F                        G
  ╭──────┬──────┬──────╮    ╭──────┬──────┬──────╮    ╭──────┬──────┬──────╮    ╭──────┬──────┬──────╮
  │ NULL │  50  │ NULL │    │ NULL │  85  │ NULL │    │ NULL │  115 │ NULL │    │ NULL │ 145  │ NULL │
  ╰──────┴──────┴──────╯    ╰──────┴──────┴──────╯    ╰──────┴──────┴──────╯    ╰──────┴──────┴──────╯