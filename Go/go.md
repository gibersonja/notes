# Go Notes
- Reference: [Go by Example](https://gobyexample.com/)

[TOC]

## Maps
- ```go
  package main

  import (
    "fmt"
    "maps"
  )

  func main() {
    m := make(map[string]int)   // To create an empty map, use the builtin 'make':
                                // make(map[key-type]val-type)

    m["k1"] = 7                 // Set key/value pairs using typical 'name[key] = val' syntax.
    m["k2"] = 13

    fmt.Println("map:", m)      // Will show all of its key/value pairs

    v1 := m["k1"]               // Get value for a key with 'name[key]'
    fmt.Println("v1:", v1)

    v3 := m["k3"]               // Doesn't exist, the zero value of the value type is returned
    fmt.Println("v3:", v3)

    fmt.Println("len:", len(m)) // Returns the number of key/value pairs when len is called on a map

    delete(m, "k2")             // The builtin 'delete' removes key/value pairs from a map
    fmt.Println("map:", m)

    clear(m)                    // To remove ALL key/value pairs from a map, use the 'clear' builtin
    fmt.Println("map:", m)

    _, prs := m["k2"]                       // The optional second return value when getting a value from a map indicates if the key
                                            // was present in the map.

    n := map[string]int{"foo": 1, "bar":2}  // Can declare and initialize a new map in the same line
    fmt.Println("map:", n)

    n2 := map[string]int{"foo": 1, "bar":2} // The maps package has useful utility functions for maps
    if maps.Equal(n, n2) {
      fmt.Println("n == n2")
    }
  }

## Pointers
- ```go
  package main

  import "fmt"

  func main() {
    var a int = 10

    fmt.Printf("Value of variable 'a':     a = %d\n", a)    // Value of 'a'
    fmt.Printf("Address of variable 'a':  &a = %X\n", &a)   // Address of 'a'

    var ip *int

    ip = &a

    fmt.Printf("Address stored in 'ip':   ip = %X\n", ip)   // Address of 'a' via 'ip'
    fmt.Printf("Value of '*ip' variable: *ip = %d\n", *ip)  // Value of 'a' via '*ip'
  }

## Interfaces
- *Interfaces* are named collections of method signatures:
- ```go
  package main

  import (
    "fmt"
    "math"
  )

  type geometry interface {  // Basic interface for geometric shapes
    area() float64
    perim() float64
  }

  Type rect struct {
    width, height float64    // Implement this interface on 'rect' and 'circle' types
  }

  type circle struct {
    radius float64
  }

  func (r rect) area() float64 { // To implement an interface we need to impement all the methods in the interface
    return r.width * r.height
  }

  func (r rect) perim() float64 {
    return 2*r.wdith + 2*r.height
  }

  func (c circle) area() float64 {
    return math.Pi * c.radius * c.radius
  }

  func (c circle) perim() float64 {
    return 2 * math.Pi * c.radius
  }

  func measure(g geometry) {  // If a variable has an interface type, then we call methods that are in the named interface.
    fmt.Println(g)
    fmt.Println(g.area())
    fmt.Println(g.perim())
  }

  func main() {
    r := rect{width: 3, height: 4}
    c := circle{radius: 5}

    measure(r)
    measure(c)
  }

