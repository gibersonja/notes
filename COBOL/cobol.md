# COBOL Notes
- Reference:
  - [https://www.mainframestechhelp.com/tutorials/cobol/coding-sheet.htm](https://www.mainframestechhelp.com/tutorials/cobol/coding-sheet.htm)
  - Murach's Mainframe COBOL by Mike Murach, Anne Prince, Raul Menendez

[TOC]
## Program Structure
### Source Code Format
```
┃ SEQUENCE NUMBER ┃ ┃   AREA A  ┃   AREA B   ┃ COMMENT AREA ┃
┃ 1| 2| 3| 4| 5| 6┃7┃ 8| 9|10|11┃12| .... |72┃73| ...... |80┃
                   ↑
            IDENTIFICATION
                 AREA
                   
```
- By using `>>SOURCE FORMAT FREE` you are allowed to avoid the above formating rules.
```
DIVISIONS
 ↳ SECTIONS
    ↳ PARAGRAPHS
       ↳ SENTENCES
          ↳ STATEMENTS
```
- Sections
  - Logical subdivision of program logic.
  - A *section* is a collection of *paragraphs*.
- Paragraphs
  - The subdivision of a *section* or *division*.
  - Either a user-define or a predefined name followed by a period, and consists of zero or more *sentences*/*entries*.
- Sentences
  - The combinatation of one or more *statements*.  Appear only in the `PROCEDURE DIVISION`. Must end with a period.
- Statements
  - Meaningful COBOL statements that preform some processing.
- Characters
  - Lowest in the hierarchy and **cannot** be divisible.
Example:
```cobol
 PROCEDUR DIVISION.
 A0000-FIRST-PARA SECTION.
 FIRST-PARAGRAPH.
 ACCEPT WS-ID                                                           -----|
 MOVE '10' TO WS-ID                                                     -----+-- Sentence - 1
 DISPLAY WS-ID                                                          -----|
```
### Divisions / Sections
#### `IDENTIFICATION DIVISON`
- Contains informations about the program like the name used to call for the program to execute, as well as the authors name and date created.
```cobol
IDENTIFICATION DIVISION.
PROGRAM-ID. helloworld.
AUTHOR. Jason Giberson .
DATE-WRITTEN. July 31st 2024
```
#### `ENVIRONMENT DIVISION`
- Contains environment information about computer the program runs on, devices available, country specific information. Sections are:
##### `CONFIGURATION SECTION`
- Details about the machine used (source and target computer).
- Define currency symbol used.
```cobol
 ENVIRONMENT DIVISION.
 CONFIGURATION SECTION.
 SOURCE-COMPUTER IBM ENTERPRISE Z/OS.
 OBJECT-COMPUTER VAX-6400

 SPECIAL-NAMES.
     CURRENCY IS DOLLAR.
* CONFIGURATION - CONTAINS THE MACHINE RELATED DETAILS
```
##### `INPUT-OUTPUT SECTION`
- Defines the input and output files used in the program and links it to external device where the file is stored.
- File Control
  - Provides information of external data sets used in the program.
- I-O Control
  - Provides information of files used in the program.
```cobol
 ENVIRONMENT DIVISION.
   INPUT-OUTPUT SECTION.
   FILE-CONTROL.
   SELECT INPUTFL ASSIGN TO DDINFL
   ORGANIZATION IS SEQUENTIAL
   ACCESS MODE IS SEQUENTIAL
   FILE STATUS IS INPUTFL-FS.
* LINKS THE FILES USED IN PROGRAM WITH EXTERNAL I/O DEVICES
```
#### `DATA DIVISION`
- Used to describe the data items used in the program. Has 4 sections being `FILE`, `WORKING-STORAGE`, `LINKAGE`, and `REPORT`.
  - `FILE`
    - Describes data sent or received from storage.
    - Under this, it has `FD` (File Description) for each file to define the layout of the file.
    ```cobol
     DATA DIVISION.
       FILE SECTION.
         FD
         01 INP-RECORD.
           05 INVOICE  PIC X(10).
           05 STORE-ID PIC X(05).
           05 ITEM-ID  PIC X(10).
    ```
  - `WORKING-STORAGE`
    - Defines variables in the program.
    ```cobol
     DATA DIVISION.
       WORKING-STORAGE SECTION.
         01 INPUTFL-FS    PIC 9(02).
         01 WS-ID-DISPLAY PIC X(30).
    ```
  - `LINKAGE`
    - Defines data available to other programs.
    - Used incase a program (main program) calls another program (sub-program).
    - `LINKAGE SECTION` is defined in the sub-program.
    - Need to define the arguments which are passed by the main program in the `LINKAGE-SECTION` as well.
  - `REPORT`
    - Deals with generating reports.
  - `COMMUNICATION SECTION` (*Mostly not used*)
    - For communicating between 2 programs running simultaneously.
  - `LOCAL-STORAGE SECTION`
    - Similar to `WORKING-STORAGE SECTION` but the variables will be allocated and initialized every time a program starts execution.
#### `PROCEDURE DIVISION`
- The main division where the business logic is kept.
- All COBOL programs must have `PROCEDURE DIVISION`.
- User defined sections, paragraphs, sentences, statements, clauses, and verbs are defined in this section.
## Compilation
- ```
                        ╭────────────────╮
                        │ Source Program │
                        ╰───────┬────────╯
                                │
                                V
    ╭──────────────╮    ╭────────────────╮    ╭─────────────────╮
  1 │ Copy Members │───>│ COBOL Compiler │───>│ Compiler Output │
    ╰──────────────╯    ╰───────┬────────╯    ╰─────────────────╯
                                │
                                V
                        ╭────────────────╮
                        │ Object Module  │
                        ╰───────┬────────╯
                                │
                                V
    ╭─────────────╮     ╭────────────────╮    ╭────────────────╮
  2 │ Subprograms │────>│ Linkage Editor │───>│ Linkage Editor │
    ╰─────────────╯     ╰───────┬────────╯    ╰────────────────╯
                                │
                                V
                        ╭────────────────╮
                        │   Load Module  │
                        ╰───────┬────────╯
                                │
                                V
    ╭─────────────╮   ╭────────────────────╮    ╭─────────────────╮
  3 │ Input Files │──>│ Program  Execution │───>│ Test Run Output │
    ╰─────────────╯   ╰────────────────────╯    ╰─────────────────╯
- Step 1
  - The compiler compiles the *source program* into a *object module*.
  - If needed, it retrieves a copy of members that is needed for the compilation from the specified libraries.
  - Output is printed that identify *compile-time errors*.
- Step 2
  - The *linkage editor* will link the object module with any needed subprograms thereby creating an executable program known as a *load module*.
- Step 3
  - The program runs and you can then see if it runs correctly.
  - If something cannot be executed, a *run-time error* will occur and program will be terminated.

## Variables, Literals, Figurative Constants
### Variables
- Identifier to hold a value, identifies memory.
- Maximum length of 30 characters.
- Contains only digits (0-9), letters (A-Z), and hyphens (-).
- Must **not** be a COBOL reserved word.
- Should not contain any spaces.
### Literals
- Literals are constants, directly hard-coded into the program.
- Literals are of 2 types:
  - Numeric Literal
    - Maximum of 18 characters
    - Valid characters are:
      - 0-9 (any number).
      - One sign only (+ or -) used on the left side only.
      - One decimal point only (must **not** be used on the end).
      - Example:
        - `123`
    - Non-Numeric (Alphanumeric) Literal:
      - Maximum of 160 characters in length.
      - Must start and end with quotes.
      - Example:
        - `"I AM AN EXAMPLE OF NON-NUMERIC LITERAL"`
        - `'123'`
### Figurative Constants
- These are constants which are predefined (built-in) in COBOL.
- `ZERO` or `ZEROS` or `ZEROES`.
- `SPACE` or `SPACES`.
- `HIGH-VALUE` or `HIGH-VALUES`.
  - Highest ordinal position.
- `LOW-VALUE` or `LOW-VALUES`.
  - Lowest ordinal position.
- `QUOTE` or `QUOTES`.
  - Quotation (`"`) or apostrophe (`'`).
- `ALL`
- `NULL` or `NULLS` *Don't use `HIGH-VALUE`/`HIGH-VALUES`/`LOW-VALUE`/`LOW-VALUES` with numeric fields*.
## Data Types / Levels
### Data Types
- Denoted by `PICTURE` (`PIC`) clause.
- `PIC` clause provides the data type along with length.
- 3 main data types:
  - Numeric
    - 0 to 9, maximum length is 18.
    - Denoted by `9`.
    - Types of Numeric Data Types:
      - `9` - Numeric - 0-9, max length is 18. Holds actual memory space. Example: `PIC 9(2)`.
      - `S` - Sign Data Type - Links a sign to a number.  If present, the number is signed.  If absent the number is unsigned.
      - `+` - Sign - plus sign, used to print (+) as sign.
      - `-` - Sign - minus sign, used to print (-) as sign.
      - `V` - Implied Decimal - Does **not** hold memory space.  Not used for display but rather for computation.  Example: `PIC 9(4)V99`
        If the value of the above example is defined as `123456`, then it is like `1234.56` but it will not be displayed like `1234.56`.  If used in computation, the value will be `1234.56` which will take part in computation.
      - `.` - Actual Decimal - Used for display, not for calculation or any arithmetic operation.  Example: `PIC 9(4).99`
        - if value `123456` is used in the above example, then it will be displayed as `1234.56` but it cannot be used for computation.
      - `Z` - Supress Zeros - Used to supress only the leading Zeros with blanks.  Does not do anything with non-zeros.  Example: `PIC ZZ99.99`
        - If the value passed to the variable in the above example is `0012.34` then it will be `bb12.34` (where `b` is a blank character).
      - `,` - Comma - Used to insert a comma into the data item at a particular position.
      - `$` - Dollar Symbol - Used to insert a dollar sign at the first position.  Normally used for currency.
    - `999 == 9(3)` and `99 == 9(2)`
  - Alphabet
    - A to Z, a to z, or SPACE.  Maximum length is 255.
    - Denoted by `A`
  - Alphanumeric
    - Combination of numeric and alphabet.
    - Denoted by `X`.
### Level Numbers
#### Level `01`-`49`
- Used for general purpose.
##### Level `01`
- Topmost level.
- Can be used for individual data item or group item.
  - There **cannot** be another `01` level within a `01` level.
  - Example: `01 EMP-ID PIC 9(5)` this is an individual variable and no sub items are present below this.
- Can also be used for group data item
  - Group data item does not have a picture clause.  It has elementary items below it
  - Level numbers with the same numeric value are considered to be at the same level.
- As you move down, the level number must increase
```cobol
 01 EMP-RECORD.
   05 EMP-ID PIC 9(5).
   05 EMP-NAME.
     10 FIRST-NAME PIC A(15).
     10 LAST-NAME PIC A(15).
```
#### Level `66`,`77`,`88` (Special Purpose)
##### Level `66`
- Used for `RENAMES` clause.  It should **not** have a picture clause.
```cobol
 01 EMP-REC
   05 EMP-ID PIC 9(5).
   05 EMP-NAME PIC A(10).
   05 EMP-BIRTH-DATE PIC X(10).
 66 EMP-DETAIL RENAMES EMP-ID THRU EMP-NAME.
```
##### Level `77`
- Normally avoid using level `77`.
- Used for individual data item or elementary data item.
- It **cannot** be further subdivided.
##### Level `88`
- Used for conditional processing.
- This **must be** coded under a group item.
- It works on the principle of *true* or *false*.
```cobol
 01 CHECK-DAY.
   05 DAY PIC X(3).
     88 MONDAY    VALUE 'MON'.
     88 TUESDAY   VALUE 'TUE'.
     88 WEDNESDAY VALUE 'WED'.
     88 THURSDAY  VALUE 'THU'.
     88 FRIDAY    VALUE 'FIR'.
     88 SATURDAY  VALUE 'SAT'.
     88 SUNDAY    VALUE 'SUN'.
```
## Datasets
### `PS`
  - Physical Sequential
  - Similar to a file
### `PDS`
  - Partitioned Data Set
  - Similar to a folder
### Space Units
  - `BLKS` : Blocks
    - 1 Track has a minium of 6 Blocks
  - `TRKS` : Tracks
    - 1 Track = ~192KB or 48KB or 56KB - Depends on which  type of `DASD` you use
  - `CYLS` : Cylinders
    - 1 Cyliner = 15 Tracks
  - `KB` : KiloBytes
  - `MB` : MegaBytes
  - `BYTES` : Bytes
 
  *A sequential data set can have 16 extents on  each volume*
  *A `PDS` can have 16 extents*
  *A `PDSE` can have 123 extents*
### Units of Spaces Allocated
  - Primary Quantity
    - Must be non-zero for `PDS`
  - Secondary Quantity
    - Proveded when primary allocation is done
 
  *Total extends = 16 per volume*
  *Therefore:*
  `Total Units (TRKS/BLKS/CYLS) = Primary + 15 * Secondary`
  **NOTE:** *This number can also change depending upon how many extents were available in contigous memory*
### Directory Blocks
  - Data Sets with `ISPF` statistics: `6`
  - Data Sets without `ISPF` statistics: `21`
### Record Format
  - `F` - Fixed-length records
  - `FB` - Fixed Blocks
  - `V` - Variable-length records
  - `VB` - Variable-length blocks
  - `U` - Undefined format records

**Record Length:** *The logical record length, in bytes, of the records to be stored in the data set*
**Block Size:** *The block size, also called **Physical Record Length**, of the blocks to be stored in t he data set.  Use this field to specify how many bytes of data to put into each block, based on the record length*
### Important Data Set Name Type
  - `LIBRARY`
    - Allocates a partitioned data set extended (`PDSE`)
  - `PDS`
    -  Allocates a partitioned date set
  - `LARGE`
    - Allocates a large format sequential data set
  - blank
    - Allocates a partitioned or sequential data set based on the data set characteristics entered.

## Statements
### `ACCEPT`
- Syntax:
  - `ACCEPT data-name`
- Example:
  - `ACCEPT SALES-AMOUNT`
- Description:
  - When an `ACCEPT` statement is executed, the program waits for the user to type an entry and press the Enter key.
  - When Enter is pressed, the entry is stored in the variable given as an argument in the `ACCEPT` statement and the cursor moves to the next line.
  - Input should be consistent with the `PICTURE` of the variable. If not, it will be truncated/adjusted according to the below table:
    - | `PICTURE` | Input | Value Stored | Notes |
      | :---: | :---: | :---: | :---: |
      | `S999` | `10` | `10` | |
      | `S999` | `787` | `787` | |
      | `S999` | `-10` | `-10` | |
      | `S999` | `5231` | `231` | Truncated on the left |
      | `999` | `-100` | `100` | Sign dropped |
      | `999` | `458.12` | `458` | Decimal digits dropped |
      | `9(3)V99` | `458.12` | `458.12` | |
      | `9(3)V99` | `45812` | `812.00` | Truncated on the left |
      | `9(3)V99` | `4735.26` | `735.26` | Truncated on the left |
      | `X` | `Y` | `Y` | |
      | `X` | `Yes` | `Y` | Truncated on the right |
### `DISPLAY`
- Syntax:
  - `DISPLAY {data-name-1 | literal-1} ...`
- Example:
  - ```cobol
    DISPLAY " " .
    DISPLAY 15000.
    DISPLAY "----------------------------------------------".
    DISPLAY "End of session.".
    DISPLAY "SALES-AMOUNT.
    DISPLAY "THE SALES AMOUNT IS " SALES-AMOUNT ".".
    DISPLAY "THE SALES TAX IS " SALES-TAX ".".
  - Output:
  - ```
    (blank line/space)
    125000
    ----------------------------------------------
    End of session.
    100.00
    THE SALES AMOUNT IS 100.00.
    THE SALES TAX IS 7.85.
- Description:
  - The `DISPLAY` statement will display one (or more) literal or variable values on screen/terminal.
  - The cursor is moved to the next line.
  - Multiple literals/variables must be space seperated.
### `MOVE`
- Syntax:
  - `MOVE {data-name-1 | literal} TO data-name-2`
- Example:
  - ```cobol
    MOVE "Y" TO END-OF-SESSION-SWITCH.
    MOVE 1 TO PAGE-NUMBER.
    MOVE NUMBER-ENTERED TO EDITED-NUMBER-ENTERED.
- Description:
  - The `MOVE` statement places data from a literal or a sending field to a receiving field.
  - The original data is retained in the sending field.
  - If sending field is numeric and receiving field is numeric edited, the statement converts the data from one to the other. (See table below)
  - If the receiving field is larger than the sending field, the receiving field is padded with trailing blanks in alphanumeric field and padded with zeros in numeric fields.
  - If the receiving field is smaller than the sending field, the data moved could be truncated, *this should be avoided*.
  - | Type of `MOVE` | Legal |
    | :---: | :---: |
    | Alphanumeric to alphanumeric | Yes |
    | Numeric to numeric | Yes |
    | Numeric to numeric edited | Yes |
    | Alphanumeric to numeric | Only if sending field is unsigned integer |
    | Alphanumeric to numeric edited | Only if sending field is unsigned integer |
    | Numeric to alphanumeric | Only if sending field is unsigned integer |
  - | `PICTURE` of Sending Feild | Data is Sending Field | Sign of Sending Field | `PICTURE` of Receiving Field | Edited Result |
    | :---: | :---: | :---: | :---: | :---: |
    | `S9(6)` | `000123` | `+` | `ZZZ,ZZ9-` | `123` |
    | `S9(6)` | `012345` | `-` | `ZZZ,ZZ9-` | `12,345-` |
    | `S9(6)` | `000000` | `(no sign)` | `ZZZ,ZZ9-` | `0` |
    | `S9(4)V99` | `012345` | `+` | `ZZZZ.99` | `123.45` |
    | `S9(4)V99` | `000000` | `(no sign)` | `ZZZZ.99` | `.00` |
### `COMPUTE`
- Syntax:
  - ```cobol
    COMPUTE data-name [ROUNDED] = arithmetic-expresion
         [ON SIZE ERROR statement-group]
  - Example:
    ```cobol
      COMPUTE YEAR-COUNTER = YEAR-COUNTER + 1.
      COMPUTE SALES-TAX ROUNDED =
          SALES-AMOUNT * .0785.
      COMPUTE SALES-CHANGE = THIS-YEAR-SALES - LAST-YEAR-SALES.
      COMPUTE CHANGE-PERCENT ROUNDED =
              SALES-CHANGE / LAST-YEAR-SALES * 100
          ON SIZE ERROR
              DISPLAY "SIZE ERROR ON CHANGE PERCENT".
      COMPUTE NUMBER-SQUARED = NUMBER-ENTERED ** 2.
- Description:
  - You can use the `ROUNDED` clause whenever the result can have more decimal places than are specified in the `PICTURE` of the reuslt field.
  - If the `ROUNDED` clause is not used, excess decimal places are truncated.
  - You can use `ON SIZE ERROR` cluase when there is a posibility the result may be larger than the receiving field.
### `ADD`
- Syntax:
  - Format 1:
    - ```cobol
      ADD {data-name-1 | literal} TO data-name-2 [ROUNDED]
          [ON SIZE ERROR statement-group]
  - Format 2:
    - ```cobol
      ADD {data-name-1 | literal-1} {data-name-2 | literal-2} ...
          GIVING data-name-3 [ROUNDED]
          [ON SIZE ERROR statement-group]
- Example:
  - Format 1:
    - ```cobol
      ADD 1 TO YEAR-COUNTER.
      ADD CUSTOMER-SALES TO GRAND-TOTAL-SALES.
  - Format 2:
    - ```cobol
      ADD OLD-BALANCE NEW-CHARGES
          GIVING NEW-BALANCE.
      ADD JAN-SALES FEB-SALES MAR-SALES
          GIVING FIRST-QUARTER-SALES.
- Description:
  - Format 1:
    - The value of `data-name-1` or a literal value is added to the value in `data-name-2` and is stored in `data-name-2`
  - Format 2:
    - When two or more values are added together, the result is stored in the data item that is named by the `GIVING` clause.
  - You can use the `ROUNDED` clause whenever the result may have more decimal places than specified in the `PICTURE` clause for the result field.
  - You can use `ON SIZE ERROR` cluase if the result may be larger than the receiving field.
### `IF`
- Syntax:
  - ```cobol
    IF condition
        statement-group-1
    [ELSE
        statement-group-2]
    [END-IF]
  - Condition:
    - `{data-name-1 | literal} relational-operator {data-name-2| literal}`
- Example:
  - Without `ELSE` and `END-IF` caluses:
    - ```cobol
      IF SALES-AMOUNT = ZERO
          MOVE "Y" TO END-OF-SESSION-SWITCH.
      IF SALES-AMOUNT NOT = ZERO
          COMPUTE SALES-TAX ROUNDED = SALES-AMOUNT * .0785
          DISPLAY "SALES TAX = " SALES-TAX.
  - With `ELSE` and `END-IF` clauses:
    - ```cobol
      IF SALES-AMOUNT = ZERO
        MOVE "Y" TO END-OF-SESSION-SWITCH
      ELSE
        COMPUTE SALES-TAX ROUNDED = SALES-AMOUNT * .0785
        DISPLAY "SALES TAX = " SALES-TAX
      END-IF.
  - Nested `IF` statements:
    - ```cobol
      IF SALES-AMOUNT >= 10000
          IF SALES-AMOUNT < 50000
              COMPUTE SALES-COMMISSION = SALES * COMMISSION-RATE-1
          ELSE
              COMPUTE SALES-COMMISSION = SALES * COMMISSION-RATE-2
          END-IF
      END-IF.
- Description:
  - Self-explanatory
### `PERFORM`
- Syntax:
  - `PERFORM procedure-name`
- Example:
  - `PERFORM 100-GET-USER-ENTRIES.`
- Description:
  - The `PERFORM` statement will jump to the procedure that is given as an argument, execute the statements for that procedure, then return to the next statement after the `PERFORM` statement.
- Syntax (`PERFORM UNTIL`):
  - ```cobol
    PERFORM procedure-name
        UNTIL condition
- Example (`PERFORM UNTIL`):
  - ```cobol
    PERFORM 100-CALCULATE-ONE-SALES-TAX
        UNTIL END-OF-SESSION-SWITCH = "Y".
- Description (`PERFORM UNTIL`):
  - If the given condition is never true, the program will not stop running, this is an error.
### `SELECT`
- Syntax:
  - `SELECT file-name ASSIGN TO system-name`
- Example:
  - ```cobol
    SELECT CUSTMAST ASSIGN TO AS-CUSTMAST.
    SELECT SALESRPT ASSIGN TO SALESRPT.
- Description:
  - Identifies a disk file or print file to be used by the program.
  - The `ddname` that is coded is the system name for a disk file used in JCL when the program runs to assign the file to a specific file on disk.
  - IBM mainframes use two types of sequential disk files:
    - VSAM (Virtual Storage Access Method)
    - non-VSAM (Common)
  - Before output is printed, it is written to a temporary disk file which is printed when the printer is avialabile.  This is called *spooling*.
### `FD`
- Syntax:
  - ```cobol
    FD file-name
       [RECORD CONTAINS integer CHARACTERS]
- Example:
  - ```cobol
    FD   CUSTMAST.

    01   CUSTOMER-MASTER-RECORD.
         05 CM-BRANCH-NUMBER         PIC 9(2).
         05 CM-SALESREP-NUMBER       PIC 9(2).
         05 CM-CUSTOMER-NUMBER       PIC 9(5).
         05 CM-CUSTOMER-NAME         PIC X(20).
         05 CM-SALES-THIS-YTD        PIC S9(5)V9(2).
         05 CM-SALES-LAST-YTD        PIC S9(5)V9(2).
- Description:
  - The filename used must be the same as the one used in the `SELECT` statement in the `ENVIRONMENT DIVISION`.
  - The `RECORD CONTAINS` caluse indicates the number of bytes that must be in the record description for the file.
### `OPEN` and `CLOSE`
- Syntax:
  - ```cobol
    OPEN INPUT  file-name-1 ...
         OUTPUT file-name-2 ...
    
    CLOSE file-name ...
- Example:
  - ```cobol
    OPEN INPUT CUSTMAST
         OUTPUT SALESRPT.
    
    CLOSE CUSTMAST
          SALESRPT.
- Description:
  - The filename used must be the same that was defined in the `SELECT` statement.
  - The `OPEN` statement must be used on a file before a `READ` or `WRITE` statement can be used for that file.
  - The `CLOSE` statement must be used on all open files before the `STOP RUN` statement can be used.
  - By closing and re-opening a file, the records can be read again from the beginning.
### `READ` and `WRITE`
- Syntax:
  - ```cobol
    READ file-name [RECORD]
        AT END
            imperative-statement-1 ...
        [NOT AT END]
            imperative-statement-2 ...

    WRITE record-name
       AFTER ADVANCING PAGE
       AFTER ADVANCING integer [LINE|LINES]
       AFTER ADVANCING data-name [LINE|LINES]
- Example:
  - ```cobol
    READ CUSTMAST RECORD
        AT END
            MOVE "Y" TO CUSTMAST-EOF-SWITCH.

    READ CUSTMAST
        AT END
            MOVE "Y" TO CUSTMAST-EOF-SWITCH
        NOT AT END
            ADD 1 TO RECORD-COUNT.

    WRITE PRINT-AREA
        AFTER ADVANCING PAGE.

    WRITE PRINT-AREA
        AFTER ADVANCING 1 LINE.

    WRITE PRINT-AREA
        AFTER ADVANCING SPACE-CONTGROL LINES.
- Description:
  - When the `READ` statement is executed for a disk file, it will read the next record in sequence into the record description
  - If there are no more records in the file: the `AT END` clause is executed.
  - Otherwise, the `NOT AT END` clause is executed.
  - When the `WRITE` statement is executed, on record is printed from the print area for the file and the paper is advanced the number of lines indicated by the `AFTER ADVANCING` clause or to the top of the next page with the `PAGE` argument.
### `CURRENT-DATE`
- Syntax:
  - `FUNCTION CURRENT-DATE`
- Data Description:
  - ```cobol
    01    CURRENT-DATE-AND-TIME.
          05  CD-YEAR                        PIC 9(4).
          05  CD-MONTH                       PIC 9(2).
          05  CD-DAY                         PIC 9(2).
          05  CD-HOURS                       PIC 9(2).
          05  CD-MINUTES                     PIC 9(2).
          05  CD-SECONDS                     PIC 9(2).
          05  CD-HUNDREDTH-SECONDS           PIC 9(2).
          05  CD-GREENWHICH-MEAN-TIME-SHHM   PIC X(5).
- Example:
  - ```cobol
    DISPLAY FUNCTION CURRENT-DATE.
    MOVE FUNCTION CURRENT-DATE TO CURRENT-DATE-AND-TIME.
- Description:
  - The five-character GMT that is returned indicates the  number of hours  and minutes that the current  time is ahead or behind GMT.
### `ACCPET`
- Syntax:
  - ```cobol
    ACCEPT data-name FROM DATE [YYYYMMDD]
    ACCEPT data-name FROM TIME
- Example:
  - ```cobol
    01  CURRENT-DATE-AND-TIME.
        05  CD-CURRENT-DATE.
            10  CD-CURRENT-YEAR      PIC 9999.
            10  CD-CURRENT-MONTH     PIC 99.
            10  CD-CURRENT-DAY       PIC 99.
        .
        .
        ACCEPT CD-CURRENT-DATE FROM DATE YYYYMMDD.
- Description:
  - If your compiler does not support the `CURRENT-DATE` function, you need to use the `ACCEPT DATE` and `ACCEPT TIME` statements.
