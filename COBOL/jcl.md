# JCL (Job Control Language)
[TOC]
## Introduction
- Scripting language
- Provide information about Datasets(I/O Files), Programs/Utilities, and Output Devices to be used during the execution of a job.
```mermaid
flowchart TD;
  node1[[JOB JCL]]-->node2[Job Entry System]-->node3[JCL Interpretation in JES]-->node4[Job Queuing]-->node5[Job Execution and log creation in SPOOL];
  node6[(Input Data)]-->node5;
  node5-->node7[(Output data to printer/dataset/SPOOL)]
```
## Stages
1. Input
1. Conversion
1. Processing
1. Output
1. Print
1. Purge
- JES (Job Entry Subsystem)
  - Keeps track of jobs that enter the system
  - Presents them to Z/OS for processing
  - Send their spool output to the correct destination based on parameters in JOB card.
- Initiators
  - System program that:
    - Processes JCL
    - Setup envrionment in address space
    - Runs batch job in the same address space
## Control Statements
1. **JOB Statement:** Marks begining of a job
1. **EXEC Statement:** Marks begining of a job step
1. **DD Statement:** Provide information for the input and output datasets
*(DD: Data Definition)*
```
                                  80  Bytes
 ╭───────────────────────────────────────────────────────────────────────────────────╮
 │Identifier                                                                         │
 │    │  ╭─ Name ╭─ Operation   ╭──── Parameters                                     │
 │   ╭┴╮╭┴────╮╭─┴─╮╭───────────┴───────────────────────────────────────────────────╮│──╮
     //TPTRK23A JOB 99999 'Topictrick',MSGCLASS=A,MSGLEVEL=(1,1),                       │
     //             NOTIFY=&SYSUID                                                      │
     //*                                             ╭──────────────╮                   │
     //* JCL TO RUN TREMP001 COBOL Program.   ╭──────┤ Program Name ├──────────╮        │
     //*                                      │      ╰──────────────╯          │        │
     //STEP01   EXEC PGM=TREMP001 ←-----------╯                                │        ├─── JCL
     //STEPLIB  DD   DSN=TP01.SYSLIB.LODLIB,DISP=SHR ─╮     ╭───────────╮      │        │
╭─── //EMPMAST  DD   DSN=TP01.EMPFLE.MASTER,DISP=SHR  │ ←---│ File Name │      │        │
├─── //EMPREPT  DD   DSN=TP01.EMPFLE.REPORT,DISP=SHR ─╯     ╰───────────╯      │        │
│    //SYSPRINT DD  SYSOUT=*                                                   │        │
│    //SYSOUT   DD  SYSOUT=*                                                   │        │
│    //                                                                        │        │
│──────────────────────────────────────────────────────────────────────────────┼────────╯
│──────────────────────────────────────────────────────────────────────────────┼────────╮
│ IDENTIFICATION DIVISION.  ╭──────────────────────────────────────────────────╯        │
│ PROGRAM-ID. TREMP001. ←---╯                                                           │
│*                                                                                      │
│* PROJ DESC: COBOL PROGRAM TO GENERATE TX REPORT.                                      │
│*                                                                                      │
│ ENVIRONMENT DIVISION.                                                                 ├── COBOL
│ INPUT-OUTPUT SECTION.                                                                 │  Program
│ FILE-CONTROL.                                                                         │
├── SELECT EMPIN ASSIGN TO EMPMAST.                                                     │
╰── SELECT EMPRP ASSIGN TO EMPREPT.                                                     │
  DATA DIVISION.                                                                        │
 *                                                                                      │
 ───────────────────────────────────────────────────────────────────────────────────────╯
```
![JCL Format](jcl1.png)
- Every line should start with the **null indicator** (*//*) in the first two columns that includes a comment
- Parameters should be separated by a comma (*,*)
- No space is allowed in between parameters except comma (*,*)
- The continuation character (*,*) can be coded anywhere between 17 to 72 characters
- If any statement continues more than one line:
  - The current line should end with a comma (*,*) immediately after the last parameter
  - The following line should start on or before the 16th column
- If there is more than one space between the fields in a line, we can replace the multiple spaces with one
- Null indicator (*//*) alone represents the end of the job
- Every job must contain at a minium of two types of control statements: JOB card and EXEC statement
```
┌──────────────────────────────────────────────────────────────────────────────────────────────────┐
│                   ╭──────────────────────────╮                                           JOB CARD│
│                   │ JOB Statement / JOB Card │                                                   │
│                   ╰────────────┬─────────────╯                                                   │
└────────────────────────────────┼─────────────────────────────────────────────────────────────────┘
┌────────────────────────────────┼─────────────────────────────────────────────────────────────────┐
│            ╭────────────┬──────┴─────┬────────────╮                                      JOB BODY│
│            ↓            ↓            ↓            ↓                                              │
│ ╭────────────────────╮                  ╭──────────────────────╮                                 │
│ │ EXEC Statement (1) │  ..............  │ EXEC Statement (255) │                                 │
│ ╰──────────┬─────────╯                  ╰───────────────┬──────╯                                 │
│       ╭────┴───────┬─────┬───────╮                 ╭────┴───────────┬─────┬───────╮              │
│       ↓            ↓     ↓       ↓                 ↓                ↓     ↓       ↓              │
│╭───────────────╮           ╭─────────────────╮  ╭───────────────╮            ╭─────────────────╮ │
││      DD       │           │       DD        │  │      DD       │            │       DD        | |
││ Statement (1) │   ......  │ Statement (255) │  │ Statement (1) │   .......  │ Statement (255) | |
│╰───────────────╯           ╰─────────────────╯  ╰───────────────╯            ╰─────────────────╯ |
└──────────────────────────────────────────────────────────────────────────────────────────────────┘
```
- JOB Card
  - A **JOB Statement** with all its parameters is also called a **JOB Card**
  - A JOB Card makes the OS aware of the specific job after it submits and provides the parameters required to run the job
  - A JOB Card is mandatory for the job to run and should code only once at the beginning of the job
- JOB Body
  - A JOB Statement contains the **JOB** keyword, and it is also called a JOB Card.  Except the JOB Card, the remaining part of the JCL is called a JOB Body
  - A JOB Body contains multiple steps and can have up to 255 steps
  - Each step is coded with a single **EXEC Statement**
  - An EXEC Statement is mandatory for the particular step of a JOB Body.
  - A JOB can have a max of up to 255 EXEC Statements
### JOB Card
#### Positional Parameters
| Positional Parameter | Description |
| ---                  | ---         |
| **Account Information** | This refers to the person or group to which the CPU time is owed.  It is set as per the rules of the company owning the mainframes.  If it is specified as (*), then it takes the id of the user, who has currently logged into the Mainframe Terminal. |
| **Programmer Name** | This identifies the person or group, who is in charge of the JCL.  This is not a mandatory parameter and can be replaced by a comma. |
#### Keyword Parameters
| Keyword Parameter | Description |
| --- | --- |
| **CLASS** | Based on the time duration and the number of resources required by the job, companies assign different job classes.  These can be visualized as individual schedulers used by the OS to receive the jobs. Placing the jobs in the right scheduler will aid in the easy execution of the jobs. Some companies have different classes for jobs in test and production environment.  Valid values for CLASS parameter are A to Z charactersand 0 to 9 numberic (of length 1).  Following is the syntax: **CLASS=0 to 9 \| A to Z** |
| **PRTY** | To specify the priority of the job within a job class. If this parameter is not specified, then the job is added to the end of the queue in the specified CLASS.  Syntax: **PRTY=N** <br>Where N is a number in between 0 to 15 and the higher the number, the higher the priority. |
| **NOTIFY**| The system sends the success or failure message (Maximum Condition Code) to the user specified in the parameter. Syntax: **NOTIFY="userid \| &SYSUID"**  <br>The system send the message to the user "userid" but if we use NOTIFY = &SYSUID, then the message is sent to the user submitting the JCL. |
| **MSGCLASS** | To specify the output destination for the system and Job messages when the job is complete. Syntax: **MSGCLASS=CLASS** <br>Valid values of CLASS can be from "A" to "Z" and "0" to "9".  MSGCLASS = Y can be set as a class to send the job log to the JMR (JOBLOG Management and Retrieval: a repository within mainframes to store the job statistics). |
| **MSGLEVEL** | Specifies the type of messages to be written to the output destination specified in the MSGCLASS. Syntax: **MSGLEVEL=(ST,MSG)**  <br>ST = Type of statements written to output log<br>- When ST = 0, Job statements only.<br>- When ST = 1, JCL along with symbolic parameters expanded.<br>- When ST = 2, Input JCL only.<br>MSG = Type of messages written to output log.<br>- When MSG = 0, Allocation and Termination messages written upon abnormal job completion.<br>- When MSG = 1, Allocation and Termination messages written irrespective of the nature of job completion. |
| **TYPRUN** | Specifies a special processing for the job.  Syntax: **TYPRUN = SCAN \| HOLD** <br>Where SCAN and HOLD has the following description:<br>- TYPRUN = SCAN  checks the syntax errors of the JCL without executing it.<br>TYPRUN = HOLD puts the job on HOLD in the job queue.  To release the job, "A" can be typed against the job in the SPOOL, which will bring the job to execution. |
| **TIME** | Specifies the time span to be used by the processor to execute the job.  Synatx: **TIME=(mm,ss) \| TIME=ss**<br>Where mm = minutes and ss = seconds<br>This parameter can be useful while testing a newly codded program.  In order to ensure that the program does not run for long because of looping errors, a time aprameter can be coded so that the program ends when the specified CPU time is reached. |
| **REGION** | Specifies the address space required to run a job step within the job.  Syntax: **REGION=nK \| nM**<br>Here, region can be specified as nK or nM where n is a number, K is kilobyte and M is megabyte.<br>When REGION = 0K or 0M, largest address space is provided for execution.  In critical applications, coding of 0K or 0M is prohibited to aviod wasting the address space. |
#### Miscellaneous Parameters
| Parameter | Description |
| --- | --- |
| **ADDRSPC** | Type of storage used: Virtual or Real |
| **BYTES** | Size of data to be written to output log and the action to be taken when the size is exceeded. |
| **LINES** | Maximum number of lines to be printed to output log. |
| **PAGES** | Maximum number of pages to be printed to output log. |
| **USER** | User id used to submit the job. |
| **PASSWORD** | Password of the user-id specified in the USER parameter. |
| **COND and RESTART** | These are used in conditional job step processing and are explained in detail while discussing conditional Processing. |

### EXEC Statement
- The statement, which holds the job step program/procedure information
#### Syntax
`//Step-name EXEC Positional-param, Keyword-param`
#### Description
- `STEP-NAME`
  - This identifies the job step within the JCL. It can be of length 1 to 8 with alphanumeric characters
- `EXEC`
  - This is the keyword to identify it as an EXEC statement
- `POSITIONAL-PARAM`
  | Positional Parameter | Description |
  | --- | --- |
  | **PGM** | This refers to the program name to be executed in the job step. |
  | **PROC** | This refers to the procedure name to be executed in the job step. |
- `KEYWORD-PARAM`
  | Keyword Parameter | Description |
  | --- | --- |
  | **PARAM** | Used to provide parametrized data to the program that is being executed in the job step.  This is a program dependant field and do not have definite rules, except that the PARM values has to be included within quotation in the event of having special characters.<br> |
  | **ADDRSPC** | This is used to specify whether the job step requires virtual or real storage for execution.  Virtual storage is pageable whereas real storage is not and is placed in the main memory for execution.  Job steps, which require faster execution can be placed in real storage.  Syntax: **ADDRSPC=VIRT\|REAL**<br>When an ADDRSPC is not coded, VIRT is the default. |
  | **ACCT** | This specifies the accounting informatin of the job step. Syntax: **ACCT=(userid)**<br>This is similar to the postional parameter **accounting information** in the JOB statement. If it is coded both in JOB and EXEC statement, then the accounting information in JOB statement applies to all job steps where an ACCT parameter is not coded.  The ACCT parameter in an EXEC statement will override the one present in the JOB statement for that job step only. |
### DD Statements
#### Syntax
- `//DD-name DD Parameters`
#### Description
- `DD-NAME`
  - A DD-NAME identifies the dataset or input/output resource. If this is an input/output file used by a COBOL/Assembler program, then the file is referenced by this name within the program.
- `DD`
  - This is the keyword to identify it as an DD statement.
- `PARAMETERS`
  | Parameter | Description |
  | --- | --- |
  | **DSN** | The DSN parameter refers to the physical dataset name of a newly created or existing dataset.  The DSN value can be made up of sub-names each of 1 to 8 characters length, separated by periods and of total length of 44 characters (alphanumeric). Syntax: **DSN=Physical Dataset Name**<br>**Temporary datasets** need storage only for the job duration and are deleted at job completion.  Such datasets are represented as **DSN=&name** or simply without a DSN specified.<br>If a temporary dataset created by a job step is to be used in the next job step, then it is referenced as **DSN=*.stepname.ddname**.  This is called **Backward Referencin**. |
  | **DISP** | The DISP parameter is used to describe the status of the dataset, disposition at the end of the job step on normal and abnormal completion.  DISP is not required in a DD statement only when the dataset gets created and deleted in the same job step (like the temporary datasets).  Syntax: **DISP=(status, normal-disposition, abnormal-disposition)**<br>Following are valid values for **status**:<br>**- NEW:** The dataset is newly created by the job step......... |
  | **DCB** |  |
  | **SPACE** |  |
  | **UNIT** |  |
  | **VOL** |  |
  | **SYSOUT** |  |
## Parameters
## Procedures
### Instream Procedure
### Catalog Proccedure
### SET Statement
## Utilities