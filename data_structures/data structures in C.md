# Data Structures In C
[TOC]
## Structures
### The Need For Structures
- A structure is a collection of one or more variables, possibly of different types, grouped together under a single name for convenient handling.
### Declare A Structure
- In C, we declare a *data type* as a *structure* using the keyword `struct`.
- Example:
  ``` C
  struct date {int day, month, year;};
  ```
- Also:
  ``` C
  struct date {
    int day;
    int month;
    int year;
  };
  ```
- The struct: `struct date {int day; int month; int year;};` can be pictured as follows:
  ```
        day   month   year
      ╭──────┬──────┬──────╮
  dob │      │      │      │
      ╰──────┴──────┴──────╯
  ```
- We refer to the fields as: `dob.day`, `dob.month`, `dob.year`.
- In C, the period `.` is referred to as the **structured member operator**.
- Assignment:
  ``` C
  dob.day = 14;
  dob.month = 11;
  dob.year = 2019;
  ```
- Can be pictured as:
  ```
        day   month   year
      ╭──────┬──────┬──────╮
  dob │  14  │  11  │ 2019 │
      ╰──────┴──────┴──────╯
  ```
- **Note:**
  - *`.` is used when you have a struct variable (**dot notation**)*
  - *`->` is used when you have a pointer to a struct (**arrow notation**)*
#### `typedef`
- Can use `typedef` to give a name to some existing type, and this name can be used to declare variables of that type.
- Example:
  ``` C
  typedef int Whole;

  Whole amount, numCopies;
  ```
- This is exactly quivalent to: `int amount, numCopies;`
- We could give a short, meaningful name, `Date`, to the date structure shown earlier with the following declaration:
  ``` C
  typedef struct date {
    int day;
    int month;
    int year;
  } Date;
  ```
### Array of Structure
- Assuming:
  ``` C
  typedef struct {
    char name[31];
    int age;
    char gender;
  } Student;
  ```
- Suppose we want to store data on 100 students: `Student pupil[100];`
- This allocate storage for `pupil[0]`, `pupil[1]`, ... , `pupil[99]`.
- Each element `pupil[i]` consists of three fields to be refered to as follows: `pupil[i].name`, `pupil[i].age`, `pupil[i].gender`.
### Search an Array of Structure
``` C
int search(char key[], Student list[], int n) {
    for(int h = 0; h < n; h++) {
        if(strcmp(key, list[h].name) == 0) {
            return h;
        }
        return -1;
    }
}
```
### Sort an Array of Structures
``` C
void sort(Student list[], int n) {
    Student temp;
    int k;
    for(int h = 1; h < n; h++) {
        temp = list[h];
        k = h - 1;
        while(k >= 0 && strcmp(temp.name, list[k].name) < 0) {
            list[k + 1] = list[k];
            k = k - 1;
        }
        list[k + 1] = temp;
    }
}
```
### Nested Structures
- A structure within a structure is called a *nested* structure.
- Example:
  ``` C
  typedef struct {
    char name[31];
    Date dob;
    char gender;
  } Student;
  ```
### Work with Fractions
``` C
typedef struct {
    int num;
    int den;
} Fraction;
```
- If `f` is a variable of type `Fraction`, we can store 5/9 in `f` with this:
```C
f.num = 5;
f.den = 9;
```
- This can be pictured as follows:
```
   num den
  ╭───┬───╮
f │ 5 │ 9 │
  ╰───┴───╯
```
#### Manipulate Fractions
- We can write functions to perform various operations on fractions.
- Example:
```math
\begin{align*}
\frac{a}{b} + \frac{c}{d} = \frac{ad + bc}{bd}
\end{align*}
```
```C
Fractions addFractions(Fraction a, Fraction b) {
    Fraction c;
    c.num = a.num * b.den + a.den * b.num;
    c.den = a.den * b.den;
    return c;
}
```
### A Voting Problem
- *Problem*: In an election, there are seven canidates. Each voter is allowed one vote for the candidate of their choice. The vote is recorded as a number from 1 to 7. The number of voters is unknown beforehand, but the votes are terminated by a vote of `0`. Any vote that is not a number from from `1` to `7` is an invalid (spoiled) vote.
- A file, `votes.txt`, contains the naems of the candidates. The first name is considered as candidate `1`, the second as candidate `2`, and so on. The names are followed by the votes. Write a program to read the data and evaluate the results of the election. Print all output to the file, `results.txt`.

- Suppose the file `votes.txt` contains the following data:
```
Victor Taylor
Denise Duncan
Kamal Ramdhan
Michael Ali
Anisa Sawh
Carol Khan
Gary Owen

3 1 2 5 4 3 5 3 5 3 2 8 1 6 7 7 3 5
6 9 3 4 7 1 2 4 5 5 1 4 0
```

- Your program should send the following output to `results.txt`:
```
Invalid vote: 8
Invalid vote: 9

Number of voters: 30
Number of valid votes: 28
Number of spoilt votes: 2

Candidate          Score

Victor Taylor        4
Denise Duncan        3
Kamal Ramdhan        6
Michael Ali          4
Anisa Sawh           6
Carol Khan           2
Gary Owen            3

The winner(s):
Kamal Raamdhan
Anisa Sawh
```

- Consider these declarations:
``` C
typedef struct {
  char name[31];
  int numVotes;
} PersonData;
PersonData candidate[8];
```
- Here, `candidate` is an array of structures. We will use `candidate[1]` to `candidate[7]` for the seven candidates; we will not use `candidate[0]`. If not, then for a vote `v`, `candidate[v-1]` would have to be updated.
- To make the program flexible, we will define the following symbolic constants:
``` C
#define MaxCandidates 7
#define MaxNamLength  30
#define MaxNameBuffer MaxNameLength+1
```
- Also change the earlier declarations to the following:
``` C
typedef struct {
  char name[MaxNameBuffer];
  int numVotes;
} PersonData;

PersonData candidate[MaxCandidates+1];

typedef struct {
  int valid, spoilt;
} VoteCount;
```
- The solution is based on the following outline:
  - initialize
  - process the votes
  - print the results
- `candidate` array:
```
   name            numVotes
 ╭───────────────┬─────────╮
1│ Victor Taylor │    4    │
 ├───────────────┼─────────┤
2│ Denise Duncan │    3    │
 ├───────────────┼─────────┤
3│ Kamal Ramdhan │    6    │ 
 ├───────────────┼─────────┤
4│ Michael Ali   │    4    │
 ├───────────────┼─────────┤
5│ Anisa Sawh    │    6    │
 ├───────────────┼─────────┤
6│ Carol Khan    │    2    │
 ├───────────────┼─────────┤
7│ Gary Owen     │    3    │
 ╰───────────────┴─────────╯
```
### Pass Structures to Functions
- Consider a structure for a "book type":
``` C
typdef struct {
  char author[31];
  char title[51];
  char binding;
  double price;
  int quantity;
} Book;

Book text;

fun1(text.quantity); // value of text.quantity is passed
fun2(text.binding);  // value of text.binding is passed
fun3(text.price);    // value of text.price is passed
fun4(text.title);    // address of array text.title is passed
fun5(text.title[0]); // value of first letter of title is passed
fun6(text);
```

- In the last example, the fields of `text` are copied to a temporary place (called the *run-time heap*), and the copy is passed to `fun6`; that is, the structure is passed **"by value"**.
- If a structure is complicated or contains arrays, the copying operation can be time-consuming and when the function returns the values of the structure elements must be removed from the heap; this adds overhead, extra processing, etc.
- To avoid this overhead, the *address* of the structure could be passed: `func6(&text);`
## Pointers
### Define Pointers
- The term *pointer* is used to refer to an address in memory.
- A pointer variable is one that can hold the address of a memory location.
- If `ptr` is a pointer variable, we can assign a value to it, as in: `ptr = &num;`.
- This statement stores the *address* of `num` int `ptr`.
- We say that `ptr` "points to" `num`.
- Pointer declaration is as follows: `int *iPtr;`.
- This is read as "`int` pointer `iPtr`" and declares `iPtr` to be a *pointer* variable, which can "point to" `int` values only.
- **Caution**: If you want to declare three pointers to `int`, it might be tempting to use: `int* a, b, c;`
- This would be wrong, this is a pointer variable to `a` only, `b` and `c` are just `int`'s.
- The correct way is: `int *a, *b, *c;`
- Suppose the *address* of `num` is `5000` and the *value* of `num` is `17`. This statement assigns the value `5000` to `iPtr`: `iPtr = &num;`
- Assuming `iPtr` is stored at location `800`, this can be pictured as follows:
```
ptr    800           num  5000
 ╭──────╮             ╭────╮
 │ 5000 │             │ 17 │
 ╰──────╯             ╰────╯
```
- We use `*iPtr` to refer to "the value pointed at by `iPtr`" (in effect, the value of `num`), and it can be used in the context that an integer can.
- Getting the value pointed to is called *dereferencing* the pointer.
- Example, the followin assigns the value `24 (17 + 7)` to `m`: `int m = *iPtr + 7;`
- Sometimes it use helpful to think of `*` and `&` as cancelling out each other.
- Example, if `iPtr = &num`, then the following holds: `*iPtr = *(&num) = num;`
- Example, `num = *iPtr + 1;` is equivalent to `num = num + 1;` and could be written as: `(*iPtr)++;`
### Character Pointers
- Supposed: `char word[20];`
- The array name `word` is a synonym for the address of its first element, `word[0]`.
- Thus: `word` ≡ `&word[0]`
- In effect, `word` "points to" the first character of the array and is, in fact, a pointer--a *character* pointer to be more percise.
- However, `word` is not a pointer variable but, a pointer *constant*.
- We cannot chage the value of `word`, which is the address of `word[0]`.
### Pointer Arighmetic
- Consider: `char *verse = "The day is done";`
- The string "The day is done" is stored somewhere in memory, and `verse` is assigned the address of the first character, `'T'`.
- In addition:
  - `verse + 1` is the address of `'h'`
  - `verse + 2` is the address of `'e'`
  - `verse + 3` is the address of `' '`
  - `verse + 4` is the address of `'d'`
  - etc
- Example, the following will print the characters of the string one per line:
``` C
while(*verse != '\0') {
    printf("%c\n", *verse++);
}
```
- Suppose a pointer, `p`, is declared to point at a type of value that occupies `k` bytes of storage.
- Incrementing `p` by `1` has the effect of adding `k` to the current value of `p` so that `p` now points to the *next* item of they type that `p` is declared to point at.
- Therefore, using pointer arithmetic, "adding `1`" means getting the address of the next item (no matter how many bytes away), and "adding `i`" means getting the address of the `i`th item beyond the current one.
- Thus, `p + i` is the address of the `i`th element beyond the one pointed to by `p`.
### Pointers to Structures
- To avoid overhead, the address of a structure could be passed: `fun7(&text);`
- In `fun7` the corresponding formal parameter must be declared appropriately, such as: `void fun7(Book *bp)`
- Pointers to structures occur so frequently in C that a special alternative notation is provided:
  - `sp -> name` refers to the `'name'` field
  - `sp -> age` refers to the `'age'` field
  - `sp -> gender` refers tot he `'gender'` field
- Summarization of common operations on structures:
  - A field can be accessed using the *structure member* (`.`) operator, as in `text.author`.
  - A structure variable can be assigned the value of another structure variable of the same type.
  - The *address-of* operator (`&`) can be applied to a structure name to give the address of the structure, e.g. `&text`. `&` can also be applied to an element of a structure. However, `&` must precede the structure name, not the field name. Example, `&text.price`.
  - If `p` is a pointer to a structure, the `*p` refers to the structure. Example, if `p` contains the address of the structure `text`, then `(*p).title` refers to the title field. However, the *structure pointer* (`->`) operator is more commonly used to refer to a field, as in: `p -> title`.
### Pointers to Functioons
- In the same way that an array name is the address of its first element. A function name is the address of the function.
- A function name is a *pointer* to the function in much the same way that an array name is a pointer to the array.
- A pointer to a function can be manipulated in much the same way was other pointers. It can be passed to functions.
- How to specify a function as a parameter?
  ``` C
  void makeTable(int first, int last, double (*fp) (int)) {
    for(int h = first; h <= last; h++) {
        printf("%2d %0.3f\n", h, (*fp)(h));
    }
  }
  ```
  - `makeTable` takes three arguments; the first two are integers and the third, `fp`, is a pointer to a function that takes an `int` argument and returns a `double` value.
  - The parenthese around `*fp` are necessary: `double (*fp) (int)`
  - If omitted: `double *fp (int)` would mean that `fp` is a function returning a pointer to a `double`.
  - In the `printf` statement, the function call `(*fp)(h)` is interpreted as follows:
    - `fp` is a pointer to a function; `*fp` *is* the function.
    - `h` is the actual argument to the function call; the brackets around `h` are the usual brackets around a function's arguemnt.
    - The value returned by the call should be a `double`, which would match the `%f` format specifier.
    - The brackets around `*fp` are necessary since `()` has higher precedence than `*`. Without them `*fp(h)` would be equivalent to `*(fp(h))`, which is meaningless in this context.
  - Example:
  ``` C
  #include <stdio.h>

  int main() {
    void makeTable(int, int, double (*fp) (int));
    double reciprocal(int);
    makeTable(1, 10, reciprocal);
  }

  void makeTable(int first, int last, double (*fp) (int)) {
    for(int h = first; h <= last; h++) {
        printf("%2d %0.3f\n", h, (*fp)(h));
    }
  }

  double reciprocal(int x) {
    return 1.0 / x;
  }
  ```
### Void Pointers
- C allows the declaration and use of `void` (also called *generic*) pointers--pointers that may point to any type of object. The following declares `pv` as a `void` pointer: `void *pv;`
- And the following declares `getNode` as a function that returns a `void` pointer: `void *getNode(int size);`
- Any valid address can be assigned to a `void` pointer. In particular, a pointer to `int` (or `double` or `float`, etc) can be assigned to a `void` pointer variable.
- Given the declaration: `double d, *dp;` you can write the following:
  ``` C
  dp = &d; //assign the address of d to dp
  pv = dp; //assign a double pointer to a void pointer variable
  ```
- Even though `pv` and `dp` now have the same pointer value it is invalid to think of `*pv` as a `double`.
- We should no attempt to dereference a `void` pointer.
- However, if we *known* that `pv` contains a `double` pointer, we can tell this to C using a *cast*, and dereference it: `* (double *) pv`
- Consider this:
  ``` C
  void dprint(double *p) {
    printf("%0.3f\n", *p);
  }
  ```
- The following would *not* work:
  ``` C
  void vprint(void *p) {
    printf("%0.3f\n", *p); // error: invalid use of void expression
  }
- However, assuming `t = 1` means an `int` and `t = 2` means a `double`:
  ``` C
  void print(void *p, int t) {
    if(t == 1) {
        printf("%d\n", *(int *) p);
    } else if(t == 2) {
        printf("%0.3f\n", *(double *) p);
    } else {
        printf(")
    }
  }
  ```
- C permits a `void` pointer to be assigned to any other type of pointer, as follows: `float *fp = pv;`
- It is up to you to make sure that assignment makes sense.
- *Note*: If you use a C++ compiler to compile you will get an error if you try to assign a `void` pointer to another pointer type. You will need to cast the `void` pointer to the correct type before assigning.
  - Example: `fp = (float *) pv;`
- Even though assigning a `void` pointer without casting is permitted in C, good practice dictates you should use a cast anyways.
## Linked Lists
### Define Linked List
- In many situations an array is used for representing a linear list.
- This data can also be represented using an organization in which each node in the list points *explicitly* to the next node. This organization is referred to as a *linked list*.
- In a (singly) linked list, each node contains a pointer that points to the next node in the list. You can think of each node as a cell with two components as this:
  ```
  ╭──────┬──────╮
  │ data │ next │
  ╰──────┴──────╯
  ```
- Where `data` can be one or more fields and `next` points to the next node of the list.
- Since the `next` field of the last node does not point to anything, we must set it to a special value called the *null pointer*.
  - In C, the null pointer value is denoted by the standard identifier: `NULL`, defined in `<stdlib.h>` and `<stdio.h>`.
- In addition to the cells of the list, we need a pointer variable (`top`) that points to the first item in the list.
- If the list is empty, the value of `top` is `NULL`.
- Pictorially, we represent a linked list as follows:
  ```
  top
  ╭─╮
  ╰┬╯   ╭──────┬──╮    ╭──────┬──╮    ╭──────┬──╮    ╭──────┬──╮
   ╰───>│      │ ─┼───>│      │ ─┼───>│      │ ─┼───>│      │ ─┼──╮
        ╰──────┴──╯    ╰──────┴──╯    ╰──────┴──╯    ╰──────┴──╯ ─┴─
                                                                 NULL
  ```
- You know where the first item is (`top`), which directs you to the second (`next`), which tells you where the thrid item is (`next`) and so on, until the last item, where `next` is a null pointer.
- The type of the `next` field within the structure is a pointer to the structure itself (*self-referencing structure*). Example:
  ``` C
  struct node {
    int num;
    struct node *next;
  };

  // or by using `typedef`:

  typedef struct node {
    int num;
    struct node *next;
  } Node, *NodePtr;



  Node *top;

  // or

  NodePtr top;
  ```
### Basic Operations on a Linked List
#### Count the Nodes in a Linked List
``` C
int length(NodePtr top) {
  int n = 0;
  NodePtr curr = top;
  while(curr != NULL) {
    n++;
    cur = curr -> next;
  }
  return n;
}
```
#### Search a Linked List
- Given the list:
  ```
  top
  ╭─╮
  ╰┬╯   ╭──────┬──╮    ╭──────┬──╮    ╭──────┬──╮    ╭──────┬──╮
   ╰───>│  36  │ ─┼───>│  15  │ ─┼───>│  52  │ ─┼───>│  23  │ ─┼──╮
        ╰──────┴──╯    ╰──────┴──╯    ╰──────┴──╯    ╰──────┴──╯ ─┴─
                                                                 NULL
  ```
  ``` C
  NodePtr search(NodePtr top, int key) {
    while(top != NULL && key != top -> num) {
      top = top -> next;
    }
    return top;
  }
  ```
#### Find the Last Node in a Linked List
``` C
NodePtr getLast(NodePtr top) {
  if(top == NULL) {
    return NULL;
  }
  while(top -> next != NULL) {
    top = top -> next;
  }
  return top;
}
```
### `malloc`, `calloc`, `sizeof`, `free`
- Consider the problem of reading positive integers (terminated by `0`) and build a linked list that contains the numbers in the order in which they were read. Example data: `36 15 52 23 0`
  ```
  top
  ╭─╮
  ╰┬╯   ╭──────┬──╮    ╭──────┬──╮    ╭──────┬──╮    ╭──────┬──╮
   ╰───>│  36  │ ─┼───>│  15  │ ─┼───>│  52  │ ─┼───>│  23  │ ─┼──╮
        ╰──────┴──╯    ╰──────┴──╯    ╰──────┴──╯    ╰──────┴──╯ ─┴─
                                                                 NULL
  ```
- With linked lists, whenever a new node must be added to the list, storage is allocated for the node (*dynamic storage allocation*). Unlike an array where the size of the array must be specified beforehand.
- Storage can be allocated dynamically by using the standard functions `malloc` and `calloc` (also `free`) the standard library header must be used: `<stdlib.h>`.
#### `malloc`
- The prototype for `malloc` is as follows, where `size_t` is an implementation-defined unsigned integer type defined in the standard header `<stddef.h>`: `void *malloc(size_t size);`
- `malloc` allocates `size` bytes of memory and returns a pointer to the first byte.
- The storage is *not* initialized.
- If `malloc` is unable to find the requested amount of storage, it returns `NULL`.
- In general, a pointer to one type may not be *directly* assigned to a pointer of another type; however, assignment is possible if an explicit cast is used. Example:
  ``` C
  int *ip;
  double *dp;

  ip = dp; // wrong
  ip = (int *) dp; // right
  ```
- On the other hand, values of type `void *` may be assigned to pointers of other types without using a cast.
- No cast is required to assign the `void *` returned by `malloc` to the character pointer `cp`.
- Even though this is premitted in C, good practice dictates that you should use a cast anyways: `char *cp = (char *) malloc(20);`
#### `calloc`
- The prototype for `calloc` is as follows:
  ``` C
  void *calloc(size_t num, size_t size);
  ```
- `calloc` allocates `num` * `size` bytes of memory and returns a pointer to the first byte.
- `calloc` allocates enough memory for an array of `num` objects each of size `size`.
- All bytes returned are initialized to `0`.
- If `calloc` is unable to find the requested amount of storage, it returns `NULL`.
- `calloc` is useful for alocating sorage for arrays.
#### `sizeof`
- `sizeof` is a standard unary operator that returns the number of bytes needed for storing its argument.
- The following returns the number of bytes needed for storing an `int` variable: `sizeof (int)`
- The argument to `sizeof` is either a type or a variable.
- If it is a type it must be enclosed in parentheses.
- If it is a type defined using `typedef`, the parentheses are optional.
- `sizeof` is used mainly for writing portable code, where the code depends on the number of bytes needed for storing various data types.
- Example:
  ``` C
  double *dp = (double *) malloc(sizeof (double));
  float  *fp = (float *) calloc(10, sizeof(float));
  Node   *np = malloc(sizeof(Node));
  ```
#### `free`
- `free` is used to free storage acquired by calls to `malloc` and `calloc`.
- The prototype is:
  ``` C
  void free(void *ptr);
  ```
- Even though `free` expects a `void` pointer, it is not necessary to explicitly cast `np` into a `void` pointer. It is perfectly acceptable to do so, but a bit cumbersome.
- It is a *fatal error* to attempt any of the following:
  - Free storage not obtained by a call to `malloc` or `calloc`.
  - Use a pointer to memory that has been freed.
  - Free pointers twice.
- **Note**: C knows how much memory to free because for each block allocated C keeps track of its size, usually by storing the size adjacent to the block.
### Build Linked List: Add Item at Tail
- When we read a new number, we must do the following:
  - Allocate storage for a node.
  - Put the number in the new node.
  - Make the new node the last one in the list.
- Assume the following declaration for defining a node:
  ``` C
  typedef struct node {
    int num;
    struct node *next;
  } Node, *NodePtr;
  ```
- Also, a function called `makeNode` that, given an integer argument, allocates storage for the node, store the integer in it, and returns a pointer to the new node.
- It will also set the `next` field to `NULL`.
  ``` C
  NodePtr makeNode(int n) {
    NodePtr np = (NodePtr) malloc(sizeof(Node));
    np -> num  = n;
    np -> next = NULL;
    return np;
  }
  ```
- Consider the call: `makeNode(36);`
  - First, storage for a new node is allocated. Assuming an `int` is 4 bytes and a pointer occupies 4 bytes, the size of `Node` is 8 bytes; so 8 bytes are allocated starting at address `4000`:
    ```
    4000
       ╭────┬──╮
       │    │  │
       ╰────┴──╯
    ```
  - `makeNode` then stores `36` in the `num` field and `NULL` in the `next` field, giving this:
    ```
    4000
       ╭────┬──╮
       │ 36 │  │
       ╰────┴──╯
    ```
  - The value `4000` is then returned by `makeNode`
  - When we read the first number, we must create a node for it and set `top` to point to the new ndoe.
  - For each subsequent number, we must set the `next` field of the current last node to point to the new node. The new node becomes the last node.
  - How to find the last node of a list?
    - One method is to start at the top of the list and follow the `next` pointers until we encounter `NULL`. Time-consuming.
    - A better approach is to keep a pointer (`last`) to the last node of the list. This pointer is updated as new nodes are added.
    - Example:
      ``` C
      np = makeNode(n);
      if(top == NULL) {
        top = np;
      } else {
        last -> next = np;
      }
      last = np;
      ```
### Insertion into a Linked List
- A list with one pointer in each node is called a *one-way*, or *singly linked*, list.
#### Doubly Linked List
  ``` C
  struct Node {
    int data;
    Node* next;
    Node* prev;
  };
  Node* head;
  Node* tail;
  ```
  ```
      ╭──────────────────────────╮
      │                    ╭─────┼────────────────────╮
      │         400        │     │   600              │   800
      │  ╭─────┬───┬─────╮ ╰─>╭──┼──┬───┬─────╮    ╭──┼──┬───┬─────╮
      ╰─>│  0  │ 1 │ 600 ┼───>│ 400 │ 5 │ 800 ┼───>│ 600 │ 7 │  0  │
      ╭─>╰─────┴───┴─────╯    ╰─────┴───┴─────╯ ╭─>╰─────┴───┴─────╯
      │   prev      data       prev      data   │   prev      data
      ╰───╮                                     │
  ╭──────╮│                                     │
  │ head ┼╯                                     │
  ╰──────╯                                      │
  ╭──────╮                                      │
  │ tail ┼──────────────────────────────────────╯
  ╰──────╯
  ```
### Build Linked List: Add Item at head
- Inserting incoming numbers at the tail of a list, this is an example of adding an item to a queue.
  - A *queue* is a linear list in which insertions occur at one end and deletions occur at the other end.
- Inserting incoming numbers at the head of a list, this is an example of adding an item to a stack.
  - A *stack* is a linear list in which insertions and eletions occur at the same end.
  - In stack terminology, when we add an item, we say the item is *pushed* into the stack.
  - Deleting an item from a stack is referred to as *popping* the stack.
### Deletion from a Linked List
- Deleting a node from the top of a linked list is accomplished by doing this: 
  ``` C
  old = top;   // save the pointer to the node to be deleted
  top = top -> next;   // set top to point to the 2nd node, if any
  free(old);   // free the space occupied by the first node
  ```
### Build a Sorted Linked List
- Each new number is compared with the numbers in the existing list. 
- As long as the new number is greater than a number in the list, we move down the list until the new number is smaller than, or equal to, an existing number or we come to the end of the list.
### Arrays vs. Linked Lists
- The big difference is that we have direct access to any element of an array, whereas to get any element of a linked list we have to traverse the list starting from the top.
- Maintaining an array in sorted order is cumbersome since each new item has to be inserted "in place", normally requireing other items to be moved.
- However, find the *location* in which to insert the itme can be done quickly using a binary search.
- Finding the *position* at which to insert a new item in a sorted linked list must be done using a sequential search.
- However, once the position is found, the item can be quickly inserted by seting/changing a couple of links.

| **Array** | **Linked List** |
| --- | --- |
| Direct Access to any element | Must traverse list to get to element |
| If unsorted, sequential search | If unsorted, sequential search |
| If sorted, binary search | If sorted, sequential search |
| Easy-to-insert item at the tail of the list | Easy to insert item anywhere in the list |
| Must move items to insert anywhere but the tail | Easy to insert item anywhere in the list |
| Deletion (except for the last one) requires items to be moved | Deletion of any item is easy |
| Need to move items when adding a new item to a sorted list | Adding a new item to a sorted linked list is easy |
| Can use binary search on a sorted list to find the position at which to insert new item | Must use sequential search to find the position at which to inwert new item in a sorted linked list |

### Circular and Two-Way Linked Lists
#### Circular Linked Lists
- There are two common variations of linked lists: *circular* lists and the *two-way* (or doubly linked) list.
- Circular lists are useful for representing situations that are circular.
  ```
  curr
     ╭─╮
     ╰┼╯     ╭─────┬──╮      ╭─────┬──╮      ╭─────┬──╮      ╭─────┬──╮
      ╰─────>│  1  │  ┼─────>│  2  │  ┼─────>│  3  │  ┼─────>│  4  │  ┼─────╮
             ╰─────┴──╯      ╰─────┴──╯      ╰─────┴──╯      ╰─────┴──╯     │
                ^                                                           │
                │                                                           │
                ╰───────────────────────────────────────────────────────────╯
  ```
#### Two-Way (Doubly Linked) Lists
- Each node will contain two pointers--one points to the next node, and the other points to the previous node.
- Advantages:
  - It is now possible to traverse the list in both directions, starting from either end. If required, reversing the list is now a simple operation.
  - It is now possible to move in either direction for the current position.
- Disadvantages:
  - More storage is required.
  - Adding and deleting nodes is more complicated since more pointer have to be set.
## Stacks and Queues
### Abstract Data Types
- An *abstract data type* is one that allows a user to manipulate the data type without any knowledge of how the data type is represented in  the computer.
### Stacks
- A *stack* is a linear list in which items are added at one end and deleted from the same end.  **last in, first out**
- Three operations: *push*, *pop*, and *empty*.
- Example:
  ``` C
  create an empty stack, S
  read(num)
  while(num != 0) {
    push num onto S
    read(num)
  }

  while(S is not empty) {
    pop S into num //store the number at the top of S in num
    print num
  }
  ```
#### Implement Stack Using an Array
``` C
typedef struct {
  int top;
  int ST[MaxStack];
} StackType, *Stack;

Stack S;

Stack initStack() {
  Stack sp = (Stack) malloc(sizeof(StackType));
  sp -> top = -1;
  return sp;
}

int empty(Stack S) {
  return (S -> top == -1);
}

void push(Stack S, int n) {
  if(S -> top == MaxStack - 1) {
    printf("\nStack Overflow\n");
    exit(1);
  }
  ++(S -> top);
  S -> ST[S -> top] = n;
}

int pop(Stack S) {
  if(empty(S)) {
    return RougeValue; //a symbolic constant
  }
  int hold = S -> ST[S -> top];
  --(S -> top);
  return hold;
}
```
```
  ╭───────╮
S │       │
  ╰───┼───╯
      │        ╭──────────────────────────────────────────╮
      ╰───────>│     ╭────╮                               │
               │ top │ -1 │                               │
               │     ╰────╯                               │
               │     ╭────┬────┬────┬────┬────┬────┬──    │
               │  ST │  ? │  ? │  ? │  ? │  ? │  ? │      │
               │     ╰────┴────┴────┴────┴────┴────┴──    │
               │        0    1    2    3    4    5        │
               ╰──────────────────────────────────────────╯
  ╭───────╮
S │       │
  ╰───┼───╯
      │        ╭──────────────────────────────────────────╮
      ╰───────>│     ╭────╮                               │
               │ top │  3 │                               │
               │     ╰────╯                               │
               │     ╭────┬────┬────┬────┬────┬────┬──    │
               │  ST │ 36 │ 15 │ 52 │ 23 │    │    │      │
               │     ╰────┴────┴────┴────┴────┴────┴──    │
               │        0    1    2    3    4    5        │
               ╰──────────────────────────────────────────╯
```
### Queues
-  A *queue* is a linear list in which items are added at one end and delted from the other end.
- Basic operations:
  - Add an item to the queue; *enqueue*
  - Take an item off the queue; *dequeue*
  - Check whether the queue is empty.
  - Inspect the item at the head of the queue.
#### Implement Queue Using an Array
``` C
typedef struct {
  int head, tail;
  int QA[MaxQ];
} QType, *Queue;

Queue Q;

Queue initQueue() {
  Queue qp = (Queue) malloc(sizeof(QType));
  qp -> head = qp -> tail = 0;
  return qp;
}

Queue Q = initQueue();
```
```
  ╭───────╮
Q │       │
  ╰───┼───╯
      │        ╭──────────────────────────────────────────╮
      ╰───────>│      ╭────╮              ╭────╮          │
               │ head │  0 │         tail │  0 │          │
               │      ╰────╯              ╰────╯          │
               │     ╭────┬────┬────┬────┬────┬────┬──    │
               │  QA │    │    │    │    │    │    │      │
               │     ╰────┴────┴────┴────┴────┴────┴──    │
               │        0    1    2    3    4    5        │
               ╰──────────────────────────────────────────╯
  ╭───────╮
Q │       │
  ╰───┼───╯
      │        ╭──────────────────────────────────────────╮
      ╰───────>│      ╭────╮              ╭────╮          │
               │ head │  0 │         tail │  3 │          │
               │      ╰────╯              ╰────╯          │
               │     ╭────┬────┬────┬────┬────┬────┬──    │
               │  QA │    │ 36 │ 15 │ 52 │    │    │      │
               │     ╰────┴────┴────┴────┴────┴────┴──    │
               │        0    1    2    3    4    5        │
               ╰──────────────────────────────────────────╯
  ╭───────╮
Q │       │
  ╰───┼───╯
      │        ╭──────────────────────────────────────────╮
      ╰───────>│      ╭────╮              ╭────╮          │
               │ head │  1 │         tail │  4 │          │
               │      ╰────╯              ╰────╯          │
               │     ╭────┬────┬────┬────┬────┬────┬──    │
               │  QA │    │ 36 │ 15 │ 52 │ 23 │    │      │
               │     ╰────┴────┴────┴────┴────┴────┴──    │
               │        0    1    2    3    4    5        │
               ╰──────────────────────────────────────────╯

```
## Binary Trees
### Trees
- A *tree* is a finite set of nodes such that:
  - There is one specially designated node called the *root* of the tree.
  - The remaining nodes are partitioned into m ≥ 0 disjoint sets T₁, T₂, ..., Tₘ, and each of these is a tree.
- The trees T₁, T₂, ..., Tₘ, are called *subtrees* of the root.
```
            ╭───╮
      ╭─────┼ A ┼─────╮
      │     ╰─┼─╯     │
    ╭─┼─╮   ╭─┼─╮   ╭─┼─╮
  ╭─┼ B ┼─╮ │ C │   │ D │
  │ ╰───╯ │ ╰───╯   ╰─┼─╯
╭─┼─╮   ╭─┼─╮       ╭─┼─╮
│ E │   │ F │   ╭───┼ G ┼───╮
╰───╯   ╰───╯   │   ╰─┼─╯   │
              ╭─┼─╮ ╭─┼─╮ ╭─┼─╮
              │ H │ │ I │ │ J │
              ╰───╯ ╰───╯ ╰───╯
```
- The *degree* of a node is the number of subtrees of the node.
  - Think of it as the number of lines leaving the node.
  - For example, degree(A) = 3, degree(C) = 0, degree(D) = 1, and degree(G) = 3.
- We use the terms *parent*, *child*, and *sibling* to refer to the nodes of a tree.
  - The parent A has three children (B, C, D).
  - The parent B has two children (E, F).
  - The parent D has one child (G), which has three children (H, I, J).
  - B, C, and D are siblings.
  - E and F are siblings.
  - H, I, and J are siblings.
- A node may have several children but, except for the root, only one parent.
- The root has no parent.
- A nonroot node has exactly one line leading *into* it.
- A *terminal* node (also called a *leaf*) is a node of degree 0.
- A *branch* node is a nonterminal node.
- C, E, F, H, I, and J are leaves.
- A, B, D, and G are branch nodes.
- The *moment* of a tree is the number of nodes in the tree.
  - The above example has moment 10.
- The *weight* of a tree is the number of leaves in the tree.
  - The above example has weight 6.
- The *level* (or *depth*) of a node is the number of branches that must be traversed on the path to then node from the  root.
  - The root has level 0.
- The *height* of a tree is the number of levels in the tree.
  - The above example has height 4.
- If the relative order of the subtrees is important, the tree is an *ordered* tree.
- If order is unimportant, the tree is *oriented*.
- A *forest* is a set of zero or more disjoint trees.
### Binary Trees
- A *binary tree* is a nonlinear data structure.
- A binary tree is a special case of the more general *tree* data structure, but it is the most useful and  most widely used kind of tree.
- A **binary tree** 
  - (a) is empty
  - Or
  - (b) consists of a root and two subtrees--a left and right--with each subtree being a binary tree.
- A node always has two subtrees, any of which may be empty.
- Another consequence is that if a node has *one* nonempty subtree, it is important to distinguish whether it is on the left or right.  For example:
  ```
       ╭───╮                                 ╭───╮  
    ╭──┼ A │ is a different binary tree from │ A ┼──╮
    │  ╰───╯                                 ╰───╯  │
    │                                               │
    │                                               │
    │                                               │
  ╭─┼─╮                                           ╭─┼─╮  
  │ B │                                           │ B │
  ╰───╯                                           ╰───╯
  ```
- The first has an empty right subtree, while the second has an empty left subtree.
- However, as *trees*, they are the same.
### Traverse a Bineary  Tree
- For a tree of *n* nodes, there are *n!* ways to visit them, assuming each node is visited once.
- **Pre-order traversal**:
  - Visit the root
  - Traverse the left subtree in pre-order
  - Traverse the right subtree in pre-order
- **In-order traversal**
  - Traverse the left subtree in in-order
  - Visit the root
  - Traverse the right subtree in in-order
- **Post-order traversal**
  - Traverse the left subtree in post-order
  - Traverse the right subtree in post-order
  - Visit the root
### Represent a Binary Tree
- At a minimum, each node of a binary tree consists of three fields:
  - A field containing the data at the node.
  - A pointer to the left subtree.
  - A pointer to the right subtree.
  - Example:
    ``` C
    typedef struct treenode {
      NodeData data;
      struct treenode *left, *right;
    } TreeNode, *TreeNodePtr;

    typedef struct {
      int num;
    } NodeData;

    typedef struct {
      char word[MaxWordSize+1];
      int freq;
    } NodeData;
    
    typedef struct {
      TreeNodePtr root;
    } BinaryTree;
    ```
### Binary Search Trees
```
               ╭─────╮
        ╭──────┼ ode ┼──────╮
        │      ╰─────╯      │
     ╭──┼──╮             ╭──┼──╮
   ╭─┼ lea ┼─╮         ╭─┼ tee ┼─╮
   │ ╰─────╯ │         │ ╰─────╯ │
╭──┼──╮   ╭──┼──╮   ╭──┼──╮   ╭──┼──╮
│ era │   │ mac │   │ ria │   │ vim │
╰─────╯   ╰─────╯   ╰─────╯   ╰─────╯
```
- This is a special kind of binary tree.
- It has the  property that, given *any* node, a word in the left subtree is "smaller" and a word in  the right subtree is "greater" than the word at the node.
- Such a tree is called a *binary search tree* (BST).
- Example: Search for `fun`
  - `fun` is smaller than `ode`, so go left
  - `fun` is smaller than `lea`, so go left
  - `fun` is greater than `era`, so go right
  - subtree of `era` is empty so add `fun`
### Non-Recursive Traversals
- **Algorithm for in-order traversal**
  ``` C
  initalize a stack S to empty
  curr = root
  finished = false
  whiel(not finished) {
    while(curr != null) {
      push curr onto S
      curr = left(curr)
    }
    if(S is empty) {
      finished = true
    } else {
      pop S into curr
      visit curr
      curr = right(curr)
    }
  }
  ```
- **Algorithm for post-order traversal**
  ``` C
  initialize a stack S to empty
  curr = root
  finished = false
  while(not finished) {
    while(curr != null) {
      push(cur, 0) onto S
      curr = left(curr)
    }
    if(S is empty) {
      finished = true
    } else {
      pop S into (temp, t)
      if(t == 1) {
        visit temp
      } else {
        push(temp, 1) onto S
        curr = right(temp)
      }
    }
  }
  ```
### Level-Order Traversal
- Another useful order is *level-order*.
- Traverse the tree level by leve, starting at the root.
- At each level, traverse the nodes from left to right
### Binary Search Tree Deletion
- The problem of deleting a node from a binary search tree (BST) so that it remains a BST.
- Consider:
  - The node is a leaf
  - The node has no left subtree
  - The node has no right subtree
  - The node has non-empty left and right subtrees
- Case 1 (leaf)
  - Delete node and point parent to null
- Case 2 (no left subtree)
  - Replace node by child
- Case 3 (...)
## Sorting
### Sorting
- *Sorting* is the process by which a set of values are arranged in ascending or descending order.
### Selection Sort
- Consider the following array:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 57 │ 48 │ 79 │ 65 │ 15 │ 33 │ 52 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- Sorting `num` in ascending order using selection sort proceeds as follows:
- **1st pass**
  - Find the smallest number in positions `0` to `6`; the smallest is `15`, found in position `4`
  - Interchange the numbers in positions `0` and `4`; we get:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 15 │ 48 │ 79 │ 65 │ 57 │ 33 │ 52 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- **2nd pass**
  - Find the smallest number in positions ``1` to `6`; the smallest is `33`, found in position `5`
  - Interchange the numbers in positions `1` adn `5`; we get:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 15 │ 33 │ 79 │ 65 │ 57 │ 48 │ 52 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- **3rd pass**
  - Find the smallest number in positions `2` to `6`; the smallest is `48`, found in position `5`
  - Interchange the numbers in positions `2` and `5`; we get:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 15 │ 33 │ 48 │ 65 │ 57 │ 79 │ 52 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- **4th pass**
  - Find the smallest number in positions `3` to `6`; the smallest is `52`, found in position `6`
  - Interchange the numbers in positions `3`  and `6`; we get:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 15 │ 33 │ 48 │ 52 │ 57 │ 79 │ 65 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- **5th pass**
  - Find the smallest number in positions `4` to `6`; the smallest is `57`, found in position `4`
  - Interchange the numbers in positions `4` and `4`; we get:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 15 │ 33 │ 48 │ 52 │ 57 │ 79 │ 65 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- **6th pass**
  - Find the smallest number in positions `5` to `6`; the smallest is `65`, found in position `6`
  - Interchange the numbers in positions `5` and `6`; we get:
  ```
  ╭────┬────┬────┬────┬────┬────┬────╮
  │ 15 │ 33 │ 48 │ 52 │ 57 │ 65 │ 79 │
  ╰────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6
  ```
- and the array is now sorted.
- outline of the algorithm:
  ```
  for h = 0 to n - 2
    s = position of smallest number from num[h] to num[n-1]
    swap num[h] and num[s]
  endfor
  ```
### Insertion Sort
- Pickup `57`, consider one number to be sorted
- Pickup `48`, add it to the front
- Pickup `79`, add it after `57`
- So on...
### Heapsort
- *Heapsort* is a method of sorting that *interprets* the elements in an array as an almost complete binary tree.
- *A **heap** is an almost complete binary tree such that the value at the root is greater than or equal to the values at the left and right children, and the left and right subtrees are also heaps.*
- An immediate consequence of this definition is that the largest value is at the root.
- Such a heap is referred to as a *max-heap*.
- We define a *min-heap* with the word *greater* replaced by *smaller*.
- In a min-heap, the *smallest* value is at the root.
### Heaps and Priority Queues
- A *priority queue* is one in which each item is assigned some "priority" and its position in the queue is based on this priority.
- The item with top priority is placed at the head of the q ueue.
- The following are some typical operations that may be performed on a priority queue:
  - Remove (serve) the item with the highest priority
  - Add an item with a given priority
  - Remove (delete without serving) an item from the queue
  - Change the priority of an item, adjusting its position based onn its new priority
### Quicksort
- At the heart of quicksort is the notion of *partitioning* the list with respect to one of the values called a *pivot*.
- Given the following list of *n* elements (*n* = 10) to be sorted:
  ```
  num
  ╭────┬────┬────┬────┬────┬────┬────┬────┬────┬────╮
  │ 53 │ 12 │ 98 │ 63 │ 18 │ 32 │ 80 │ 46 │ 72 │ 21 │
  ╰────┴────┴────┴────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6    7    8    9
  ```
- We can *partition* it with respect to the first value, `53`. Placing `53` in such a position that all *values to the left of it are smaller* and all *values to the right are greater* than or equal to it.
  ```
  num
  ╭────┬────┬────┬────┬────┬────┬────┬────┬────┬────╮
  │ 21 │ 12 │ 18 │ 32 │ 46 │ 53 │ 80 │ 98 │ 72 │ 63 │
  ╰────┴────┴────┴────┴────┴────┴────┴────┴────┴────╯
     0    1    2    3    4    5    6    7    8    9
  ```
- The value `53` is used as the *pivot*.
- It is placed in position `5`.
- All values to the left of `53` are smaller.
- All values to the right of `53` are greater.
- The location in which the pivot is placed is called the *division point* (`dp`).
- By definition, `53` is in its final sorted position.
- If we can sort `num[0..dp-1]` and `num[dp+1..n-1]`, we would have sorted the entire list.
``` C
void quicksort(int A[], int lo, int hi) {
  int partition(int[], int, int);
  if(lo < hi) {
    int dp = partition(A, lo, hi);
    quicksort(A, lo, dp-1);
    quicksort(A, dp+1, hi);
  }
}
```
#### Another Way to Partition
- We choose `53` as the pivot.
- The general idea is to *scan from the right* looking for a key that is smaller than, or equal to, the pivot.
- We then *scan from the left* for a key that is greater than, or equal to, the pivot.
- We swap these two values; this process effectively puts smaller values to the left and bigger values to the right.
### Mergesort
- 



╭─┬─╮
├─┼─┤
│ │ │
╰─┴─╯
 ╳
╱ ╲
╲ ╱
 ╳
╱ ╲

```


