# Special Relativity
## Time Dilation:
```math
t^\prime = t\sqrt{1 - \frac{v^2}{c^2}}
```
## Length Contraction:
```math
L = L_0\sqrt{1 - \frac{v^2}{c^2}},\quad\quad\quad\quad\gamma = \frac{1}{\sqrt{\frac{1 - v^2}{c^2}}}
```
## Lorentz Transformation:
```math
\begin{align*}
&x^\prime = \gamma(x - Vt)\\
&y^\prime = y\\
&z^\prime = z\\
&t^\prime = \gamma\left(t - \frac{Vx}{c^2}\right)
\end{align*}
```
## Velocity Transformation:
```math
V^\prime_x = \frac{V_x - v}{1 - \frac{vV_x}{c^2}}
```
## Relativistic Momentum:
```math
\vec{P} = \frac{m\vec{v}}{\sqrt{1 - \frac{v^2}{c^2}}}
```
## Relativistic Force:
```math
\vec{F} = \frac{d\vec{P}}{dt} = \frac{d}{dt}\left(\frac{m\vec{v}}{\sqrt{1 - \frac{v^2}{c^2}}}\right)
```
## Relativistic Energy:
```math
E_{\text{tot}} = \frac{mc^2}{\sqrt{1 - \frac{v^2}{c^2}}}
```
## Mass Energy:
```math
E_{\text{mass}} = mc^2
```
## Relativistic Translational Kinetic Energy:
```math
E_{\text{trans}} = \frac{mc^2}{\sqrt{1 - \frac{v^2}{c^2}}} - mc^2
```