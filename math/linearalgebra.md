# Linear Algebra
## Matrices
### Matrix Inverse
```math
\mathbf{A}=\left(\begin{array}{ccc}
a & b\\
c & d
\end{array}\right) \Rightarrow \mathbf{A}^{-1} = \frac{1}{\det \mathbf{A}}\left(
\begin{array}{ccc}
d & -b\\
-c & a
\end{array}\right) \iff \det \mathbf{A} \neq 0
```
### Determinant
```math
\det\left(\begin{array}{ccc}
a & b\\
c & d
\end{array}\right) = ad - bc
```
### Trace
```math
\mathrm{tr}\left(\begin{array}{ccc}
a & b & c\\
d & e & f\\
g & h & i
\end{array}\right) = a + e + i
```
### Matrix Multiplication
```math
\left(\begin{array}{ccc}
a & b\\
c & d
\end{array}\right)
\left(\begin{array}{ccc}
e & f\\
g & h
\end{array}\right) = \left(\begin{array}{ccc}
ae + bg & af + bh\\
ce+dg & cf+dh
\end{array}\right)
```
### Matrix Row Operations
#### Swap any two rows
```math
\left(\begin{array}{ccc}
\alpha & \beta\\
\gamma & \delta
\end{array}\right) \xrightarrow{R_1 \leftrightarrow R_2} \left(\begin{array}{ccc}
\gamma & \delta\\
\alpha & \beta
\end{array}\right)
```
#### Multiply row by a nonzero constant
```math
\left(\begin{array}{ccc}
\alpha & \beta\\
\gamma & \delta
\end{array}\right) \xrightarrow{3 \cdot R_1 \rightarrow R_1} \left(\begin{array}{ccc}
3\cdot\alpha & 3\cdot\beta\\
\gamma & \delta
\end{array}\right)
```
#### Add one row to another
```math
\left(\begin{array}{ccc}
\alpha & \beta\\
\gamma & \delta
\end{array}\right) \xrightarrow{R_2 + R_1 \rightarrow R_2}\left(\begin{array}{ccc}
\alpha & \beta\\
\gamma + \alpha & \delta + \beta
\end{array}\right)
```
## Miscellaneous
### Integration by Parts:
```math
\int u\ dv = uv - \int v\ du\\
```
### Line Integral
```math
dS = \sqrt{\left(\frac{dx}{dt}\right)^2 + \left(\frac{dy}{dt}\right)^2}\ \ dt
```
### Euler's Formula
```math
\begin{align*}

e^{i\theta} = \cos(\theta) &+ i\sin(\theta)\\[25pt]

\cos(\theta) = \frac{1}{2}\left(e^{i\theta}+e^{-i\theta}\right)\quad\quad&\quad\quad\sin(\theta) = \frac{1}{2}\left(e^{i\theta}-e^{-i\theta}\right)

\end{align*}
```
## Vector Operations
### Eigenvectors & Eigenvalues
```math
\begin{align*}

\mathbf{A}\vec{\mathbf{x}} &= \lambda \vec{\mathbf{x}} \implies (\mathbf{A} - \lambda\mathbf{I})\vec{\mathbf{x}} = \vec{\mathbf{0}}\\[25pt]

\lambda &= \det(\mathbf{A} - \lambda\mathbf{I}) = 0

\end{align*}
```
### Dot Product (Scalar Product)
```math
\begin{align*}

{\vec{\mathbf\alpha}}\cdot\vec{\mathbf{\beta}} = ||\vec{\mathbf{\alpha}}|| &\cdot ||\vec{\mathbf{\beta}}||\cos\theta\\[25pt]

\vec{\mathbf{\alpha}} = (\alpha_1,\alpha_2,\alpha_3), \vec{\mathbf{\beta}} = (\beta_1,\beta_2,\beta_3) &\implies \vec{\mathbf{\alpha}}\cdot\vec{\mathbf{\beta}} = \alpha_1\beta_1 + \alpha_2\beta_2 +\alpha_3\beta_3\\

\end{align*}
```
### Cross Product (Vector Product)
```math
\begin{align*}

\vec{\mathbf{\alpha}}\times\vec{\mathbf{\beta}} &= ||\vec{\mathbf{\alpha}}|| \cdot ||\vec{\mathbf{\beta}}||\sin\theta\hat{\mathbf{n}}\\[25pt]

\vec{\mathbf{\alpha}} &=\left(\begin{array}{ccc}
\alpha_1\\
\alpha_2\\
\alpha_3
\end{array}\right),\vec{\mathbf{\beta}} = \left(\begin{array}{ccc}
\beta_1\\
\beta_2\\
\beta_3
\end{array}\right) \implies \vec{\mathbf{\alpha}}\times\vec{\mathbf{\beta}} = \left|\begin{array}{ccc}
\hat{\mathbf{i}} & \hat{\mathbf{j}} & \hat{\mathbf{k}}\\
\alpha_1 & \alpha_2 & \alpha_3\\
\beta_1 & \beta_2 & \beta_3
\end{array}\right|\\[25pt]
&= \hat{\mathbf{i}}(\alpha_2\beta_3 - \alpha_3\beta_2) - \hat{\mathbf{j}}(\alpha_1\beta_3 - \alpha_3\beta_1)+\hat{\mathbf{k}}(\alpha_1\beta_2 - \alpha_2\beta_1)

\end{align*}
```
### Divergence
```math

\mathrm{div}(\vec{\mathbf{F}}) = \vec{\nabla} \cdot \vec{\mathbf{F}} = \frac{\partial F_x}{\partial x} + \frac{\partial F_y}{\partial y}+\frac{\partial F_z}{\partial z}

```
### Curl
```math
\begin{align*}

\mathrm{curl}(\vec{\mathbf{F}}) = \vec{\nabla} \times \vec{\mathbf{F}} =\left( \frac{\partial}{\partial x}\mathbf{\hat i} + \frac{\partial}{\partial y}\mathbf{\hat j} + \frac{\partial}{\partial z}\mathbf{\hat k}\right) \times \left( F_x \mathbf{\hat i} + F_y \mathbf{\hat j} + F_z \mathbf{\hat k}\right)\\[25pt]

\vec{\nabla} \times \vec{\mathbf{F}} = \left| \begin{array}{ccc}
\mathbf{\hat i} & \mathbf{\hat j} & \mathbf{\hat k}\\
\frac{\partial}{\partial x} & \frac{\partial}{\partial y} & \frac{\partial}{\partial z}\\
F_x & F_y & F_z
\end{array}
\right| =\left( \frac{\partial F_z}{\partial y} - \frac{\partial F_y}{\partial z}\right)\mathbf{\hat i} + \left( \frac{\partial F_x}{\partial z} - \frac{\partial F_z}{\partial x}\right)\mathbf{\hat j}\ + \left( \frac{\partial F_y}{\partial x} - \frac{\partial F_x}{\partial y}\right)\mathbf{\hat k}

\end{align*}
```
### Gradient
```math
\vec{\nabla} = \frac{\partial}{\partial x}\mathbf{\hat i} + \frac{\partial}{\partial y}\mathbf{\hat j}+\frac{\partial}{\partial z}\mathbf{\hat k}
```
### Laplacian
```math
\nabla^2f(x,y,z) = \frac{\partial^2f}{\partial x^2} + \frac{\partial^2 f}{\partial y^2} + \frac{\partial^2 f}{\partial z^2}
```
### Laplacian (re-written)
```math
\vec{\nabla} \cdot \vec{\nabla} f \Rightarrow \mathrm{Divergence\ of\ the\ Gradient\ of\ }f
```
### Triple Product
```math
\vec{A} \cdot (\vec{B} \times \vec{C}) = \vec{B} \cdot (\vec{C} \times \vec{A}) = \vec{C} \cdot (\vec{A} \times \vec{B})\\[25pt]

\vec{A} \times (\vec{B} \times \vec{C}) = \vec{B}(\vec{A} \cdot \vec{C}) - \vec{C}(\vec{A} \cdot \vec{B})
```
### Product Rules
```math
\nabla (fg) = f(\nabla g) + g(\nabla f)\\[25pt]
\nabla (\vec{A} \cdot \vec{B}) = \vec{A} \times (\nabla \times \vec{B}) + \vec{B} \times(\nabla \times \vec{A}) + (\vec{A} \cdot \nabla)\vec{B} + (\vec{B} \cdot \nabla)\vec{A}\\[25pt]
\nabla \cdot (f\vec{A}) = f(\nabla \cdot \vec{A}) + \vec{A} \cdot (\nabla f)\\[25pt]
\nabla \cdot (\vec{A} \times \vec{B}) = \vec{B} \cdot (\nabla \times \vec{A}) - \vec{A} \cdot (\nabla \times \vec{B})\\[25pt]
\nabla \times (f\vec{A}) = f(\nabla \times \vec{A})- \vec{A} \times (\nabla f)\\[25pt]
\nabla \times (\vec{A} \times \vec{B}) = (\vec{B} \cdot \nabla)\vec{A} - (\vec{A} \cdot \nabla)\vec{B} + \vec{A}(\nabla \cdot \vec{B}) - \vec{B}(\nabla \cdot \vec{A})\\[25pt]
```
### Second Derivatives
```math
\nabla \cdot (\nabla \times \vec{A}) = 0\\[25pt]
\nabla \times (\nabla f) = 0\\[25pt]
\nabla \times (\nabla \times \vec{A}) = \nabla (\nabla \cdot \vec{A}) - \nabla^2\vec{A}\\[25pt]
```
### Fundamental Theorems
#### Gradient Theorem
```math
\int^{\vec{b}}_{\vec{a}}(\nabla f) \cdot d\vec{l} = f(\vec{b}) - f(\vec{a})
```
#### Divergence Theorem
```math
\int(\nabla \cdot \vec{A}) d \tau = \oint \vec{A} \cdot d\vec{a}
```
#### Curl Theorem
```math
\int(\nabla \times \vec{A})\cdot d\vec{a} = \oint \vec{A} \cdot d\vec{l}
```
## Tensors
### Convention
```math
\begin{align*}

y_i &= c_i^r a_{rs} x_s,\ \ \ \ \ n = 2\\
y_1 &= c_1^1a_{11}x_1 + c^2_1a_{21}x_1 + c_1^1 a_{12}x_2 + c^2_1 a_{22} x_2,\\
y_2 &= c_2^1 a_{11}x_1 + c_2^2 a_{21}x_1 + c_2^1 a_{12} x_2 + c_2^2 a_{22} x_2\\[15pt]

&\text{Any free index (}i\text{) in an expression shal have the same range as summationindices }(r, s)\text{, unless}\\
&\text{stated otherwise}\\[35pt]
&\text{No index may occur \textbf{more than twice} in any given expression}\\
\end{align*}
```
### Kronecker Delta
```math
\begin{align*}
\delta_{ij} \equiv \delta_j^i &\equiv \delta^{ij} \equiv \left \{ \begin{array}{ccc}
1\ \ \ i = j \\
0\ \ \ i \neq j
\end{array}\right]\\
\mathbf{I} = \delta_{ij} &= \delta_{ji}\ \forall\ i,j\\
\end{align*}\\[15pt]

\text{In general, }\delta_{ij} x_i x_j = x_i x_i\text{  and  }\delta_j^r a_{ir} x_i = a_{ij} x_i
```
### Identities
```math
\begin{align*}
a_{ij}(x_j + y_j) &\equiv a_{ij} x_j + a_{ij} y_j\\
a_{ij}x_i y_j & \equiv a_{ij} y_j x_i\\
a_{ij} x_i x_j &\equiv a_{ji} x_i x_j\\
(a_{ij} + a_{ji}) x_i x_j &\equiv 2a_{ij} x_i x_j\\
(a_{ij} - a_{ji}) x_i x_j &\equiv 0\\
\end{align*}
```
### Non-Identities
```math
\begin{align*}
a_{ij}x_j &\neq a_{kj}x_j\\
a_{ij}(x_i + y_j) &\not \equiv a_{ij}x_i + a_{ij}y_j\\
a_{ij}x_iy_j &\not \equiv a_{ij} y_i x_j\\
(a_{ij} + a_{ji})x_i y_j & \not \equiv 2a_{ij}x_i y_j\\
\end{align*}
```
### Matrices (Tensor Notation)
```math
\begin{align*}
[a^i_j]_{mn} \equiv \left [ \begin{array}{cccc}
a^1_1 & a_2^1 & \cdots & a^1_n\\
a^2_1 & a_2^2 & \cdots & a^2_n\\
\vdots &\vdots &\ddots &\vdots\\
a^m_1 & a^m_2 & \cdots & a^m_n
\end{array}\right ]\ \ \ &\ \ \ [y^{pq}]_{42} \equiv \left [ \begin{array}{cc}
y^{11} & y^{12}\\
y^{21} & y^{22}\\
y^{31} & y^{32}\\
y^{41} & y^{42}
\end{array}\right ]\\
\end{align*}
```
#### Vector
```math
\vec{v} = (x_i) \equiv (x_{i1})
```
#### Scalar Multiplication
```math
\lambda[a_{ij}]_{mn} \equiv [\lambda a_{ij}]_{mn}
```
#### Matrix Multiplication
```math
\begin{align*}
\mathbf{AB} &\equiv [a_{ij}]_{mn}[b_{ij}]_{nk} = [a_{ir} b_{rj} ]_{mk},\\
\mathbf{AB} &\equiv [a^i_j]_{mn} [b^i_j]_{nk} = [a^i_r b^r_j]_{mk},\\
\mathbf{AB} &\equiv [a^{ij}]_{mn} [b^{ij}]_{nk} = [a^{ir} b^{rj}]_{mk}
\end{align*}
```
#### Matrix Inverse
```math
\begin{align*}
\mathbf{AB} = \mathbf{BA} = \mathbf{I} \implies a_{ir} b_{rj} = b_{ir} a_{rj} = \delta_{ij},\\
a_{r}^i b_{j}^r = b_{r}^i a_{j}^r = \delta_{j}^i, a^{ir} b^{rj} = b^{ir} a^{rj} = \delta^{ij}\\
\end{align*}
```
