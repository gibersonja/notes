# Dynamics
## Determining Velocity and Acceleration from Position
### Determining velocity and acceleration from position:
```math
\begin{align*}

&\text{If a particle moves along a straight line, with position }x\text{, velocity }v\text{, and acceleration }a\text{, at time }t\text{, then:}\\[25pt]

\end{align*}
```
```math
v = \frac{dx}{dt}\quad\quad\text{and}\quad\quad a = \frac{dv}{dt} = \frac{d^2x}{dt^2}\\[25pt]

```
### Determining Position from Velocity, and Velocity from Acceleration:
```math
\begin{align*}

&\text{If a particle moves along a straight line, with position }x\text{, velocity }v\text{, and acceleration }a\text{, at time }t\text{, then:}\\[25pt]

\end{align*}
```
```math
x = \int vdt\ \ \ \mathrm{and}\ \ \ v = \int adt
```
### Equations of Motion for Constant Acceleration along a Straight Line:
```math
\begin{align*}
&\text{If a particle is moving along a straight line with constant acceleration }a\text{, with position }x\text{, and velocity }v\text{, at time }\\
&t\text{, and with }x = 0\text{, and }v = v_0\text{ at time }t = 0\text{, then the following equations hold:}\\[25pt]

\end{align*}
```
```math
\begin{align*}
x=v_0t+\frac{1}{2}at^2\ \ \ x = \frac{1}{2}&\left(v_0 + v \right)t\ \ \ x = vt - \frac{1}{2}at^2\\
v=v_0+at\ \ \ &\ \ \ v^2=v_0^2+2ax\\
\end{align*}
```
### Alternate Relationship between Velocity and Acceleration:
```math
\begin{align*}

&\text{If a particle moves along a straight line with velocity }v\text{ and acceleration }a\text{, at position }x\text{, then:}\\[25pt]

\end{align*}
```
```math
a=v\frac{dv}{dx}
```
### Newton's Second Law of Motion:
```math
\mathbf{\vec{F}}=m\mathbf{\vec{a}}
```
### Sliding Friction:
```math
\begin{align*}

&\text{If an object is moving across a flat rough surface, then the friction force }\vec{\textbf{F}}\text{ on the object acts parallel to the surface,}\\
&\text{in the opposite direction to the motion.  It's magnitude is given by: }|\vec{\textbf{F}} = \mu|\vec{\textbf{N}}\text{, where }\mu\text{ is the coefficient of}\\
&\text{sliding friction for the object and the surface involved, and }\vec{\textbf{N}}\text{ is the normal reaction of the surface on the object.}\\[45pt]
\end{align*}
```
### Determining Velocity from Position:
```math
\begin{align*}


&\text{If the position of a particle is given by:}\\[25pt]

\end{align*}
```
```math
\vec{\mathbf{r}} = x \mathbf{\hat i} + y \mathbf{\hat j} + z \mathbf{\hat k}
```
```math
\text{Then its velocity is given by:}\\
```
```math
\mathbf{\vec{v}}= \frac{d\vec{\mathbf{r}}}{dt} = \frac{dx}{dt}\mathbf{\hat i} + \frac{dy}{dt}\mathbf{\hat j} + \frac{dz}{dt}\mathbf{\hat k}\\
```
### Determining Position from Velocity:
```math
\begin{align*}

&\text{If the velocity of a particle is given by:}\\
\end{align*}
```
```math
\vec{\mathbf{v}} = v_x\mathbf{\hat i} + v_y\mathbf{\hat j} + v_z\mathbf{\hat k}
```
```math
\begin{align*}
\text{Then its position is given by:}\\
\end{align*}\\
\vec{\mathbf{r}} = \int \vec{\mathbf{v}}dt = \left(\int v_xdt\right)\mathbf{\hat i} + \left(\int v_ydt\right)\mathbf{\hat j} + \left(\int v_zdt\right)\mathbf{\hat k}
```
### Determining Acceleration from Velocity or Position:
```math
\begin{align*}

&\text{If the velocity of a particle is given by:}\\

\end{align*}
```
```math
\vec{\mathbf{v}} = v_x\mathbf{\hat i} + v_y\mathbf{\hat j} + v_z\mathbf{\hat k}
```
```math
\begin{align*}

&\text{Then its acceleration is given by:}

\end{align*}
```
```math
\vec{\mathbf{a}} = \frac{d\vec{\mathbf{v}}}{dt} = \frac{dv_x}{dt}\mathbf{\hat i} + \frac{dv_y}{dt}\mathbf{\hat j} + \frac{dv_z}{dt}\mathbf{\hat k}
```

```math
\begin{align*}

&\text{If the position of a particle is given by:}\\

\end{align*}
```
```math
\vec{\mathbf{r}} = x \mathbf{\hat i} + y \mathbf{\hat j} + z \mathbf{\hat k}
```
```math
\begin{align*}
&\text{Then its acceleration is given by:}\\
\end{align*}
```
```math
\vec{\mathbf{a}} = \frac{d^2\vec{\mathbf{r}}}{dt^2} = \frac{d^2x}{dt^2}\mathbf{\hat i} + \frac{d^2y}{dt^2}\mathbf{\hat j} + \frac{d^2z}{dt^2}\mathbf{\hat k}
```
### Determining Velocity from Acceleration:
```math
\begin{align*}

&\text{If the acceleration of a particle is given by:}\\

\end{align*}
```
```math
\vec{\mathbf{a}} = a_x\mathbf{\hat i} + a_y\mathbf{\hat j} + a_z\mathbf{\hat k}
```
```math
\begin{align*}

&\text{Then its velocity is given by:}

\end{align*}
```
```math
\vec{\mathbf{v}} = \int \vec{\mathbf{a}}dt = \left(\int a_xdt\right)\mathbf{\hat i} + \left(\int a_ydt\right)\mathbf{\hat j} + \left(\int a_zdt\right)\mathbf{\hat k}
```

### Motion in a Plane in Space:
```math
|\vec{\textbf{r}}| = \sqrt{x^2+y^2},\quad\quad x = |\vec{\textbf{r}}|\cos(\theta),\quad\quad y = |\vec{\textbf{r}}|\sin(\theta)\\[25pt]

\text{Displacement Vecotr: }\quad\vec{\textbf{s}} = \Delta\vec{\textbf{r}} = \vec{\textbf{r}}_2 - \vec{\textbf{r}}_1 = (x_2 - x_1, y_2 - y_1)\\[25pt]

\text{Vectors:}\quad\vec{\textbf{a}} + \vec{\textbf{b}} = \vec{\textbf{c}},\quad\quad\quad c_x = a_x + b_x,\quad\quad\quad c_y = a_y + b_y\\[15pt]
\therefore \quad\quad\vec{\textbf{a}} + \vec{\textbf{b}} = (a_x,a_y) + (b_x,b_y) = (a_z + b_x, a_y + b_y)\\[25pt]

\lambda\vec{\textbf{a}} = (\lambda a_x, \lambda a_y)\\[25pt]

\text{Velocity and acceleration in a plane: }\quad a_x = \frac{dv_x}{dt} = \frac{d^2x}{dt^2},\quad\quad\quad\vec{a} = \frac{d\vec{v}}{dt} = (\frac{dv_x}{dt}, \frac{dv_y}{dt})
```
```math
\begin{align*}

\text{Motion in space: }\quad\quad &r = |\vec{r}| = \sqrt{x^2 + y^2 + z^2}\\[10pt]
&\vec{v} = \frac{d\vec{r}}{dt} = (\frac{dx}{dt},\frac{dy}{dt},\frac{dz}{dt})\\[10pt]
&\vec{a} = \frac{d\vec{v}}{dt} = (\frac{dv_x}{dt},\frac{dv_y}{dt},\frac{dv_z}{dt})\\[25pt]

\end{align*}
```
### Periodic Motion:
```math
\begin{align*}

\text{Circular motion: }\quad\quad &\Delta\theta: \text{Angular displacement between 2 points}\\[10pt]
S_{\text{arc}} = R|\Delta\theta|\quad\quad\quad&\Delta\theta = \theta_2 - \theta_1 \text{ (in radians)}\\[10pt]
(x,y) = (R\cos(\theta), R\sin(\theta))\quad\quad &\text{R: Radius}\\[25pt]

|\omega| = \left|\frac{d\theta}{dt}\right|\quad\quad\quad\vec{\omega}:\text{ Angular velocity,}\quad&\therefore\quad|\vec{\omega}|:\text{ Angular speed}\\[35pt]

\text{Uniform circular motion:}\quad|\vec{\omega}| &= \left|\frac{d\theta}{dt}\right| = \text{constant} = \left|\frac{\Delta \theta}{\Delta t}\right| = \frac{2\pi\text{rad}}{T}\\[10pt]

|\vec{r}| = R,\quad\quad\quad(x,y) &= (R\cos(\pm|\vec{\omega}|t\pm\theta_0), R\sin(\pm|\vec{\omega}|\pm\theta_0)),\quad\quad\vec{v} = \frac{2\pi\vec{r}}{T}\\[25pt]

\text{Magnitude of centripetal acceleration: }&\\
a = v\omega,\quad\quad a &= r\omega^2,\quad\quad a = \frac{v^2}{r}
\end{align*}
```
### Kepler's Law
```math
\begin{align*}

T:\text{ Period}\quad\quad\quad\quad k:\text{ Constant for}&\text{ the system}\quad\quad\quad\quad a:\text{ Semi-major axis (ellipse)}\\[15pt]
\frac{T^2}{a^3} = k \quad\quad&\lor\quad\quad T \propto a^\frac{3}{2}

\end{align*}
```
### Simple Harmonic Motion:
```math
\begin{align*}

f = \frac{1}{T}&\quad\quad\quad\quad \omega:\text{ Angular frequency}\\
\omega = 2\pi f = \frac{2\pi}{T}&\quad\quad\quad\quad f:\text{ Frequency,}\quad T:\text{ Period}\\
x(t) = A\sin(\omega t + \phi)&\quad\quad\quad\quad \phi :\text{ Phase constant/Initial phase,}\quad A:\text{ Amplitude}\\[25pt]

x(t) = A\sin(\omega t + \phi)&\implies x(t) = A\sin(\omega t + \phi)\\
v_x(t) = A\omega\cos(\omega t + \phi)&\implies v_x(t) = A\omega\sin\left(\omega t + \phi + \frac{\pi}{2}\right)\\
a_x(t) = -A\omega^2\sin(\omega t + \phi)&\implies a_x(t) = A\omega^2\sin(\omega t + \phi + \pi)\\[15pt]

\therefore\quad a_x(t) = -\omega^2x(t),&\quad\quad \frac{d^2x(t)}{dt^2} = -\omega^2x(t)

\end{align*}
```
### Work, Energy, & Power:
```math
\begin{align*}

&E_{\text{trans}} = \frac{1}{2}mv^2 - \frac{1}{2}mu^2,\quad\quad W = \Delta E_{\text{trans}} = \frac{1}{2}mv^2-\frac{1}{2}mu^2\\[15pt]

&E_{\text{grav}} = mgh,\quad\quad E_{\text{grav}} = -\frac{GmM_E}{r},\quad\quad E_{\text{grav}} = - \frac{Gm_1m_2}{r} \\[15pt]

&E_{\text{mech}} = E_{\text{trans}} + E_{\text{pot}} = \frac{1}{2}mv^2_{\text{escape}} - \frac{GmM_E}{r},\quad\quad E_{\text{mech}} = E_{\text{trans}} + E_{\text{pot}} = 0\\[15pt]

&V_{\text{escape}} = \sqrt{\frac{2GM_E}{R_E}}\\[15pt]

&W = \vec{F} \cdot \vec{s},\quad\quad W = \int^B_A F_x\ dx,\quad\quad W = \int^B_A \vec{F}\cdot d\vec{s}\\[15pt]

&P = \frac{dW}{dt} = \vec{F} \cdot \frac{d\vec{s}}{dt} = \vec{F} \cdot v\\[15pt]

&x(t) = A\sin(\omega t + \phi)\\
&v_x(t) = \frac{dx}{dt} = A\omega\cos(\omega t + \phi)\\
&a_x(t) = \frac{d^2x}{dt^2} = -A\omega^2\sin(\omega t + \phi) = \omega x(t)\\

\end{align*}
```
### Linear Momentum and Collisions:
```math
\begin{align*}

&(\vec{p}) = m\vec{v},\quad\quad\quad\Delta\vec{p} = \int^{T_2}_{T_1}\vec{F}\ dt\\[25pt]

&\text{No change in mass }\rightarrow \vec{F} = \frac{d\vec{p}}{dt}\\
&\text{Mass changing as a function of time }\rightarrow \vec{F} = m\vec{a} + \vec{v}\frac{dm}{dt}\\
&\vec{F} = m\vec{a}\rightarrow \text{Force,}\quad\quad \vec{p} = m\vec{v}\rightarrow \text{Momentum}\\[25pt]

&\text{One-Dimensional Elastic Collision }\rightarrow v_{1x}-v_{2x} = -(u_{1x} - u_{2x})\\
&\text{Multi-Dimensional Elastic Collision }\rightarrow \vec{u} = \vec{v}_1+\vec{v}_2\\
&\text{(kinetic energy is changed to some other form of energy in the collision)}\\[25pt]

&\text{Inelastic Collision }\rightarrow m_1u_{1x} + m_2u_{2x} = (m_1 + m_2)v_x\\
&\text{(no loss of kinetic energy in the collision)}\\[25pt]
\end{align*}
```
```math
\begin{align*}
&\text{Relativistic Momentum: }\quad\quad \vec{p} = \frac{m\vec{v}}{\sqrt{1-\frac{v^2}{c^2}}}\\

&E_{\text{tot}} = \frac{mc^2}{\sqrt{1-\frac{v^2}{c^2}}},\quad\quad E_{\text{mass}} = mc^2,\quad\quad E_{\text{trans}} = \frac{mc^2}{\sqrt{1-\frac{v^2}{c^2}}} - mc^2


\end{align*}
```
### Torque and Angular Momentum:
```math
\begin{align*}
&\Gamma = Fl,\quad\quad\quad Fl = \text{moment of force}\\
&\Gamma = rF\sin(\theta) \implies \Gamma = \vec{r} \times \vec{F},\quad\quad\vec{r} = \text{Displacement,}\quad\quad\vec{F} = \text{Force}\\[25pt]

&\text{Translational Equilibrium:}\\
&\sum_i \vec{F}_i = \vec{0}\quad\quad\quad\text{and}\quad\quad\quad\sum_i \vec{\Gamma}_i = \vec{0}\\[25pt]

&\text{Circular motion}\rightarrow \vec{v} = \vec{\omega}\times\vec{r}\\
&\vec{v} = \text{Instantaneous Velocity,}\quad\quad\quad\vec{\omega} = \text{Angular Velocity.}\quad\quad\quad\vec{r} = \text{Displacement}\\[25pt]

\end{align*}
```
```math
\begin{align*}

&\vec{\alpha} = \frac{d\vec{\omega}}{dt},\quad\quad\quad\quad\ \vec{\alpha} = \text{Angular Acceleration}\\
&\vec{\alpha}_{\text{tang}} = \vec{\alpha}\times\vec{r},\quad\quad\vec{\alpha}_{\text{tang}} = \text{Tangential Acceleration}\\

&I = \sum_i m_ir^2_i,\quad\quad I = \text{Moment of Inertia}\\

&E_{\text{rot}} = \frac{1}{2}I\omega^2\\
&p = \Gamma \cdot \vec{\omega}\\
&\vec{\Gamma} = \frac{d\vec{L}}{dt}\\
&\vec{F} = \frac{d\vec{p}}{dt}\\
&\vec{l} = \vec{r}\times\vec{p},\quad\quad\quad\quad\vec{l} = Angular Momentum\\
&\vec{L} = I\vec{\omega}\\

\end{align*}
```





































