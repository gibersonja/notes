# Calculus
## Derivatives
```math
\begin{align*}
\frac{d}{dx}\left[cu\right] = cu^\prime\ \ \ \ \ \ \frac{d}{dx}\left[u \pm v\right] = u^\prime \pm v^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[uv\right] = uv^\prime + vu^\prime\ \ \ \ \ \ \frac{d}{dx}\left[\frac{u}{v}\right] = \frac{vu^\prime - uv^\prime}{v^2}\\\\
\frac{d}{dx}\left[c\right] = 0\ \ \ \ \ \ \frac{d}{dx}\left[u^n\right] = nu^{n-1}u^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[x\right] = 1\ \ \ \ \ \ \frac{d}{dx}\left[|u|\right] = \frac{u}{|u|}(u^\prime)\\\\
\frac{d}{dx}\left[\ln u\right] = \frac{u^\prime}{u}\ \ \ \ \ \ \frac{d}{dx}\left[e^u\right] = e^uu^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[\log_au\right] = \frac{u^\prime}{(\ln a)u}\ \ \ \ \ \ \frac{d}{dx}\left[a^u\right] = (\ln a)a^uu^\prime\\\\
\frac{d}{dx}\left[\sin u\right] = (\cos u)u^\prime\ \ \ \ \ \ \frac{d}{dx}\left[\cos u\right] = -(\sin u)u^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[\tan u\right] = (\sec^2 u)u^\prime\ \ \ \ \ \ \frac{d}{dx}\left[\cot u\right] = -(\csc^2u)u^\prime\\\\
\end{align*}
```

```math
\begin{align*}
\frac{d}{dx}\left[\sec u\right] = (\sec u \tan u)u^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[\csc u\right] = -(\csc u \cot u)u^\prime\\\\
\frac{d}{dx}\left[\sin^{-1}u\right] = \frac{u^\prime}{\sqrt{1-u^2}}\ \ \ \ \ \ &\frac{d}{dx}\left[\cos^{-1}u\right] = \frac{-u^\prime}{\sqrt{1 - u^2}}\\\\
\frac{d}{dx}\left[\tan^{-1}u\right] = \frac{u^\prime}{1+u^2}\ \ \ \ \ \ &\frac{d}{dx}\left[\cot^{-1}u\right] = \frac{-u^\prime}{1+u^2}\\\\
\frac{d}{dx}\left[\sec^{-1}u\right] =\frac{u^\prime}{|u|\sqrt{u^2-1}}\ \ \ \ \ \ &\frac{d}{dx}\left[\csc^{-1}u\right] = \frac{-u^\prime}{|u|\sqrt{u^2-1}}\\\\
\frac{d}{dx}\left[\sinh u\right] = (\cosh u)u^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[\cosh u\right] = (\sinh u)u^\prime\\\\
\frac{d}{dx}\left[\tanh u\right] = (\text{sech}^2 u)u^\prime\ \ \ \ \ \ &\frac{d}{dx}\left[\coth u\right] = -(\text{csch}^2 u)u^\prime\\\\
\end{align*}
```
```math
\begin{align*}
\frac{d}{dx}\left[\text{sech} u\right] = -(\text{sech} u \tanh u)u^\prime\ \ \ \ \ \ &\frac{d}
{dx}\left[\text{csch} u\right] = -(\text{csch} u \coth u)u^\prime\\\\
\frac{d}{dx}\left[\sinh^{-1}u\right] = \frac{u^\prime}{\sqrt{u^2+1}}\ \ \ \ \ \ &\frac{d}{dx}\left[\cosh^{-1}u\right] = \frac{u^\prime}{\sqrt{u^2-1}}\\\\
\frac{d}{dx}\left[\tanh^{-1}u\right] = \frac{u^\prime}{1 - u^2}\ \ \ \ \ \ &\frac{d}{dx}\left[\coth^{-1} u\right] = \frac{u^\prime}{1 - u^2}\\\\
\frac{d}{dx}\left[\text{sech}^{-1}u\right] = \frac{-u^\prime}{u\sqrt{1-u^2}}\ \ \ \ \ \ &\frac{d}{dx}\left[\text{csch}^{-1}\right] = \frac{-u^\prime}{|u|\sqrt{1+u^2}}
\end{align*}
```
## Integrals
```math

\begin{align*}
\int kf(u)du = k \int f(u)du\ \ \ \ \ \ \int\left[f(u) \pm g(u)\right]du &= \int f(u)du \pm \int g(u)du\ \ \ \ \ \ \int du = u + C\\\\
\int u^ndu = \frac{u^{n+1}}{n+1}+C\ \ \ \ \ \ \int \frac{du}{u} = \ln |u| + C\ \ \ &\ \ \ \int e^udu = e^u+C\ \ \ \ \ \ \int a^udu = \left(\frac{1}{\ln a}\right)a^u+C\\\\
\int \sin u du = -\cos u + C\ \ \ \ \ \ \int \cos u du &= \sin u + C\ \ \ \ \ \ \int \tan u du = -\ln |\cos u|+C\\\\
\int \cot u du = \ln |\sin u| + C\ \ \ &\ \ \ \int \sec u du = \ln |\sec u + \tan u| + C\\\\
\int \csc u du = -\ln |\csc u + \cot u|+C\ \ \ &\ \ \ \int \sec^2 udu = \tan u + C\\\\
\int \csc^2 u du = -\cot u + C\ \ \ &\ \ \ \int \sec u \tan u du = \sec u + C\\\\
\int \csc u \cot u du = -\csc u + C\ \ \ &\ \ \ \int \frac{du}{\sqrt{a^2-u^2}}=\sin^{-1}\frac{u}{a} + C\\\\
\int \frac{du}{a^2+u^2}=\frac{1}{a}\tan^{-1}\frac{u}{a} + C\ \ \ &\ \ \ \int \frac{du}{u\sqrt{u^2-a^2}}=\frac{1}{a}\sec^{-1}\frac{|u|}{a}+C\\\\
\end{align*}
```
```math
\begin{align*}
\int \cosh u du = \sinh u + C\ \ \ &\ \ \ \int \sinh u du = \cosh u + C\\\\
\int \text{sech}^2udu = \tanh u + C\ \ \ &\ \ \ \int \text{csch}^2 u du = -\coth u + C\\\\
\int \text{sech} u \tanh u du = - \text{sech} u + C\ \ \ &\ \ \ \int \text{csch} u \coth u du = -\text{csch} u + C\\\\
\int^b_a f(t) dt + \int^c_b f(t) dt = \int^c_a f(t) dt\ \ \ \ \ \ \int_a^a f(t) &dt = 0\ \ \ \ \ \ \int^b_a f(t) dt = - \int^a_b f(t) dt
\end{align*}

```
## Differential Equations
### Separation of Variables
```math
\begin{align*}

&\text{All }y\text{ terms (including }dy\text{) can be moved to one side of the equation while}\\
&\text{ all }x\text{ terms (including }dx\text{) can be moved to the other.}
\end{align*}
```
```math
\begin{align*}
\frac{dy}{dx} &= 5xy\\
\frac{dy}{y} &= 5xdx\\
\int\frac{dy}{y} &= \int 5xdx\\
\ln|y| + C_1 &= \frac{5x^2}{2} + C_2\\
\ln y &= \frac{5x^2}{2} + C_3\\
y &=e^{\frac{5x^2}{2}+C_3} = e^{\frac{5x^2}{2}}e^{C_3}\\
y &= C_4e^{\frac{5x^2}{2}}\ \ \ \ \forall\ \ y \ge 0\\

\end{align*}
```
### Integrating Factor
```math
\begin{align*}

&\text{Differential equations of this form: }\frac{dy}{dx} + P(x)y = Q(x)\text{.  It is essentially the reverse of the product}\\
&\text{rule for derivatives.  By using the 'Integrating Factor', }\rho\text{, which is }e\text{ to the powerof the integral }\\
&\text{of the }y\text{ coefficient.  After multiplying each term of the equation by this factor it becomes more}\\
&\text{obvious that one side of the equation was the result of the product rule of derivatives.  The 'first' }\\
&(x^3)\text{ times the derivative of the 'second' }\left(\frac{dy}{dx}\right)\text{ plus the derivative of the 'first' }(3x^2)\text{ times the}\\
&\text{'second' }(y)\text{.  Using this infomration the anti-derivative can be reassembled and all that is left}\\
&\text{is to integrate the other side of the equation.}

\end{align*}
```
```math
\begin{align*}

\frac{dy}{dx} + \frac{3y}{x} &= \frac{e^x}{x^3}\\
\rho &= e^{\int\frac{3}{x}dx} = e^{3\ln|x|+C_1}\\
\rho &= x^3e^{C_1} = C_2x^3\\
\rho\frac{dy}{dx} + \rho\frac{3y}{x} &= \rho\frac{e^x}{x^3}\\
C_2x^3\frac{dy}{dx} + C_2x^3\frac{3y}{x} &= C_2x^3\frac{e^x}{x^3}\\
x^3\cdot\frac{dy}{dx} + 3x^2\cdot y &= e^x\\
x^3 \cdot y &= \int e^x dx\\
x^3 \cdot y &= e^x + C_3\\
y &= \frac{e^x + C_3}{x^3}\\

\end{align*}
```