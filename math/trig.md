## Trigonometric Identities
### Trigonometric Functions
```math
\begin{align*}

\sin(x) = \frac{1}{\csc(x)} = \frac{\text{opp}}{\text{hyp}}\quad\quad\quad\sec(x) &= \frac{1}{\cos(x)} = \frac{\text{hyp}}{\text{adj}}\quad\quad\quad\tan(x) = \frac{1}{\cot(x)} = \frac{\text{opp}}{\text{adj}}\\[25pt]

\csc(x) = \frac{1}{\sin(x)} = \frac{\text{hyp}}{\text{opp}}\quad\quad\quad\cos(x) &= \frac{1}{\sec(x)} = \frac{\text{adj}}{\text{hyp}}\quad\quad\quad\cot(x) = \frac{1}{\tan(x)} = \frac{\text{adj}}{\text{opp}}\\

\end{align*}
```
### Pythagorean Identities
```math
\begin{align*}

\sin^2(x) + \cos^2(x) = 1\quad\quad\quad1 + \tan^2(x) = \sec^2(x)\quad\quad\quad1 + \cot^2(x) = \csc^2(x)\\

\end{align*}
```
### Linear Combination of Sine and Cosine
```math
\begin{align*}

\alpha\cos(\theta) + \beta\sin(\theta) &= A\cos(\theta - D)\\[15pt]
\text{Where: }A = \sqrt{\alpha^2 + \beta^2}&\text{ and }D = \tan^{-1}\left(\frac{\beta}{\alpha}\right)

\end{align*}
```
### Co-Function Identites
```math
\begin{align*}

\sin\left(\frac{\pi}{2} - x\right) = \cos(x)\quad\quad\quad\csc\left(\frac{\pi}{2} - x\right) &= \sec(x)\quad\quad\quad\sec\left(\frac{\pi}{2} - x\right) = \csc(x)\\[25pt]

\cos\left(\frac{\pi}{2} - x\right) = \sin(x)\quad\quad\quad\tan\left(\frac{\pi}{2} - x\right) &= \cot(x)\quad\quad\quad\cot\left(\frac{\pi}{2} - x\right) = \tan(x)

\end{align*}
```
### Reduction Formulae
```math
\begin{align*}

\sin(-x) = -\sin(x)\quad\quad\quad\csc(-x) &= -\csc(x)\quad\quad\quad\sec(-x) = \sec(x)\\[25pt]

\cos(-x) = \cos(x)\quad\quad\quad\tan(-x) &= -\tan(x)\quad\quad\quad\cot(-x) = -\cot(x)

\end{align*}
```
### Sum and Difference
```math
\begin{align*}

\sin(u \pm v) = \sin(u)\cos(v) \pm \cos(u)\sin(v)\quad\quad&\quad\quad\cos(u \pm v) = \cos(u)\cos(v) \mp \sin(u)\sin(v)\\[25pt]

\tan(u \pm v) &= \frac{\tan(u) \pm \tan(v)}{1 \mp \tan(u)\tan(v)}

\end{align*}
```
### Double-Angle
```math
\begin{align*}

\sin(2u) = 2\sin(u)\cos(u)\quad\quad&\quad\quad\tan(2u) = \frac{2\tan(u)}{1 - \tan^2(u)}\\[25pt]

\cos(2u) = \cos^2(u) - \sin^2(u) &= 2\cos^2(u) - 1 = 1 - 2\sin^2(u)

\end{align*}
```
### Power-Reducing
```math
\begin{align*}

\sin^2(u) = \frac{1 - \cos(2u)}{2}\quad\quad\quad\cos^2(u) &= \frac{1 + cos(2u)}{2}\quad\quad\quad\tan^2(u) = \frac{1 - \cos(2u)}{1 + \cos(2u)}

\end{align*}
```
### Sum-to-Product
```math
\begin{align*}

\sin(u) + \sin(v) = 2\sin\left(\frac{u + v}{2}\right)\cos\left(\frac{u - v}{2}\right)\quad\quad&\quad\quad\sin(u) - \sin(v) = 2\cos\left(\frac{u + v}{2}\right)\sin\left(\frac{u - v}{2}\right)\\[25pt]

\cos(u) + \cos(v) = 2\cos\left(\frac{u + v}{2}\right)\cos\left(\frac{u - v}{2}\right)\quad\quad&\quad\quad\cos(u) - \cos(v) = -2\sin\left(\frac{u + v}{2}\right)\sin\left(\frac{u - v}{2}\right)

\end{align*}
```
### Sum-to-Product
```math
\begin{align*}

\sin(u)\sin(v) = \frac{1}{2}[\cos(u -v) - \cos(u + v)]\quad\quad&\quad\quad\cos(u)\cos(v) = \frac{1}{2}[\cos(u - v) + \cos(u + v)]\\[25pt]

\sin(u)\cos(v) = \frac{1}{2}[\sin(u + v) + \sin(u - v)]\quad\quad&\quad\quad\cos(u)\sin(v) = \frac{1}{2}[\sin(u + v) - \sin(u - v)]

\end{align*}
```
### Hyperbolic Definitions
```math
\begin{align*}

\sinh(x) = \frac{e^x - e^{-x}}{2}\quad\quad\cosh(x) &= \frac{e^x + e^{-x}}{2}\quad\quad\tanh(x) = \frac{\sinh(x)}{\cosh(x)}\\[25pt]

\text{csch}(x) = \frac{1}{\sinh(x)}\quad\quad\text{sech}(x) &= \frac{1}{\cosh(x)}\quad\quad\coth(x) = \frac{1}{\tanh(x)}\\[15pt]

x \ne 0\quad\quad\quad\quad\quad\quad&\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad x \ne 0

\end{align*}
```
### Hyperbolic Identities
```math
\begin{align*}

\cosh^2(x) - \sinh^2(x) = 1\quad\quad\tanh^2(x) &+ \text{sech}^2(x) = 1\quad\quad\coth^2(x) - \text{csch}^2(x) = 1\\[25pt]

\sinh^2(x) = \frac{-1 + \cosh(2x)}{2}\quad\quad&\quad\quad\cosh^2(x) = \frac{1 + \cosh(2x)}{2}\\[25pt]

\sinh(x + y) = \sinh(x)\cosh(y) + \cosh(x)\sinh(y)\quad&\quad\sinh(x - y) = \sinh(x)\cosh(y) - \cosh(x)\sinh(y)\\[25pt]

\cosh(x + y) = \cosh(x)\cosh(y) + \sinh(x)\sinh(y)\quad&\quad\cosh(x -y) = \cosh(x)\cosh(y) - \sinh(x)\sinh(y)\\[25pt]

\cosh(ix) = \frac{e^{ix} + e^{-ie}}{2} = \cos(x)\quad\quad&\quad\quad\sinh(ix) = \frac{e^{ix} - e^{-ie}}{2} = i\sin(x)

\end{align*}
```
### Unit Circle
![Unit Circle](unitcircle.png)

## Conics
![Conics Table](conics.png)

## Logic Symbols
```math
\begin{align*}
&\neg\text{ NOT}\\
&\land\text{ AND}\\
&\lor\text{ OR}\\
&\implies\text{ implies ...}\\
&\iff\text{ if and only if ...}\\
&\equiv\text{ logical equivalence}\\
&\therefore\text{ therefore}\\
&\exists\text{ there exists}\\
&\forall\text{ for all}\\
\end{align*}
```
## Set Theory
```math
\begin{align*}

&\emptyset\text{ empty set, } \{0\}\\
&\mathbb{N}\text{ set of natural numbers, } \{0,1,2,3,...\}\in\mathbb{N}_0, \{1,2,3,...\}\in\mathbb{N}_1\\
&\mathbb{Z}\text{ set of integers, }\{...,-2,-1,0,1,2,...\}\in\mathbb{Z}\\
&\mathbb{Q}\text{ set of rational numbers (fractions)}\\
&\mathbb{R}\text{ set of real numbers}\\
&\mathbb{C}\text{ set of complex numbers}\\
&\{\alpha, \beta, \gamma\}\text{ elements of a set}\\
&|\text{ such that ...}\\
&\in\text{ element of ...}\\
&\not\in\text{ not an element of ...}\\
&\subset\text{ subset of ... (sets are different)}\\
&\subseteq\text { subset of ... (set maybe equal)}\\
&\not\subset\text{ not a subset of ...}\\
&\supset\text{ converse of }\subset, A \subset B \implies B \supset A\\
&\supseteq\text{ converse of }\subseteq, A \subseteq B \implies B \supseteq A\\
&\cup\text{ union, }A\cup B = \{x|(x\in A)\lor (x \in B) \}\\
&\cap\text{ intersection, }A\cap B = \{x|(x\in A) \land (x\in B) \}
\end{align*}
```
## Exponent and Logarithm Properties
```math
\begin{align*}

a^ma^n = a^{m+n}\quad\quad\quad\quad(a^m)^n &= a^{mn}\quad\quad\quad\quad\quad(ab)^m = a^mb^m\\[25pt]

\frac{a^m}{a^n} = a^{m - n}, a \ne 0\quad\quad\quad\left(\frac{a}{b}\right)^m &= \frac{a^m}{b^m}, b \ne 0\quad\quad\quad a^{-m} = \frac{1}{a^m}, a \ne 0\\[25pt]

a^{\frac{1}{n}} = \sqrt[n]{a}\quad\quad\quad\quad\quad\quad\quad\quad a^0 &= 1, a \ne 0\quad\quad\quad\quad a^{\frac{m}{n}} = \sqrt[n]{a^m} = \left(\sqrt[n]{a}\right)^m\\[25pt]

\ln(xy) = \ln(x) + \ln(y)\quad\quad\quad\quad&\quad\quad\quad\quad\quad\log_a(xy) = \log_a(x) + log_a(y)\\[25pt]

\ln\left(\frac{x}{y}\right) = \ln(x) - \ln(y)\quad\quad\quad\quad&\quad\quad\quad\quad\quad\log_a\left(\frac{x}{y}\right) = \log_a(x) - \log_a(y)\\[25pt]
\end{align*}
```
```math
\begin{align*}
\ln(x^y) = y \cdot \ln(x)\quad\quad\quad\quad&\quad\quad\quad\quad\quad\log_a(x^y) = y \cdot \log_a(x)\\[25pt]

\ln(e^x) = x \quad\quad\quad\quad&\quad\quad\quad\quad\quad \log_a(a^x) = x\\[25pt]

e^{\ln(x)} = x \quad\quad\quad\quad&\quad\quad\quad\quad\quad a^{log_a(x)} = x\\[25pt]

\ln(e) = 1\quad\quad\quad\quad&\quad\quad\quad\quad\quad\log_a(a) = 1,\ \ \forall a > 0\\[25pt]

\ln(1) = 0\quad\quad\quad\quad&\quad\quad\quad\quad\quad\log_a(1) = 0,\ \ \forall a > 0\\

\end{align*}
```