# Statics
## Laws of Motion
\
**Law I:** Every body continues in a state of rest, or moves with constant velocity in a straight line, unless a resultant force is applied to it.\
\
**Law II:** The rate of change of the velocity of a body is proportional to the resultant force applied to  the body, and is made in the direction of the resultant force.\
\
**Law III:** To every action (force) by one body on another, there is always an opposed equal reaction (force).\

```math
N = \frac{\text{kg}\cdot\text{m}}{\text{s}^2}\quad\quad\quad \text{J} = \text{N}\cdot\text{m} = \frac{\text{kg}\cdot\text{m}^2}{\text{s}^2}
```
![Force Diagram](triangle.png)
```math
\begin{align*}
\vec{\mathbf{F}} = |\vec{\mathbf{F}}|\cos\theta\mathbf{\hat i} + |\vec{\mathbf{F}}|\sin\theta\mathbf{\hat j},\ \ &\ \ |\vec{\mathbf{F}}| = \sqrt{\Delta x^2+\Delta y^2}\\
\sin\theta = \frac{\Delta y_\mathbf{{\hat j}}}{|\vec{\mathbf{F}}|} \Rightarrow |\vec{\mathbf{F}}|\sin\theta\mathbf{\hat j},\ \ &\ \ \cos\theta = \frac{\Delta x_\mathbf{{\hat i}}}{|\vec{\mathbf{F}}|} \Rightarrow |\vec{\mathbf{F}}|\cos\theta\mathbf{\hat i}\\[25pt]
\end{align*}
```

```math
\begin{align*}

&\text{If an object rests on a plane inclined at an  angle of }\alpha\text{, and is  on the point of slipping, then the coefficient }\mu\text{ of static friction between the object and the plane}\\
&\text{is given by: }\mu = \tan(\alpha)\\[25pt]

&\text{The maximum magnitude of the friction force is }\mu|\vec{\textbf{N}}|\text{, where }\mu\text{ is the coefficient of static friction for the object and the surface involved, and }\vec{\textbf{N}}\text{ is the Normal}\\
&\text{reaction of the Surface on the object.}\\[25pt]

&\text{If }|\vec{\textbf{F}}_\textbf{\text{friction}} = \mu|\vec{\textbf{N}}\text{, then the object is on the point of slipping.}

\end{align*}
```