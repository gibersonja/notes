# Electromagnetism
## Gravitational/Electric Forces & Fields
### Constants:
```math
\begin{align*}

&G = 6.67 \times 10^{-11}\text{Nm}^2\text{kg}^{-2}\\
&\epsilon_0 = \frac{1}{\mu_0c^2} = \text{Permittivity of Free Space} \approx 8.8542 \times 10^{-12} \text{Fm}^{-1}\\
&\frac{1}{4\pi\epsilon_0} \approx 8.998\times 10^9\frac{\text{Nm}^2}{C^2}\\

\end{align*}
```
### Newtons Law of Gravitation:
```math
\begin{align*}

\vec{F}_{\text{grav}} = -\frac{GmM}{r^2}\vec{r}\\

\end{align*}
```
### Gravitational Field (point mass):
```math
\vec{g}(\vec{r}) = \frac{\vec{F}_{\text{grav}}(\text{on }m\text{ at }\vec{r})}{m}\implies\vec{g}(\vec{r}) = \frac{-GM}{r^2}\vec{r}\\
```
### Coulomb's Law:
```math
\vec{F}_{el} = \frac{1}{4\pi\epsilon_0}\cdot\frac{qQ}{r^2}\vec{r}
```
### Electric Field (point charge):
```math
\vec{E}(\vec{r}) = \frac{\vec{F}_{\text{el}}\text{(on }q\text{ at }\vec{r})}{q}
```
## Gravitation & Electric Potential:
```math

\vec{F} = \frac{k}{r^2}\hat{r},\quad\text{ where }k = \begin{cases}
        \frac{q_1q_2}{4\pi \epsilon_0} & \text{in the electrostatic case}\\
        -Gm_1m_2 & \text{in the gravitational case}
    \end{cases}

```
```math
\begin{align*}

&E_{\text{grav}} = \frac{-Gm_1m_2}{r}\quad\text{ and }\quad E_{\text{el}} = \frac{q_1q_2}{4\pi\epsilon_0r}\\[15pt]

&\text{Where }E_{\text{grav}}\text{ and }E_{\text{el}}\text{ are taken to be zero where }r = \infty

\end{align*}
```
### Electric Potential:
```math

V(\vec{r}) = \left(\frac{1}{q}\right)E_{\text{el}}\\[15pt]
\text{With }q\text{ at }\vec{r}\implies V(\vec{r}) = \frac{Q}{4\pi\epsilon_0r}

```
### Electric Potential Difference:
```math
\begin{align*}

&\Delta E_{\text{el}} = q\Delta V\\[15pt]
&E_x = \frac{-dV(\vec{r})}{dx}\\[15pt]
&E_y = \frac{-dV(\vec{r})}{dy}\\[15pt]
&E_z = \frac{-dV(\vec{r})}{dz}

\end{align*}
```
### Gravitational Potential:
```math

V_{\text{grav}}(\vec{r}) = \frac{1}{m}\left(\frac{-GmM}{r}\right)\implies V_{\text{grav}}(\vec{r}) = \frac{-GM}{r}

```
### Capacitance
```math
C = \frac{q}{V}
```
#### Capacitance of Parallel Plate Capacitor in Vacuum:
```math
C = \epsilon_0\frac{A}{d}\\[25pt]
A = \text{area},\quad\quad d = \text{distance},\quad\quad\epsilon_0 = \text{Permittivity of Free Space},\quad\quad\epsilon_r = \text{Relative Permittivity}
```
#### Capacitance of Parallel Plate Capacitor filled with dielectric:
```math
C = \epsilon_r\epsilon_0\frac{A}{d}\\[25pt]
A = \text{area},\quad\quad d = \text{distance},\quad\quad\epsilon_0 = \text{Permittivity of Free Space},\quad\quad\epsilon_r = \text{Relative Permittivity}
```
#### Electric Energy  Stored by a Capacitor:
```math
E_{\text{el}} = \frac{qV}{2} = \frac{CV^2}{2} = \frac{q^2}{2C}
```
### Electric Currents:
#### Current:
```math
i = \frac{dq}{dt}
```
#### Resistance:
```math
R = \frac{|V_R|}{|i|}
```
#### Ohm's Law:
```math
V = iR
```
#### Series Resistances:
```math
R_{\text{eff}} = R_1 + R_2 + R_3 + \ldots = \sum_i R_i
```
#### Parallel Resistances:
```math
\frac{1}{R_{\text{eff}}} = \frac{1}{R_1} + \frac{1}{R_2} + \frac{1}{R_3} + \ldots = \sum_i \frac{1}{R_i}
```
#### Power:
```math
P = iV = i^2R = \frac{V^2}{R}
```
#### Capacitance:
```math
C = \frac{Q}{V}
```
#### Capacitance in Parallel:
```math
\sum_i Q_i = \sum_i C_iV = CV\quad\quad\quad\quad \therefore\quad\sum_iC_i
```
#### Capacitance in Series:
```math
V = \frac{Q}{C} = \sum_i\frac{Q}{C_i}\quad\quad\quad\lor\quad\quad\quad\frac{1}{C} = \sum_i\frac{1}{C_i}
```
```math
C = \frac{C_1C_2}{C_1 + C_2}
```
### Electric Fields:
```math
\begin{align*}

&\text{The force exerted by a point charge }Q_a\text{ on a point, charge }Q_b\text{ is: }\quad\vec{F}_{ab} = \frac{1}{4\pi\epsilon_0}\frac{Q_aQ_b}{r^2}\hat{r}_1\\
&\text{Where }r\text{ is the distance bettween the charges and }\hat{r}_1\text{ is the unit vector pointing \textbf{from} }Q_a\\
&\text{to }Q_b\\[15pt]

&\text{We consider the force }\vec{F}_{ab}\text{ as being the product of }Q_b\text{ by the \textbf{electric field intensity} due}\\
&\text{to }Q_a:\quad\vec{E} = \frac{Q_a}{4\pi\epsilon_0r^2}\hat{r}_1\\[25pt]

&\vec{\nabla}\times\vec{E} = \vec{0}\quad\quad\text{hence:}\quad\vec{E} = -\vec{\nabla}V\quad\quad\text{where:}\quad\frac{1}{4\pi\epsilon_0}\int_{\tau^\prime}\frac{\rho d\tau^\prime}{r}\text{ is the \textbf{electric potential.}}\\
&\rho d\tau^\prime \text{ is contained within the element of the volume }d\tau^\prime\text{ and }r\text{ is the distance between this element}\\
\\&\text{and the point where }v\text{ is calculated.}

\end{align*}
```
#### Poisson's Equation:
```math
\vec{\nabla}^2V = \frac{-\rho}{\epsilon_0}\\
\text{Where charge density is zero we have Laplace's Equation.}
```
#### Laplace's Equation:
```math
\vec{\nabla}^2V = 0
```
```math
\begin{align*}

&\text{According to the \textbf{principle of superposition} two or more electric field intensityies acting at a given}\\
&\text{point add vectorially.  For an extended charge distribution:}\quad\vec{E} = \frac{1}{4\pi\epsilon_0}\int_{\tau^\prime}\frac{\rho\hat{r}_1}{r^2}d\tau^\prime\quad\text{The electrostatic}\\
&\text{field is conservative.}

\end{align*}
```
#### Gauss's Law:
```math
\int_{S^\prime}\vec{E}\cdot d\vec{a}^\prime = \frac{1}{\epsilon_0}\int_{\tau^\prime}\rho  d\tau^\prime\quad\quad\quad\lor\quad\quad\quad \vec{\nabla}\cdot\vec{E} = \frac{\rho}{\epsilon_0} \\[15pt]
\text{Where }S^\prime\text{ is the surface that encloses }\tau^\prime\\[25pt]

\text{The \textbf{potential energy} associated with a charge distribution can be written either as: }\\

W = \frac{1}{2}\int_{\tau^\prime}V\rho d\tau^\prime\quad\quad\quad\lor\quad\quad\quad W = \frac{\epsilon_0}{2}\int_\tau E^2\ d\tau
```
### Magnetic Fields:
#### Magnetic Force:
```math
\vec{F}_m = q[\vec{v}\times\vec{B}]
```
#### Magnetic Field (Long Straight Wire):
```math
B(r) = \frac{\mu_0i}{2\pi r}
```
#### Magnetic Field (Circular Current Loop):
```math
B_{\text{center}} = \frac{\mu_0i}{2R}
```
#### Magnetic Field (Infinitely Long Cylindrical Solenoid):
```math
B = \frac{\mu_0Ni}{l}
```
#### Cyclotron Radius:
```math
R_c = \frac{mV}{|q|B}
```
#### Cyclotron Frequency:
```math
f_c = \frac{1}{2\pi}\frac{|q|B}{m}
```
#### Magnetic Flux:
```math
\phi = AB\cos(\theta),\quad\quad \phi = \vec{A}\cdot\vec{B}
```
#### Induced Current:
```math
|V_{\text{ind}}(t)| = \left|\frac{d\phi(t)}{dt}\right|
```
#### Work:
```math
\Delta W = \vec{F}\cdot\Delta\vec{s}
```
#### Electric Force:
```math
\vec{F}(\vec{r}) = q\vec{E}(\vec{r})
```
#### Lorentz Force Law:
```math
\vec{F}(\vec{r}) = q\left[\vec{E}(\vec{r}) + \vec{v}\times\vec{B}(\vec{r})\right]
```
#### Maxwell's Equations:
```math
\begin{align*}

&\vec{\nabla}\cdot\vec{E} = \frac{\rho}{\epsilon_0}\\
&\vec{\nabla}\cdot\vec{B} = 0\\
&\vec{\nabla}\times\vec{E} =  -\frac{\partial\vec{B}}{\partial t}\\
&\vec{\nabla}\times\vec{B} = \mu_0\vec{J} + \epsilon_0\mu_0\frac{\partial\vec{E}}{\partial t}

\end{align*}
```
#### Permeability  of Free Space:
```math
\mu_0 = 4\pi \times 10^{-7}TmA^{-1} = 1.257\times10^{-6}NA^{-2}
```
#### Permittivity of Free  Space:
```math
\epsilon_0 = \frac{1}{\mu_0c^2} = 8.854 \times 10^{-12}Fm^{-1}
```
#### Transformations in Electromagnetism
```math
\begin{align*}

\vec{E}^\prime_{||} = \vec{E}_{||}\quad\quad\quad&\quad\quad\quad\vec{B}^\prime_{||} = \vec{B}_{||}\\
\vec{E}^\prime_{\perp} = \gamma\left(\vec{E}_\perp + \vec{v} \times \vec{B}_\perp\right)\quad\quad&\quad\quad\vec{B}^\prime_\perp = \gamma\left(\vec{B}_\perp - \frac{\vec{v}\times\vec{E}}{c^2}\right)

\end{align*}
```